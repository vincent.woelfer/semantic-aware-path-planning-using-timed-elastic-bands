#!/bin/bash

# Abort on error
set -e

printf "===============================\n"
printf "===  Seting up Repository   ===\n"
printf "===============================\n"


printf "\n===============================\n"
printf "=== Installing apt packages ===\n"
printf "===============================\n\n"
sudo apt update -y
sudo apt install -y curl ca-certificates wget xvfb python-pip \
  ros-melodic-gazebo-* ros-melodic-gmapping ros-melodic-map-server ros-melodic-turtlebot3 ros-melodic-turtlebot3-* ros-melodic-amcl ros-melodic-map-msgs \
  ros-melodic-move-base ros-melodic-tf2-eigen ros-melodic-costmap-converter ros-melodic-interactive-markers ros-melodic-laser-filters \
  ros-melodic-kobuki-ftdi ros-melodic-rocon-std-msgs ros-melodic-dynamixel-sdk ros-melodic-joint-state-publisher-gui \
  libgoogle-glog-dev libgflags-dev libatlas-base-dev libsuitesparse-dev libmetis-dev libeigen3-dev


printf "\n===============================\n"
printf "===  Cloning Ceres-Solver   ===\n"
printf "===============================\n\n"
rm -rf ceres-solver
git -c advice.detachedHead=false clone --depth=1 https://ceres-solver.googlesource.com/ceres-solver --branch 2.0.0 ceres-solver


printf "\n===============================\n"
printf "===  Building Ceres Solver  ===\n"
printf "===============================\n\n"
cd ceres-solver
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=RELEASE, -DCMAKE_CXX_FLAGS="-O3" ..
make -j$((`nproc`-2))    
sudo make install
cd ../../


printf "\n===============================\n"
printf "===  Building SEB Planner   ===\n"
printf "===============================\n\n"
sudo rm -rf build devel
source /opt/ros/melodic/setup.bash
catkin_make
source devel/setup.bash
catkin_make install -j$((`nproc`-2))

printf "\n===============================\n"
printf "===          DONE           ===\n"
printf "===============================\n\n"
