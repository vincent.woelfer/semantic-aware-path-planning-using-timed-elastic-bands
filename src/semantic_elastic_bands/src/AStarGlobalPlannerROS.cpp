#include "semantic_elastic_bands/AStarGlobalPlannerROS.h"

// pluginlib macros
#include <pluginlib/class_list_macros.h>

#include <utility>

// register this planner as a BaseLocalPlanner plugin
PLUGINLIB_EXPORT_CLASS(semantic_elastic_bands::AStarGlobalPlannerROS, nav_core::BaseGlobalPlanner)

namespace semantic_elastic_bands {

AStarGlobalPlannerROS::AStarGlobalPlannerROS() :
		isInitialized(false)
{
}

AStarGlobalPlannerROS::AStarGlobalPlannerROS(std::string name, costmap_2d::Costmap2DROS* costmap_ros) :
		isInitialized(false)
{
	initialize(std::move(name), costmap_ros);
}

AStarGlobalPlannerROS::~AStarGlobalPlannerROS() = default;

void AStarGlobalPlannerROS::initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros) {
	if(isInitialized) {
		ROS_WARN("seb_a_star_global_planner has already been initialized!");
		return;
	}

	// create Node Handle with name of plugin (as used in move_base for loading)
	ros::NodeHandle nh("~/" + name);

	int costOffset = 100;       // Increase everything by this cost to avoid the planner staying in low cost regions completely 
	int maxTraversalCost = 160; // Cells higher than this are impassable
	bool smoothing = true;
	
	if(!nh.param("costOffset", costOffset, costOffset)) {
		ROS_WARN("seb_a_star_global_planner: Param costOffset is not set, using default value %d", costOffset);
	}
	if(!nh.param("maxTraversalCost", maxTraversalCost, maxTraversalCost)) {
		ROS_WARN("seb_a_star_global_planner: Param maxTraversalCost is not set, using default value %d", maxTraversalCost);
	}
	if(!nh.param("smoothing", smoothing, smoothing)) {
		ROS_WARN("seb_a_star_global_planner: Param smoothing is not set, using default value %s", smoothing ? "true" : "false");
	}
	
	frameId = costmap_ros->getGlobalFrameID();
	planner = new AStarPathPlanner(costmap_ros->getCostmap(), frameId, costOffset, maxTraversalCost, smoothing);
	publisher = nh.advertise<nav_msgs::Path>("plan", 1);

	ROS_INFO("Initialized seb_a_star_global_planner");
	isInitialized = true;
}

bool AStarGlobalPlannerROS::makePlan(const geometry_msgs::PoseStamped& start, const geometry_msgs::PoseStamped& goal, std::vector<geometry_msgs::PoseStamped>& plan) {
	if(!isInitialized) {
		ROS_ERROR("seb_a_star_global_planner has not been initialized!");
		return false;
	}		

	plan.clear();
	plan = planner->findPath(start, goal);

	// Plan already is in world coordinates
	publishPlan(plan);
	
	return !plan.empty();
}

void AStarGlobalPlannerROS::publishPlan(const std::vector<geometry_msgs::PoseStamped>& path) const {
	if(!isInitialized) {
		ROS_ERROR("seb_a_star_global_planner has not been initialized!");
		return;
	}
	
	// Create a message for the plan
	nav_msgs::Path msg;
	msg.poses.resize(path.size());

	msg.header.frame_id = frameId;
	msg.header.stamp = ros::Time::now();
	
	// Extract the plan in world coordinates, we assume the path is all in the same frame
	for (unsigned int i = 0; i < path.size(); i++) {
		msg.poses[i] = path[i];
	}
	
	publisher.publish(msg);
}

}


