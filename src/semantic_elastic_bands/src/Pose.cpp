#include <tf/transform_datatypes.h>
#include "semantic_elastic_bands/Pose.h"
#include "semantic_elastic_bands/Utility.h"

namespace semantic_elastic_bands {

Pose::Pose() : 
		position(0, 0),
		theta(0)
{
}

Pose::Pose(Eigen::Vector2d position, double theta) :
		position(std::move(position)),
		theta(normalizeTheta(theta)) {
}

Pose::Pose(double x, double y, double theta) :
		position(x, y),
		theta(normalizeTheta(theta)) {
}

Pose Pose::constructAverage(const Pose& a, const Pose& b) {
	return Pose((a.position + b.position)/2.0, averageTheta(a.theta, b.theta));	
}

double Pose::getLinearDistance(const Pose& a, const Pose& b) {
	return (a.position - b.position).norm();
}

bool Pose::operator==(const Pose& other) const {
	return this->position == other.position && this->theta == other.theta;
}

bool Pose::operator!=(const Pose& other) const {
	return !(other == *this);
}

geometry_msgs::Pose Pose::toPoseMsg() const {
	geometry_msgs::Pose msg;
	msg.position.x = position.x();
	msg.position.y = position.y();
	msg.position.z = 0;
	msg.orientation = tf::createQuaternionMsgFromYaw(theta);
	return msg;
}

Point Pose::toPoint() const {
	return Point(std::floor(position.x()), std::floor(position.y()));
}

}