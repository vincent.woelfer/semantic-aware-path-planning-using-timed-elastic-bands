#include <ros/init.h>
#include <ros/ros.h>

#include <nav_msgs/OccupancyGrid.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/ColorRGBA.h>

/*
 * This ROS-Node subscribers to the costmap and creates a visually easily understandable version of it.
 * This version is created as visualization-marker and can be viewed in rviz.
 * The simplified visualization separates the cells into 4 categories based on their value,
 * impassable cells are colored black while the remaining cells are colored in red - yellow - green. 
 */

// Global Variables
static int sequence = 0;

nav_msgs::OccupancyGrid occupancyGrid;
visualization_msgs::Marker msg;
int lastWidth;
int lastHeight;
double lastResolution;

// Forward declare functions
void globalCostmapCallback(const nav_msgs::OccupancyGrid& msg);
void constructImage(ros::Publisher& publisher);
void initMessage(int width, int height, double resolution);
std_msgs::ColorRGBA calculateColor(int8_t occupancyValue);

// ################################### MAIN ###################################
int main(int argc, char** argv ) {
	ros::init(argc, argv, "seb_costmap_visualization");
	ros::NodeHandle n("~");
	ROS_INFO("Starting seb Costmap Visualization!");
	
	std::string costmapTopic = "/move_base/local_costmap/costmap";
	std::string imageTopic = "/costmap_visualization";

	ros::Subscriber subscriber = n.subscribe(costmapTopic, 1, &globalCostmapCallback);
	ros::Publisher publisher = n.advertise<visualization_msgs::Marker>(imageTopic, 1, true);

	ros::Rate loopRate(4);	

	while(ros::ok()) {
		constructImage(publisher);

		ros::spinOnce();
		loopRate.sleep();
	}

	return 0;
}

std_msgs::ColorRGBA calculateColor(int8_t occupancyValue) {
	std_msgs::ColorRGBA color;
	
	// Unknown
	if(occupancyValue == -1) {
		color.a = 0.0;
		return color;
	}
	color.a = 1.0;
	
	// Wall
	if(occupancyValue >= 100) {
		color.r = 0.1;
		color.g = 0.1;
		color.b = 0.1;
		return color;
	} 
	
	// Red
	if(occupancyValue >= 60) {
		color.r = 0.8;
		color.g = 0.2;
		color.b = 0.2;
		return color;
	}
	
	// Yellow
	if(occupancyValue >= 30) {
		color.r = 0.9;
		color.g = 0.7;
		color.b = 0.1;
		return color;
	}
	
	// Green
	color.r = 0.1;
	color.g = 0.8;
	color.b = 0.2;
	
	return color;
}

void constructImage(ros::Publisher& publisher) {
	ros::WallTime t0 = ros::WallTime::now();

	const int width = occupancyGrid.info.width;
	const int height = occupancyGrid.info.height;
	const double resolution = occupancyGrid.info.resolution;
	
	// Check if we can reuse the triangle position data
	bool reuse = true;
	if(lastWidth != width || lastHeight != height || lastResolution != resolution) {
		reuse = false;
		lastWidth = width;
		lastHeight = height;
		lastResolution = resolution;
		initMessage(width, height, resolution);
	}
	
	// Update changing data
	msg.header.frame_id = occupancyGrid.header.frame_id;
	msg.header.stamp = ros::Time::now();
	msg.header.seq = sequence++;	

	msg.pose = occupancyGrid.info.origin;
	
	// Update colors
	for(int x = 0; x < width; x++) {
		for(int y = 0; y < height; y++) {
			const int occupancyIndex = y * width + x;
			const int index = occupancyIndex * 6;

			// Color
			std_msgs::ColorRGBA color = calculateColor(occupancyGrid.data[occupancyIndex]);
			
			msg.colors[index + 0] = color;
			msg.colors[index + 1] = color;
			msg.colors[index + 2] = color;
			msg.colors[index + 3] = color;
			msg.colors[index + 4] = color;
			msg.colors[index + 5] = color;
		}		
	}
	
	publisher.publish(msg);

	ros::WallTime t1 = ros::WallTime::now();
	ROS_DEBUG("Costmap Visualization generated in %4.1f ms, initialized memory %s be reused", (t1 - t0).toNSec() * 1e-6, reuse ? "could" : "could not");
}

void initMessage(int width, int height, double resolution) {
	msg.ns = "costmap_visualization";
	msg.type = visualization_msgs::Marker::TRIANGLE_LIST;
	msg.action = visualization_msgs::Marker::MODIFY;
	
	msg.scale.x = 1.0;
	msg.scale.y = 1.0;
	msg.scale.z = 1.0;

	msg.points.resize(width * height * 6);
	msg.colors.resize(width * height * 6);

	for(int x = 0; x < width; x++) {
		for(int y = 0; y < height; y++) {
			const int occupancyIndex = y * width + x;
			const int index = occupancyIndex * 6;

			auto x0 = static_cast<float>(x * resolution);
			auto x1 = static_cast<float>((x + 1) * resolution);
			auto y0 = static_cast<float>(y * resolution);
			auto y1 = static_cast<float>((y + 1) * resolution);

			// up-left
			msg.points[index + 0].x = x0;
			msg.points[index + 0].y = y0;

			// down-left
			msg.points[index + 1].x = x0;
			msg.points[index + 1].y = y1;

			// down-right
			msg.points[index + 2].x = x1;
			msg.points[index + 2].y = y1;

			// up-left
			msg.points[index + 3].x = x0;
			msg.points[index + 3].y = y0;

			// up-right
			msg.points[index + 4].x = x1;
			msg.points[index + 4].y = y0;

			// down-right
			msg.points[index + 5].x = x1;
			msg.points[index + 5].y = y1;
		}
	}
}


void globalCostmapCallback(const nav_msgs::OccupancyGrid& msg) {
	occupancyGrid = msg;
}