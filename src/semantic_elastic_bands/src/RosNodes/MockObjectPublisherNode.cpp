#include <ros/init.h>
#include <ros/ros.h>

#include <geometry_msgs/Pose.h>
#include <random>
#include <tf2/LinearMath/Quaternion.h>
#include "semantic_elastic_bands/Utility.h"

#include <semantic_elastic_bands/DetectedObjectMsg.h>
#include <semantic_elastic_bands/DetectedObjectsMsg.h>
#include <semantic_elastic_bands/TrackedObjectMsg.h>
#include <semantic_elastic_bands/TrackedObjectsMsg.h>

/*
 * This ROS-Node acts as an mock publisher for Humans, AGVs and Corridors. 
 * */

#define CLASS_HUMAN 0
#define CLASS_AGV 1

using geometry_msgs::Pose;
using semantic_elastic_bands::DetectedObjectMsg;
using semantic_elastic_bands::DetectedObjectsMsg;
using semantic_elastic_bands::TrackedObjectMsg;
using semantic_elastic_bands::TrackedObjectsMsg;

std::mt19937* rng;
std::normal_distribution<double>* distribution;
int UniqueDetectionId = 0;

// Forward declare functions
geometry_msgs::Point addNoise(geometry_msgs::Point p, double strength);
geometry_msgs::Point toPoint(double x, double y);
geometry_msgs::Point simulatePath(double time, geometry_msgs::Point start, geometry_msgs::Point end, double duration);

void createAndSendMessages(ros::Publisher& publisherDetected, ros::Publisher& publisherTracked, double time, int sequence);

void createStaticHuman(int trackingId, std::vector<DetectedObjectMsg>& DetectedObjectMsgs, std::vector<TrackedObjectMsg>& trackedObjects,
					   double x, double y, double noiseStrength = 0.0);
void createWalkingHuman(int trackingId, std::vector<DetectedObjectMsg>& DetectedObjectMsgs, std::vector<TrackedObjectMsg>& trackedObjects, 
						double startX, double startY, double endX, double endY, double time, double speed, double noiseStrength = 0.0);
void createWalkingAGV(int trackingId, std::vector<DetectedObjectMsg>& DetectedObjectMsgs, std::vector<TrackedObjectMsg>& trackedObjects,
                        double startX, double startY, double endX, double endY, double time, double speed, double noiseStrength = 0.0);


// ################################### MAIN ###################################
int main(int argc, char** argv ) {
	ros::init(argc, argv, "seb_mock_object_publisher");
	ros::NodeHandle n("~");
	ROS_INFO("Starting seb Mock Object Publisher!");
	
	// Init random
	std::random_device dev;
	rng = new std::mt19937(dev());
	distribution = new std::normal_distribution<double>(0, 0.025);
	
	ros::Publisher publisherDetected = n.advertise<DetectedObjectsMsg>("detected_objects", 1, true);
	ros::Publisher publisherTracked = n.advertise<TrackedObjectsMsg>("tracked_objects", 1, true);

	ros::Rate loopRate(10);
	int sequence = 0;
	
	ros::Time startTime = ros::Time::now();
	
	while(ros::ok()) {
		createAndSendMessages(publisherDetected, publisherTracked, (ros::Time::now() - startTime).toSec(), sequence++);

		ros::spinOnce();
		loopRate.sleep();
	}

	return 0;
}

void createAndSendMessages(ros::Publisher& publisherDetected, ros::Publisher& publisherTracked, double time, int sequence) {
	DetectedObjectsMsg detectedMessage;
	TrackedObjectsMsg trackedMessage;

	detectedMessage.header.frame_id = "map";
	detectedMessage.header.seq = sequence;
	detectedMessage.header.stamp = ros::Time::now();
	
	trackedMessage.header = detectedMessage.header;
	
	// Humans
	double speed = 0.4;
	int trackingId = 0;
	
	// Noise humans for global path planning test
	double noiseStrength = 0.0;
	createStaticHuman(trackingId++, detectedMessage.detections, trackedMessage.tracks, -6.5, -1.0, noiseStrength);
	createStaticHuman(trackingId++, detectedMessage.detections, trackedMessage.tracks, -5.9, -3.9, noiseStrength);

	createWalkingHuman(trackingId++, detectedMessage.detections, trackedMessage.tracks, 1.5, -0.65, 13.0, -0.65, time + 2.0, speed);
	
	// Publish
	publisherDetected.publish(detectedMessage);
	publisherTracked.publish(trackedMessage);
}

void createStaticHuman(int trackingId, std::vector<DetectedObjectMsg>& DetectedObjectMsgs, std::vector<TrackedObjectMsg>& trackedObjects,
					   double x, double y, double noiseStrength) {
	DetectedObjectMsg detected;

	detected.detection_id = UniqueDetectionId++;
	detected.class_id = CLASS_HUMAN;
	detected.confidence = 1.0;
	detected.modality = DetectedObjectMsg::MODALITY_GENERIC_RGBD;

	detected.pose.pose.position = addNoise(toPoint(x, y), noiseStrength);
	detected.pose.pose.position.z = 0.0;

	detected.pose.pose.orientation.x = 0.0;
	detected.pose.pose.orientation.y = 0.0;
	detected.pose.pose.orientation.z = 0.0;
	detected.pose.pose.orientation.w = 1.0;
	
	DetectedObjectMsgs.push_back(detected);
	
	// For now do not create tracked entry	
}

void createWalkingHuman(int trackingId, std::vector<DetectedObjectMsg>& DetectedObjectMsgs, std::vector<TrackedObjectMsg>& trackedObjects,
						double startX, double startY, double endX, double endY, double time, double speed, double noiseStrength) {
	DetectedObjectMsg detected;

	detected.detection_id = UniqueDetectionId++;
	detected.class_id = CLASS_HUMAN;
	detected.confidence = 1.0;
	detected.modality = DetectedObjectMsg::MODALITY_GENERIC_RGBD;

	// Calculate walking period. simulatePath() takes period for back and forth path -> multiply with 2.0
	double period = (2.0 * semantic_elastic_bands::getDistance(startX, startY, endX, endY)) / speed;

	detected.pose.pose.position = addNoise(simulatePath(time, toPoint(startX, startY), toPoint(endX, endY), period), noiseStrength);

	// Calculate movementDirection
	geometry_msgs::Point prevPos = simulatePath(time - 0.5, toPoint(startX, startY), toPoint(endX, endY), period);
	const double yaw = semantic_elastic_bands::getAngleBetweenPoints(prevPos.x, prevPos.y, detected.pose.pose.position.x, detected.pose.pose.position.y);

	tf2::Quaternion orientation;
	orientation.setRPY(0.0, 0.0, yaw);
	orientation.normalize();
	detected.pose.pose.orientation.x = orientation.x();
	detected.pose.pose.orientation.y = orientation.y();
	detected.pose.pose.orientation.z = orientation.z();
	detected.pose.pose.orientation.w = orientation.w();
	
	DetectedObjectMsgs.push_back(detected);
	
	TrackedObjectMsg tracked;
	
	tracked.track_id = trackingId;
	tracked.is_occluded = false;
	tracked.is_matched = true;
	tracked.detection_id = detected.detection_id;
	tracked.class_id = CLASS_HUMAN;
	tracked.age = ros::Duration(1);
	tracked.pose = detected.pose;
	tracked.twist.twist.linear.x = 2.25;

	trackedObjects.push_back(tracked);
}

void createWalkingAGV(int trackingId, std::vector<DetectedObjectMsg>& DetectedObjectMsgs, std::vector<TrackedObjectMsg>& trackedObjects,
                      double startX, double startY, double endX, double endY, double time, double speed, double noiseStrength) {
	DetectedObjectMsg detected;

	detected.detection_id = UniqueDetectionId++;
	detected.class_id = CLASS_AGV;
	detected.confidence = 1.0;
	detected.modality = DetectedObjectMsg::MODALITY_GENERIC_RGBD;

	// Calculate walking period. simulatePath() takes period for back and forth path -> multiply with 2.0
	double period = (2.0 * semantic_elastic_bands::getDistance(startX, startY, endX, endY)) / speed;

	detected.pose.pose.position = addNoise(simulatePath(time, toPoint(startX, startY), toPoint(endX, endY), period), noiseStrength);

	// Calculate movementDirection
	geometry_msgs::Point prevPos = simulatePath(time - 0.5, toPoint(startX, startY), toPoint(endX, endY), period);
	const double yaw = semantic_elastic_bands::getAngleBetweenPoints(prevPos.x, prevPos.y, detected.pose.pose.position.x, detected.pose.pose.position.y);

	tf2::Quaternion orientation;
	orientation.setRPY(0.0, 0.0, yaw);
	orientation.normalize();
	detected.pose.pose.orientation.x = orientation.x();
	detected.pose.pose.orientation.y = orientation.y();
	detected.pose.pose.orientation.z = orientation.z();
	detected.pose.pose.orientation.w = orientation.w();

	// Publish only tracking message for AGVs
	//DetectedObjectMsgs.push_back(detected);

	TrackedObjectMsg tracked;

	tracked.track_id = trackingId;
	tracked.is_occluded = false;
	tracked.is_matched = false;
	tracked.class_id = CLASS_AGV;
	tracked.age = ros::Duration(1);
	tracked.pose = detected.pose;
	tracked.twist.twist.linear.x = 1.5;

	trackedObjects.push_back(tracked);
}

geometry_msgs::Point addNoise(geometry_msgs::Point p, double strength) {
	p.x += ((*distribution)(*rng) * strength);
	p.y += ((*distribution)(*rng) * strength);	

	return p;
}

geometry_msgs::Point toPoint(double x, double y) {
	geometry_msgs::Point p;
	p.x = x;
	p.y = y;
	p.z = 0.0;
	return p;
}

geometry_msgs::Point simulatePath(double time, geometry_msgs::Point start, geometry_msgs::Point end, double duration) {
	// Duration is for back AND forth
	double timeInInterval = std::fmod(time, duration);
	double t;

	double durationOneWay = duration / 2.0;

	// First half
	if(timeInInterval <= durationOneWay) {
		t = timeInInterval / durationOneWay;
	} else {
		t = std::abs(((timeInInterval - durationOneWay) / durationOneWay) - 1.0);
	}

	geometry_msgs::Point delta;
	delta.x = end.x - start.x;
	delta.y = end.y - start.y;
	delta.z = 0.0;

	geometry_msgs::Point result = start;
	result.x += delta.x * t;
	result.y += delta.y * t;

	return result;
}