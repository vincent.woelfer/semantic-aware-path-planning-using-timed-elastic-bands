#include <ros/init.h>
#include <ros/ros.h>
#include <numeric>

#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Float64.h>

#include "semantic_elastic_bands/Utility.h"
#include "semantic_elastic_bands/LineOfSightCalculations/LineOfSightCalculatorCellCenter.h"
#include "semantic_elastic_bands/LineOfSightCalculations/LineOfSightCalculatorExact.h"

#include <fstream>

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include "semantic_elastic_bands/DetectedObjects/DetectedObjectParser.h"
#include <tf2_ros/transform_listener.h>
#include "semantic_elastic_bands/DetectedObjects/DetectedPerson.h"

/*
 * This ROS-NODE only serves as an evaluation tool. Is subscribes to several topics and records, processes and analyzes their published data. 
 */

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

using namespace semantic_elastic_bands;

struct GlobalMap {
	std::vector<int8_t> data;
	int width{};
	int height{};	
	double originX{};
	double originY{};
	double resolution{};
	
	void copyData(int newWidth, int newHeight, double newOriginX, double newOriginY, double newResolution, const int8_t* newData) {
		if(width != newWidth || height != newHeight || originX != newOriginX || originY != newOriginY || resolution != newResolution) {
			width = newWidth;
			height = newHeight;
			originX = newOriginX;
			originY = newOriginY;
			resolution = newResolution;
			
			data.resize(width * height);
		}
		
		memcpy(data.data(), newData, width * height * sizeof(int8_t));
	}

	bool convertWorldToCostmapCell(double worldX, double worldY, int& mapX, int& mapY) const {
		if(worldX < originX || worldY < originY) {
			return false;
		}

		mapX = std::floor((worldX - originX) / resolution);
		mapY = std::floor((worldY - originY) / resolution);

		return !(mapX >= width || mapY >= height); 
	}

	bool convertWorldToCostmapExact(double worldX, double worldY, double& mapX, double& mapY) const {
		if(worldX < originX || worldY < originY) {
			return false;
		}

		mapX = (worldX - originX) / resolution;
		mapY = (worldY - originY) / resolution;

		return !(mapX >= width || mapY >= height);
	}
	
	double getCostmapCostBetween(double startXWorld, double startYWorld, double endXWorld, double endYWorld) {
		int startX, startY;
		int endX, endY;
		
		if(!convertWorldToCostmapCell(startXWorld, startYWorld, startX, startY) || !convertWorldToCostmapCell(endXWorld, endYWorld, endX, endY)) {
			ROS_ERROR("Path point not in global costmap!");
			return 0;
		}

		//ROS_WARN("Cost: %d - %d", data[startY * width + startX] + costOffset, data[endY * width + endX] + costOffset);
		LineOfSightCalculatorCellCenter<int8_t> lineOfSightCalculator(data.data(), width, startX, startY, endX, endY);

		constexpr int costOffset = 1;
		return lineOfSightCalculator.getLineOfSightCost(costOffset, 9999);		
	}

	double getCostmapCostBetweenExactPositions(double startXWorld, double startYWorld, double endXWorld, double endYWorld) {
		double startX, startY;
		double endX, endY;
		if(!convertWorldToCostmapExact(startXWorld, startYWorld, startX, startY) || !convertWorldToCostmapExact(endXWorld, endYWorld, endX, endY)) {
			ROS_ERROR("Path point not in global costmap!");
			return 0;
		}
		
		//ROS_WARN("Cost: %d - %d", data[startY * width + startX] + costOffset, data[endY * width + endX] + costOffset);
		LineOfSightCalculatorExact<int8_t> lineOfSightCalculator(data.data(), width,
																 static_cast<float>(startX), static_cast<float>(startY),
															     static_cast<float>(endX), static_cast<float>(endY));

		constexpr int costOffset = 1;
		return lineOfSightCalculator.getLineOfSightCost(costOffset, 9999);
	}
};

// Global Variables
GlobalMap globalMap;

// Odom Callback
double prevX = -999;
double prevY = -999;
double prevTheta = 0;
double traveledDistance = 0;
double traveledCost = 0;
double prevTimeStamp = -999;
double actualPrevTimeStamp = -999;

bool goalReached = false;

std::vector<double> planningTimes;
std::vector<double> planningTimesGlobal;

std::vector<double> speedData;
std::vector<double> distanceToHumansData;
std::vector<double> maxAllowedSpeedData;

double globalPlannerMean;
double globalPlannerStdev;
double globalPlannerMaxTime;
void calculateGlobalPlannerMeanStdev();

double globalPlanTotalDistance = 0;
double globalPlanTotalCost = 0;
int globalPlanTotalEntries = 0;

double finalGoalX = 11.4;
double finalGoalY = 2.0;

DetectedObjectParser* detectedObjectParser;
tf2_ros::Buffer* tfBuffer;

// Forward declare callbacks
void PathACallback(const nav_msgs::Path& msg);
void globalCostmapCallback(const nav_msgs::OccupancyGrid& msg);
void odomCallback(const nav_msgs::Odometry& msg);
void planningTimeCallback(const std_msgs::Float64& msg);
void planningTimeGlobalCallback(const std_msgs::Float64& msg);


// ################################### MAIN ###################################
int main(int argc, char** argv ) {
	ros::init(argc, argv, "seb_path_analysis");
	ros::NodeHandle n("~");
	ROS_INFO("Starting seb Path Analysis Node!");

	//ros::Subscriber subscriber1 = n.subscribe("/move_base/ThetaStarGlobalPlannerROS/plan", 1, &PathACallback);
	ros::Subscriber subscriber1 = n.subscribe("/move_base/AStarGlobalPlannerROS/plan", 1, &PathACallback);
	
	ros::Subscriber subscriber2 = n.subscribe("/move_base/global_costmap/costmap", 1, &globalCostmapCallback);
	ros::Subscriber subscriber3 = n.subscribe("/odom", 1, &odomCallback);
	ros::Subscriber subscriber4 = n.subscribe("/move_base/planning_time", 1, &planningTimeCallback);
	ros::Subscriber subscriber5 = n.subscribe("/move_base/planning_time_global", 1, &planningTimeGlobalCallback);
	
	tfBuffer = new tf2_ros::Buffer;
	tf2_ros::TransformListener listener(*tfBuffer);
	
	detectedObjectParser = new DetectedObjectParser("/seb_mock_object_publisher/detected_objects", "/seb_mock_object_publisher/tracked_objects");
	
	// Create action client
	MoveBaseClient actionClient("move_base", true);

	//wait for the action server to come up
	ROS_INFO("Waiting for the move_base action server to come up");
	while(!actionClient.waitForServer(ros::Duration(5.0))) {
	}

	// Sleep for 3 sec 
	ros::Duration d;
	d.fromSec(5);
	d.sleep();

	move_base_msgs::MoveBaseGoal goal;
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = finalGoalX;
	goal.target_pose.pose.position.y = finalGoalY;
	goal.target_pose.pose.orientation.x = 0.0;
	goal.target_pose.pose.orientation.y = 0.0;
	goal.target_pose.pose.orientation.z = 0.7071068;
	goal.target_pose.pose.orientation.w = 0.7071068;
	
	// Send Goal
	ROS_ERROR("GO!");
	ros::WallTime t0 = ros::WallTime::now();
	ros::WallTime t1;
	traveledDistance = 0;
	traveledCost = 0;
	planningTimes.reserve(1000);
	
	actionClient.sendGoal(goal);
	
	double finalDistance = 0;
	double finalCost = 0;	
	double finalTime = 0;
	
	while(ros::ok()) {		
		ros::spinOnce();
		
		if(goalReached) {
			t1 = ros::WallTime::now();
			finalTime = (t1 - t0).toSec();
			finalDistance = traveledDistance;
			finalCost = traveledCost;			
			
			ROS_ERROR("FINAL GOAL REACHED!");
			break;
		}
	}
	
	// ======= Calculate Planning Time mean + std-dev
	double sum = std::accumulate(planningTimes.begin(), planningTimes.end(), 0.0);
	double mean = sum / planningTimes.size();

	std::vector<double> diff(planningTimes.size());
	std::transform(planningTimes.begin(), planningTimes.end(), diff.begin(), std::bind2nd(std::minus<double>(), mean));
	double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	double stdev = std::sqrt(sq_sum / planningTimes.size());

	
	calculateGlobalPlannerMeanStdev();

	double globalPlanMeanDistance = globalPlanTotalDistance / globalPlanTotalEntries;
	double globalPlanMeanCost = globalPlanTotalCost / globalPlanTotalEntries;
	
	double closestToHuman = 99999;
	
	for(double v : distanceToHumansData) {
		closestToHuman = std::min(closestToHuman, v);
	}
		
	// Write to file
	/* Time
	 * Distance
	 * Cost
	 * ConvergenceTime Mean
	 * ConvergenceTime Stdev
	 * Global Planner Time Mean
	 * Global Planner Rolling Window Stdev
	 * Global Planner (avg 5 max) Time
	 * Global Plan Mean Distance
	 * Global Plan Mean Cost
	 * Closest to Human
	 */
	
	/* speed
	 * maxAllowedSpeed
	 * DistanceToHuman
	 * */
	
	std::stringstream s;
	s << std::fixed << std::setprecision(4) << finalTime   << " \t " << std::fixed << std::setprecision(4) << finalDistance << " \t "
	  << std::fixed << std::setprecision(4) << finalCost   << " \t " << std::fixed << std::setprecision(4) << mean << " \t " << std::fixed << std::setprecision(4) << stdev << " \t " 
	  << std::fixed << std::setprecision(4) << globalPlannerMean << " \t " << std::fixed << std::setprecision(4) << globalPlannerStdev << " \t "
	  << std::fixed << std::setprecision(4) << globalPlannerMaxTime << " \t " 
	  << std::fixed << std::setprecision(4) << globalPlanMeanDistance << " \t " << std::fixed << std::setprecision(4) << globalPlanMeanCost << " \t "
	  << std::fixed << std::setprecision(4) << closestToHuman << "\n";
	
	std::ofstream outfile;
	outfile.open("/root/catkin_ws/evaluation/eval.txt", std::ios_base::app); // append instead of overwrite
	outfile << s.str();
	outfile.close();
	
	// Speed / Human data
	std::stringstream ss;
	
	for(const double value : speedData) {
		ss << std::fixed << std::setprecision(4) << value << " ";	
	}
	ss << "\n";
	for(const double value : maxAllowedSpeedData) {
		ss << std::fixed << std::setprecision(4) << value << " ";
	}
	ss << "\n";
	for(const double value : distanceToHumansData) {
		ss << std::fixed << std::setprecision(4) << value << " ";
	}
	ss << "\n\n";
	
	std::ofstream outfileo;
	outfileo.open("/root/catkin_ws/evaluation/eval_human.txt", std::ios_base::app); // append instead of overwrite
	outfileo << ss.str();
	outfileo.close();
	
	ros::requestShutdown();
	ros::shutdown();
	
	return 0;
}

void calculateGlobalPlannerMeanStdev() {
	std::string s;
	for(double d : planningTimesGlobal) {
		s += std::to_string(d) + ",  ";
	}
	//ROS_WARN("\n%s\n", s.c_str());
			
	double sumTotal = std::accumulate(planningTimesGlobal.begin(), planningTimesGlobal.end(), 0.0);
	double meanTotal = sumTotal / planningTimesGlobal.size();
	ROS_WARN("Overall Mean: %f", meanTotal);
	
	int windowSize = 5;

	globalPlannerMaxTime = 0;
	for(int i = 0; i < 5; i++) {
		globalPlannerMaxTime += planningTimesGlobal[i];
	}
	globalPlannerMaxTime = globalPlannerMaxTime / 5.0;
	ROS_WARN("Global Plan max time average of five: %f", globalPlannerMaxTime);
	
	double totalVariance = 0;
	int countVarianceEntries = 0;
	
	// i is end of window and is the last valid entry
	std::vector<double> differences(windowSize);
	for(int i = windowSize - 1; i < planningTimesGlobal.size(); i++) {
		int startIndex = i - (windowSize - 1);
		int endIndex = i;
		
		double mean = std::accumulate(&planningTimesGlobal[startIndex], &planningTimesGlobal[endIndex], 0.0) / windowSize;		
		std::transform(&planningTimesGlobal[startIndex], &planningTimesGlobal[endIndex], differences.begin(), std::bind2nd(std::minus<double>(), mean));
		double squaredSum = std::inner_product(differences.begin(), differences.end(), differences.begin(), 0.0);
		double variance =  squaredSum / windowSize;
		
		totalVariance += variance;
		countVarianceEntries++;
	}
	
	double meanTotalVariance = totalVariance / countVarianceEntries;

	globalPlannerMean = meanTotal;
	globalPlannerStdev = std::sqrt(meanTotalVariance);
	
	ROS_WARN("Rolling Window Stdev: %f", globalPlannerStdev);
}

void planningTimeCallback(const std_msgs::Float64& msg) {
	if(prevX < -100 || traveledDistance <= 0.0 || goalReached) {
		return;
	}
	
	planningTimes.emplace_back(msg.data);
}

void planningTimeGlobalCallback(const std_msgs::Float64& msg) {
	if(prevX < -100 || traveledDistance <= 0.0 || goalReached) {
		return;
	}

	planningTimesGlobal.emplace_back(msg.data);
}

void odomCallback(const nav_msgs::Odometry& msg) {
	if(prevX < -100) {
		prevX = msg.pose.pose.position.x; 
		prevY = msg.pose.pose.position.y; 
		prevTheta = toAngle(msg.pose.pose.orientation);
		prevTimeStamp = msg.header.stamp.toSec();
		actualPrevTimeStamp = prevTimeStamp;
		return;
	}
	
	// Kept for backwards-compatibility Only process every 0.05 sec
	if(msg.header.stamp.toSec() < prevTimeStamp + 0.05) {
		return;
	}	
	
	double currX = msg.pose.pose.position.x;
	double currY = msg.pose.pose.position.y;
	double currTheta = toAngle(msg.pose.pose.orientation);
	
	double arcDistance = getArcDistance(prevX, prevY, prevTheta, currX, currY, currTheta);
	double costExact = globalMap.getCostmapCostBetweenExactPositions(prevX, prevY, currX, currY);
	
	traveledDistance += arcDistance;
	traveledCost += costExact;

	///////////////////////////////////////
	// Calculate speed
	double currTimeStamp = msg.header.stamp.toSec();
	if(currTimeStamp >= actualPrevTimeStamp + 0.001) {
		double deltaT = currTimeStamp - actualPrevTimeStamp;
		speedData.push_back(arcDistance / deltaT);
		actualPrevTimeStamp = currTimeStamp;

		// Calculate distance to humans
		std::vector<std::unique_ptr<DetectedObject>> detectedObjects = detectedObjectParser->getTransformedObjects(tfBuffer, "map", ros::Duration(0.5));
		double minDistToHuman = 99999;
		double maxAllowedVel = 0.5; // Hardcoded
		
		for(const std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
			switch(detectedObject->getObjectType()) {
				case DetectedObject::ObjectType::Person: {
					DetectedPerson* person = reinterpret_cast<DetectedPerson*>(detectedObject.get());
					double distance = getDistance(currX, currY, person->pose.pose.position.x, person->pose.pose.position.y);				

					double ratio;
					if(minDistToHuman < person->lowerDistanceBound) {
						ratio = person->velocityReduction;
					} else if(minDistToHuman > person->upperDistanceBound) {
						ratio = 1.0;
					} else {
						ratio = (minDistToHuman - person->lowerDistanceBound) / (person->upperDistanceBound - person->lowerDistanceBound); // 0 -> completely inner, 1 -> completely outer
						ratio = person->velocityReduction + ratio * (1.0 - person->velocityReduction);
					}

					maxAllowedVel = std::min(maxAllowedVel, 0.5 * ratio);					
					minDistToHuman = std::min(minDistToHuman, distance);					
					
					break;
				}
				case DetectedObject::ObjectType::AGV:
					break;
				case DetectedObject::ObjectType::Corridor:
					break;
			}
		}
		
		distanceToHumansData.push_back(minDistToHuman);
		maxAllowedSpeedData.push_back(maxAllowedVel);
	}
	
	///////////////////////////////////////
	
	prevX = currX;
	prevY = currY;
	prevTheta = currTheta;	

	// Check goal reached
	double goalX = finalGoalX;
	double goalY = finalGoalY;	
	
	double dist = std::sqrt((currX - goalX) * (currX - goalX) + (currY - goalY) * (currY - goalY));
	
	if(dist <= 0.15) {
		goalReached = true;
	}	
}

void PathACallback(const nav_msgs::Path& msg) {
	if(goalReached) {
		return;
	}
	
	if(msg.header.frame_id != "map") {
		ROS_ERROR("Received path which was not in \"map\" frame but in %s", msg.header.frame_id.c_str());
	}
	
	if(msg.poses.empty() || msg.poses.size() < 2) {
		return;
	}
	
	double distance = 0;
	double arcDistance = 0; 
	double costCellCenter = 0;
	double costExact = 0;
	 
	// Check pose pairs [i, i+1]
	for(int i = 0; i < msg.poses.size() - 1; i++) {
		double x1 = msg.poses[i].pose.position.x;
		double y1 = msg.poses[i].pose.position.y;
		double theta1 = toAngle(msg.poses[i].pose.orientation);

		double x2 = msg.poses[i + 1].pose.position.x;
		double y2 = msg.poses[i + 1].pose.position.y;
		double theta2 = toAngle(msg.poses[i + 1].pose.orientation);
		
		distance += getDistance(x1, y1, x2, y2);
		arcDistance += getArcDistance(x1, y1, theta1, x2, y2, theta2);
		costCellCenter += globalMap.getCostmapCostBetween(x1, y1, x2, y2);
		costExact += globalMap.getCostmapCostBetweenExactPositions(x1, y1, x2, y2);
	}
	
	globalPlanTotalCost += costExact;
	globalPlanTotalDistance += distance;
	globalPlanTotalEntries++;
	
	//ROS_INFO("Path (%3zu nodes) distance: %6.3f m, arcDistance: %6.3f m, costExact: %6.3f, costCellCenter: %6.3f", msg.poses.size(), distance, arcDistance, costExact, costCellCenter);
}

void globalCostmapCallback(const nav_msgs::OccupancyGrid& msg) {
	if(msg.header.frame_id != "map") {
		ROS_ERROR("Received global costmap which was not in \"map\" frame but in %s", msg.header.frame_id.c_str());
	}
	
	//ROS_WARN("Received map %d x %d at %f m/pix", msg.info.width, msg.info.height, msg.info.resolution);
	globalMap.copyData(msg.info.width, msg.info.height, msg.info.origin.position.x, msg.info.origin.position.y, msg.info.resolution, msg.data.data());
}