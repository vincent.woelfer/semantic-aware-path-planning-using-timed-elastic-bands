#include "semantic_elastic_bands/Visualization.h"

#include <ros/node_handle.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>
#include <ros/console.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

#include <semantic_elastic_bands/TrajectoryPointMsg.h>
#include <semantic_elastic_bands/TrajectoryMsg.h>

#include <utility>

namespace semantic_elastic_bands {

Visualization::Visualization() :
		isInitialized(false)
{
}

void semantic_elastic_bands::Visualization::initialize(ros::NodeHandle& nodeHandle, std::string localCostmapFrame_) {
	if(isInitialized) {
		ROS_WARN("SEB-Visualization is already initialized. Reinitializing...");
	}

	this->localCostmapFrame = std::move(localCostmapFrame_);
	
	// Register topics
	sebPlanPublisher = nodeHandle.advertise<nav_msgs::Path>("seb_plan", 1, true);
	sebPublisher = nodeHandle.advertise<visualization_msgs::MarkerArray>("seb", 1, true);
	initialPlanPublisher = nodeHandle.advertise<nav_msgs::Path>("seb_initial_plan", 1, true);
	preOptimizationPlanPublisher = nodeHandle.advertise<nav_msgs::Path>("seb_pre_optimization", 1, true);
	trajectoryDataPublisher = nodeHandle.advertise<semantic_elastic_bands::TrajectoryMsg>("seb_trajectory_data", 1, true);

	isInitialized = true;
}

void Visualization::publishSemanticElasticBandPlan(const SemanticElasticBand& seb) const {
	ROS_ASSERT(isInitialized);
	
	nav_msgs::Path msg;
	msg.header.frame_id = localCostmapFrame;
	msg.header.stamp = ros::Time::now();
	
	geometry_msgs::PoseStamped poseStamped;
	poseStamped.header.frame_id = msg.header.frame_id;
	poseStamped.header.stamp = msg.header.stamp;

	for(const auto& pose : seb.getTransformedPoses()) {
		poseStamped.pose.position.x = pose.position.x();
		poseStamped.pose.position.y = pose.position.y();
		poseStamped.pose.position.z = 0.0;
		poseStamped.pose.orientation = tf::createQuaternionMsgFromYaw(pose.theta);

		msg.poses.push_back(poseStamped);
	}

	sebPlanPublisher.publish(msg);
}

void Visualization::publishSemanticElasticBand(const SemanticElasticBand& seb) const {
	ROS_ASSERT(isInitialized);
	
	//double scaleLines = 0.025;
	double scaleDots = 0.025;
	double scaleLines = 0.035;
	
	visualization_msgs::MarkerArray msg;
	
	// Lines with speed as color
	visualization_msgs::Marker linesMarker;
	linesMarker.header.frame_id = localCostmapFrame;
	linesMarker.header.stamp = ros::Time::now();
	linesMarker.ns = "seb_lin";
	linesMarker.type = visualization_msgs::Marker::LINE_LIST;
	linesMarker.action = visualization_msgs::Marker::ADD;
	linesMarker.scale.x = scaleLines;
	linesMarker.pose.orientation.x = 0;
	linesMarker.pose.orientation.y = 0;
	linesMarker.pose.orientation.z = 0;
	linesMarker.pose.orientation.w = 1.0;
	linesMarker.color.a = 1.0;

	// Dots with rotational speed as color
	visualization_msgs::Marker dotsMarker;
	dotsMarker.header.frame_id = localCostmapFrame;
	dotsMarker.header.stamp = ros::Time::now();
	dotsMarker.ns = "seb_rot";
	dotsMarker.type = visualization_msgs::Marker::POINTS;
	dotsMarker.action = visualization_msgs::Marker::ADD;
	dotsMarker.scale.x = scaleDots;
	dotsMarker.scale.y = scaleDots;	
	dotsMarker.pose.orientation.x = 0;
	dotsMarker.pose.orientation.y = 0;
	dotsMarker.pose.orientation.z = 0;
	dotsMarker.pose.orientation.w = 1.0;
	dotsMarker.color.a = 1.0;
	
	std::vector<double> linearVelocities = seb.getLinearVelocities();
	std::vector<double> rotationalVelocities = seb.getRotationalVelocities();
	const std::vector<Pose>& transformedPoses = seb.getTransformedPoses();

	for(int i = 0; i < transformedPoses.size() - 1; i++) {
		geometry_msgs::Point a;
		a.x = transformedPoses.at(i).position.x();
		a.y = transformedPoses.at(i).position.y();
		a.z = 0;
		
		geometry_msgs::Point b;
		b.x = transformedPoses.at(i + 1).position.x();
		b.y = transformedPoses.at(i + 1).position.y();
		b.z = 0;
		
		linesMarker.points.push_back(a);		
		linesMarker.points.push_back(b);

		geometry_msgs::Point center;
		Pose avg = Pose::constructAverage(transformedPoses.at(i), transformedPoses.at(i + 1));
		center.x = avg.position.x();
		center.y = avg.position.y();
		dotsMarker.points.push_back(center);

		std_msgs::ColorRGBA linSpeedColor;
		if(linearVelocities.at(i) >= 0) {
			// Forward
			linSpeedColor = speedToColor(linearVelocities.at(i), seb.getConfig().robot.maxVelX);
		} else {
			// Backward
			linSpeedColor = speedToColorBackwards(std::abs(linearVelocities.at(i)), seb.getConfig().robot.maxVelX);
		}		
 
		linesMarker.colors.push_back(linSpeedColor);
		linesMarker.colors.push_back(linSpeedColor);

		std_msgs::ColorRGBA rotSpeedColor = speedToColor(std::abs(rotationalVelocities.at(i)), seb.getConfig().robot.maxVelTheta);
		dotsMarker.colors.push_back(rotSpeedColor);		
	}
	msg.markers.push_back(linesMarker);
	//msg.markers.push_back(dotsMarker);	
	
	sebPublisher.publish(msg);
}

void Visualization::publishInitialPath(const std::vector<geometry_msgs::PoseStamped>& initialPath) const {
	ROS_ASSERT(isInitialized);
	
	nav_msgs::Path msg;
	msg.header.frame_id = localCostmapFrame;
	msg.header.stamp = ros::Time::now();
	
	msg.poses.reserve(initialPath.size());

	for(const auto & pose : initialPath) {
		msg.poses.push_back(pose);
		msg.poses.back().header.stamp = msg.header.stamp;
	}
	
	initialPlanPublisher.publish(msg);
}

void Visualization::publishSemanticElasticBandPreOptimization(const SemanticElasticBand& seb) const {
	ROS_ASSERT(isInitialized);

	nav_msgs::Path msg;
	msg.header.frame_id = localCostmapFrame;
	msg.header.stamp = ros::Time::now();

	geometry_msgs::PoseStamped poseStamped;
	poseStamped.header.frame_id = msg.header.frame_id;
	poseStamped.header.stamp = msg.header.stamp;

	for(const auto& pose : seb.getTransformedPoses()) {
		poseStamped.pose.position.x = pose.position.x();
		poseStamped.pose.position.y = pose.position.y();
		poseStamped.pose.position.z = 0.0;
		poseStamped.pose.orientation = tf::createQuaternionMsgFromYaw(pose.theta);

		msg.poses.push_back(poseStamped);
	}

	preOptimizationPlanPublisher.publish(msg);
}

void Visualization::publishTrajectoryData(const SemanticElasticBand& seb) const {
	ROS_ASSERT(isInitialized);
	
	TrajectoryMsg msg;
	msg.header.frame_id = localCostmapFrame;
	msg.header.stamp = ros::Time::now();

	// Velocity i is between pose i and i+1 
	std::vector<double> linearVelocities = seb.getLinearVelocities();
	std::vector<double> rotationalVelocities = seb.getRotationalVelocities();
	const std::vector<Pose>& transformedPoses = seb.getTransformedPoses();
	
	double currentTime = 0;
	
	// Start
	TrajectoryPointMsg point;
	point.time_from_start.fromSec(currentTime);
	point.pose = transformedPoses.front().toPoseMsg();
	point.velocity.linear.y = 0;
	point.velocity.linear.z = 0;
	point.velocity.angular.x = 0;
	point.velocity.angular.y = 0;
	
	FixedVelocityConstraint startVel = seb.getVelocityConstraintStart();
	if(startVel.isFixed) {
		point.velocity.linear.x = startVel.velX;
		point.velocity.angular.z = startVel.velTheta;	
	} else {
		point.velocity.linear.x = linearVelocities.at(0);
		point.velocity.angular.z = rotationalVelocities.at(0);
	}	
	
	msg.trajectory.push_back(point);
	currentTime += seb.getTimeDiffs().front();	
	
	for(int i = 1; i < transformedPoses.size() - 1; i++) {
		point.time_from_start.fromSec(currentTime);
		point.pose = transformedPoses.at(i).toPoseMsg();
		point.velocity.linear.x = 0.5 * (linearVelocities.at(i - 1) + linearVelocities.at(i));
		point.velocity.linear.y = 0;
		point.velocity.linear.z = 0;
		point.velocity.angular.x = 0;
		point.velocity.angular.y = 0;
		point.velocity.angular.z = 0.5 * (rotationalVelocities.at(i - 1) + rotationalVelocities.at(i));
		
		msg.trajectory.push_back(point);
		currentTime += seb.getTimeDiffs().at(i);		
	}
	
	// End
	point.time_from_start.fromSec(currentTime);
	point.pose = transformedPoses.back().toPoseMsg();
	point.velocity.linear.y = 0;
	point.velocity.linear.z = 0;
	point.velocity.angular.x = 0;
	point.velocity.angular.y = 0;

	FixedVelocityConstraint goalVel = seb.getVelocityConstraintGoal();
	if(goalVel.isFixed) {
		point.velocity.linear.x = goalVel.velX;
		point.velocity.angular.z = goalVel.velTheta;
	} else {
		point.velocity.linear.x = linearVelocities.back();
		point.velocity.angular.z = rotationalVelocities.back();
	}	
	
	msg.trajectory.push_back(point);
	
	trajectoryDataPublisher.publish(msg);	
}

void Visualization::publishEmptySeb() const {
	ROS_ASSERT(isInitialized);

	// Empty seb path message
	nav_msgs::Path msgPath;
	msgPath.header.frame_id = localCostmapFrame;
	msgPath.header.stamp = ros::Time::now();
	sebPlanPublisher.publish(msgPath);
	
	// Empty seb message
	visualization_msgs::MarkerArray msgSeb;
	
	// Lines with speed as color
	visualization_msgs::Marker linesMarker;
	linesMarker.header.frame_id = localCostmapFrame;
	linesMarker.header.stamp = ros::Time::now();
	linesMarker.ns = "seb_lin";
	linesMarker.type = visualization_msgs::Marker::LINE_LIST;
	linesMarker.action = visualization_msgs::Marker::DELETEALL;

	// Dots with rotational speed as color
	visualization_msgs::Marker dotsMarker;
	dotsMarker.header.frame_id = localCostmapFrame;
	dotsMarker.header.stamp = ros::Time::now();
	dotsMarker.ns = "seb_rot";
	dotsMarker.type = visualization_msgs::Marker::POINTS;
	dotsMarker.action = visualization_msgs::Marker::DELETEALL;
	
	msgSeb.markers.push_back(linesMarker);
	msgSeb.markers.push_back(dotsMarker);
	
	sebPublisher.publish(msgSeb);

	// Empty trajectory message
	TrajectoryMsg msgTrajectory;
	msgTrajectory.header.frame_id = localCostmapFrame;
	msgTrajectory.header.stamp = ros::Time::now();
	trajectoryDataPublisher.publish(msgTrajectory);
}

std_msgs::ColorRGBA Visualization::speedToColor(double speed, double maxSpeed, double alpha) {
	std_msgs:: ColorRGBA color;
	color.a = alpha;

	// Map from 0-blue to 1-red
	double max = std::max(maxSpeed, 0.025);
	double ratio = speed / max;

	color.g = 0.0;

	color.r = std::clamp(ratio, 0.0, 1.0);
	color.b = std::clamp(1.0 - ratio, 0.0, 1.0);

	return color;	
}

std_msgs::ColorRGBA Visualization::speedToColorBackwards(double speed, double maxSpeed, double alpha) {
	std_msgs::ColorRGBA color = speedToColor(speed, maxSpeed, alpha);
	color.g = 1.0;
	return color;
}

}