#include "semantic_elastic_bands/Costmap/SemanticLayer.h"

#include <pluginlib/class_list_macros.h>
#include <geometry_msgs/PointStamped.h>
#include <costmap_2d/costmap_2d.h>

#include "semantic_elastic_bands/Utility.h"

PLUGINLIB_EXPORT_CLASS(semantic_elastic_bands::SemanticLayer, costmap_2d::Layer)

namespace semantic_elastic_bands {

using costmap_2d::LETHAL_OBSTACLE;
using costmap_2d::INSCRIBED_INFLATED_OBSTACLE;
using costmap_2d::NO_INFORMATION;
using costmap_2d::FREE_SPACE;

SemanticLayer::SemanticLayer()
{
	layered_costmap_ = nullptr;
	parser = nullptr;
}

SemanticLayer::~SemanticLayer() 
{
	delete parser;
}

void SemanticLayer::onInitialize() {
	current_ = true;
	enabled_ = true;
	
	std::string localPlanTopic = "localPlanTopic";
	std::string detectedObjectsTopic = "detectedObjectsTopic";
	std::string trackedObjectsTopic = "trackedObjectsTopic";

	ros::NodeHandle nh("~/" + name_);	
	if(!nh.getParam("localPlanTopic", localPlanTopic)) {
		ROS_ERROR("Semantic Layer: Param localPlanTopic not set!");
	}
	if(!nh.getParam("detectedObjectsTopic", detectedObjectsTopic)) {
		ROS_ERROR("Semantic Layer: Param detectedObjectsTopic not set! No detected objects are taken into account!");
	}
	if(!nh.getParam("trackedObjectsTopic", trackedObjectsTopic)) {
		ROS_ERROR("Semantic Layer: Param trackedObjectsTopic not set! No tracked objects are taken into account!");
	}

	localPlanSubscriber = nh.subscribe(localPlanTopic, 1, &SemanticLayer::localPlanCallback, this);
	
	parser = new DetectedObjectParser(detectedObjectsTopic, trackedObjectsTopic);

	isPoseRobotValid = false;
}

void SemanticLayer::updateBounds(double robot_x, double robot_y, double robot_yaw, double* min_x, double* min_y, double* max_x, double* max_y) {
	if(!enabled_) {
		return;
	}
	std::scoped_lock<std::mutex> lock(mutex);

	// Update and convert robot pose if current pose not valid
	if(!isPoseRobotValid) {
		convertPoseToCellSpaceNoBounds(layered_costmap_->getCostmap(), robot_x, robot_y, robot_yaw, robotPoseCellSpace);
		isPoseRobotValid = true;
	}
	
	detectedObjects.clear();
	detectedObjects = parser->getTransformedObjects(tf_, layered_costmap_->getGlobalFrameID(), ros::Duration(0.1));

	// Transform to cell space and update bounds
	for(std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
		detectedObject->convertToCellSpace(layered_costmap_->getCostmap());
		detectedObject->updateCostmapBounds(min_x, min_y, max_x, max_y);
	}
}

void SemanticLayer::updateCosts(costmap_2d::Costmap2D& master_grid, int min_i, int min_j, int max_i, int max_j) {
	if(!enabled_ || detectedObjects.empty()) {
		return;
	}
	std::scoped_lock<std::mutex> lock(mutex);
	ros::WallTime t0 = ros::WallTime::now();

	costmap_2d::Costmap2D* costmap = layered_costmap_->getCostmap();

	constexpr int maxSemanticCost = INSCRIBED_INFLATED_OBSTACLE - 1;

	for(std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
		if(isPoseRobotValid && detectedObject->skipBasedOnRobotPose(robotPoseCellSpace)) {
			continue;
		}
		
		// Get update area in cell space
		int minX = min_i;
		int minY = min_j;
		int maxX = max_i;
		int maxY = max_j;		
		detectedObject->getCostmapUpdateArea(costmap->getResolution(), minX, minY, maxX, maxY);
		
		// Skip if area is empty/negative
		if(minX > maxX || minY > maxY) {
			continue;
		}
			
		Point p;
		for(p.x = minX; p.x < maxX; p.x++) {
			for(p.y = minY; p.y < maxY; p.y++) {
				unsigned char cost = detectedObject->getCellCost(costmap->getResolution(), p);
				
				if(cost == NO_INFORMATION || cost == 0) {
					continue;
				} else if(cost == LETHAL_OBSTACLE || cost == INSCRIBED_INFLATED_OBSTACLE) {
					costmap->setCost(p.x, p.y, cost);
				} else {
					unsigned char oldCost = costmap->getCost(p.x, p.y);
					
					if(oldCost == NO_INFORMATION || oldCost == LETHAL_OBSTACLE || oldCost == INSCRIBED_INFLATED_OBSTACLE) {
						continue;
					}
					
					// Additive cost
					//unsigned char newCost = static_cast<unsigned char>(std::clamp(cost + oldCost, 0, maxSemanticCost));
					
					// Max cost
					unsigned char newCost = static_cast<unsigned char>(std::max(oldCost, cost));
                    costmap->setCost(p.x, p.y, newCost);
				}					
			}
		}		
	}
	
	ros::WallTime t1 = ros::WallTime::now();
	double timeMs = (t1 - t0).toNSec() * 1e-6; 
	ROS_DEBUG("Updated semantic cost-map in %5.2f ms", timeMs);	
}

void SemanticLayer::localPlanCallback(const nav_msgs::Path& msg) {
	if(msg.poses.empty()) {
		isPoseRobotValid = false;
		return;
	}

	if(layered_costmap_->getGlobalFrameID() != msg.header.frame_id) {
		ROS_ERROR("Received local plan in '%s' frame but semantic layer has the global frame '%s'! Local plan cant be used!", msg.header.frame_id.c_str(), layered_costmap_->getGlobalFrameID().c_str());
		isPoseRobotValid = false;
		return;
	}
	
	// Estimate near-future robot orientation	
	constexpr int desiredNumPoses = 5;
	int index = std::min(desiredNumPoses, std::max(static_cast<int>(msg.poses.size() - 1), 0));
	const geometry_msgs::Pose& pose = msg.poses.at(index).pose;
	
	convertPoseToCellSpaceNoBounds(layered_costmap_->getCostmap(), pose.position.x, pose.position.y, toAngle(pose.orientation), robotPoseCellSpace);
	isPoseRobotValid = true;
}

}