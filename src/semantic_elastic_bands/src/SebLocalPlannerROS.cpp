#include "semantic_elastic_bands/SebLocalPlannerROS.h"

// pluginlib macros
#include <pluginlib/class_list_macros.h>

#include "semantic_elastic_bands/Utility.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedPerson.h"
#include <std_msgs/Float64.h>


// register this planner as a BaseLocalPlanner plugin
PLUGINLIB_EXPORT_CLASS(semantic_elastic_bands::SebLocalPlannerROS, nav_core::BaseLocalPlanner)

namespace semantic_elastic_bands {

using costmap_2d::Costmap2D;
using costmap_2d::Costmap2DROS;

using geometry_msgs::Twist;
using geometry_msgs::PoseStamped;
using geometry_msgs::TransformStamped;

SebLocalPlannerROS::SebLocalPlannerROS() :
		isInitialized(false),
		currentState(State::GoalReached),
		forceBandReinitializationNextIteration(true),
		planner(nullptr),
		costmapRos(nullptr),
		costmap(nullptr),
		tf(nullptr)		
{
}

SebLocalPlannerROS::~SebLocalPlannerROS() {
	delete planner;
	delete detectedObjectParser;
}

void SebLocalPlannerROS::initialize(std::string name, tf2_ros::Buffer* tf_, costmap_2d::Costmap2DROS* costmapRos_) {
	if(isInitialized) {
		ROS_WARN("seb_local_planner has already been initialized!");
		return;
	}

	// create Node Handle with name of plugin (as used in move_base for loading)
	ros::NodeHandle nh("~/" + name);

	// Fetch config 
	config.loadFromRosNodeHandle(nh);

	// Initialize transforms and frame data
	tf = tf_;
	costmapRos = costmapRos_;
	costmap = costmapRos->getCostmap();
	localCostmapFrame = costmapRos->getGlobalFrameID();

	// Setup dynamic reconfigure
	dynamicReconfigure = boost::make_shared<dynamic_reconfigure::Server<SebLocalPlannerReconfigureConfig>>(nh);
	dynamicReconfigure->setCallback(boost::bind(&SebLocalPlannerROS::reconfigureCallback, this, _1, _2));

	// Check parameters
	config.checkParameterValidity();
	config.checkParameterValidityRegardingCostmap(costmap);
		
	// Init odometry helper to receive robots velocity
	std::string odomTopic = config.odomTopic;
	odometryHelper.setOdomTopic(odomTopic);
	
	planner = new Planner(nh, config, costmap, localCostmapFrame);
	detectedObjectParser = new DetectedObjectParser(config.detectedObjectsTopic, config.trackedObjectsTopic);

	ros::NodeHandle handle("/move_base/");
	planningTimePublisher = handle.advertise<std_msgs::Float64>("planning_time", 50, true);

	ROS_INFO("Initialized seb_local_planner");
	isInitialized = true;
}

bool SebLocalPlannerROS::computeVelocityCommands(Twist& cmd_vel) {
	if(!isInitialized) {
		ROS_ERROR("seb_local_planner has not been initialized!");
		return false;
	}

	ros::WallTime t0 = ros::WallTime::now();
	
	// Prepare cmd_vel default message
	cmd_vel.linear.x = 0.0;
	cmd_vel.linear.y = 0.0;
	cmd_vel.linear.z = 0.0;
	cmd_vel.angular.x = 0.0;
	cmd_vel.angular.y = 0.0;
	cmd_vel.angular.z = 0.0;
	
	if(currentState == State::GoalReached) {
		return true;
	}
	
	// Lock config to prevent changes through dynamic config. This lock automatically releases when going out of scope
	std::scoped_lock<std::mutex> lock(config.getConfigMutex());	

	// Get most recent transform data
	if(!updateRobotPositionData()) {
		ROS_ERROR("Unable to receive up to date robot pose, not computing any velocity commands!");
		return false;
	}

	// Remove parts of global plan the robot has already passed
	pruneGlobalPlan();

	// Transform global plan into local costmap frame and discard parts outside of costmap (to avoid out-of-bounds access later on)
	std::vector<PoseStamped> transformedPlan;
	bool isFinalGoal;
	std::tie(transformedPlan, isFinalGoal) = transformGlobalPlanToCostmapFrame();
	
	if(transformedPlan.empty()) {
		ROS_ERROR("Could not transform global plan to planner frame!");
		return false;
	}
	
	// Check if we can update our state because we have reached the global goal
	if(isFinalGoal) {
		updatePlanningStatus(transformedPlan.back());
	}

	if(currentState == State::GoalReached) {
		return true;
	}
	if(currentState == State::OnlyRotationRequired) {
		double orientationDifferenceDeg = rad2deg(getAngleDifference(toAngle(robotPose.pose.orientation), toAngle(transformedPlan.back().pose.orientation))); 
		double rotationSpeedPercentage = std::clamp(std::abs(orientationDifferenceDeg) / 25.0, 0.2, 1.0);		
		cmd_vel.angular.z = sign(orientationDifferenceDeg) * config.robot.maxVelTheta * rotationSpeedPercentage;

		if(config.isVerbosityAtLeastNormal()) {
			ROS_INFO("Rotating on goal, difference left: %4.1f deg", orientationDifferenceDeg);	
		}
		
		return true;		
	}
	// -> Driving required
	
	// Get updated detected obstacles
	updateMaximumVelocity(detectedObjectParser->getTransformedObjects(tf, localCostmapFrame));	
	planner->updateDetectedObjects(detectedObjectParser->getTransformedObjects(tf, localCostmapFrame));
	
	// Set the starting pose to the exact robots pose (in local costmap frame but meter scale). Add pose if only goal in plan
	if(transformedPlan.size() == 1) {
		transformedPlan.insert(transformedPlan.begin(), transformedPlan.front());
	}
	transformedPlan.front().pose = robotPose.pose;
	
	// Perform actual planning. The transformed plan is in the local costmap frame in meter-scale
	Planner::PlanningResult planningResult = planner->plan(transformedPlan, robotVelocity, isFinalGoal, forceBandReinitializationNextIteration);
	
	std_msgs::Float64 msg;
	msg.data = planner->prevPlanningTime;
	planningTimePublisher.publish(msg);
	
	if(planningResult == Planner::PlanningResult::FAILURE) {
		ROS_WARN("SebLocalPlanner: Could not plan trajectory, publishing stop command!");		
		return false;
	}
	forceBandReinitializationNextIteration = (planningResult == Planner::PlanningResult::USABLE_ONCE);
	
	// Get most recent velocity data
	updateRobotVelocityData();
	
	// Compute control signals
	double velX;
	double velTheta;
	planner->computeCmdVel(velX, velTheta);

	const double unlimitedVelX = velX;
	const double unlimitedVelTheta = velTheta;
	
	limitVelocities(velX, velTheta);
	
	if(config.robot.steeringAngleInsteadVelocity) {
		velTheta = convertAngularVelocityToSteeringAngle(velX, velTheta);
		
		if(!std::isfinite(velTheta)) {
			ROS_WARN("SebLocalPlanner: Could not compute finite steering angle, publishing stop command!");
			forceBandReinitializationNextIteration = true;
			return false;	
		}		
	}

	cmd_vel.linear.x = velX;
	cmd_vel.angular.z = velTheta;

	ros::WallTime t1 = ros::WallTime::now();
	
	if(config.isVerbosityAtLeastFull()) {
		if(config.robot.steeringAngleInsteadVelocity) {
			ROS_INFO("Velocity command in %4.1f ms | velX: %3.2f | Steering: %3.2f | sebX: %3.2f | sebT: %3.2f", (t1 - t0).toNSec() * 1e-6, velX, velTheta, unlimitedVelX, unlimitedVelTheta);	
		} else {
			ROS_INFO("Velocity command in %4.1f ms | velX: %3.2f | velT: %3.2f | sebX: %3.2f | sebT: %3.2f", (t1 - t0).toNSec() * 1e-6, velX, velTheta, unlimitedVelX, unlimitedVelTheta);	
		}		

		// Print empty line for better debugging readability
		ROS_INFO(" ");	
	}	

	return true;
}

bool SebLocalPlannerROS::isGoalReached() {
	return currentState == State::GoalReached;
}

bool SebLocalPlannerROS::setPlan(const std::vector<PoseStamped>& plan) {
	if(!isInitialized) {
		ROS_ERROR("seb_local_planner has not been initialized!");
		return false;
	}

	// Check if plans are similar enough to allow reusing the band
	if(!forceBandReinitializationNextIteration) {
		forceBandReinitializationNextIteration =  !isSimilarToGlobalPlan(plan);
		
		if(forceBandReinitializationNextIteration) {
			ROS_INFO("Got new global plan, forcing band reinitialization!");
		}
	}
	
	setCurrentState(State::DrivingRequired);	
	globalPlan = plan;	

	return true;
}

/* ========================== Private Methods ========================== */

bool SebLocalPlannerROS::updateRobotPositionData() {
	// Update robot pose
	bool updateSuccessful = costmapRos->getRobotPose(robotPose);

	if(!updateSuccessful){
		ROS_WARN("Could not update robot pose!");
	} else {
		ROS_DEBUG("SEB: Update robot pose = %f/%f", robotPose.pose.position.x, robotPose.pose.position.y);
	}

	return updateSuccessful;
}

bool SebLocalPlannerROS::updateRobotVelocityData() {
	// Update robot velocity
	PoseStamped odometry;
	odometryHelper.getRobotVel(odometry);
	robotVelocity.linear.x = odometry.pose.position.x;
	robotVelocity.linear.y = odometry.pose.position.y;
	robotVelocity.angular.z = tf2::getYaw(odometry.pose.orientation);

	return true;
}

std::pair<std::vector<PoseStamped>, bool> SebLocalPlannerROS::transformGlobalPlanToCostmapFrame() {
	std::vector<PoseStamped> transformedPlan;
	bool isFinalGoal = false;

	if(globalPlan.empty()) {
		ROS_ERROR("Cannot transform empty global plan!");
		return std::make_pair(transformedPlan, false);
	}

	// This checks which parts of the global plan are in the local costmap and transforms these
	try {
		// Get global plan to costmap transform
		const std::string planFrame = globalPlan[0].header.frame_id;
		const ros::Time planTime = globalPlan[0].header.stamp;
		TransformStamped planToCostmapTransform = tf->lookupTransform(localCostmapFrame, ros::Time::now(), planFrame, planTime, planFrame, ros::Duration(0.1));

		// Find first position in costmap and add it to transformed plan
		int i = 0;
		while(i < globalPlan.size()) {
			PoseStamped pose;
			tf2::doTransform(globalPlan.at(i++), pose, planToCostmapTransform);

			if(isPointInCostmap(pose.pose.position)) {
				transformedPlan.push_back(pose);
				break;
			}
		}
		
		// Only interpolate if the two points are at least two cells away (x1.5 to include diagonal connections)
		const double minDistanceForInterpolation = costmap->getResolution() * 1.5;

		// Limit plan length/duration based on config
		double planLength = 0.0;
		double planDuration = 0.0;		
		bool limitLength = config.planning.globalPlanMaxLookaheadDistance > 0.0;
		bool limitDuration = config.planning.globalPlanMaxLookaheadDuration > 0.0;

		// Now add until we are outside of local costmap again or plan exceeds max length
		while(i < globalPlan.size()) {
			bool previousPoseExists = !transformedPlan.empty();
			PoseStamped pose;
			tf2::doTransform(globalPlan.at(i++), pose, planToCostmapTransform);
			
			double distance = 0.0;
			double duration = 0.0;
			
			if(previousPoseExists) {
				distance = getDistance(transformedPlan.back().pose.position, pose.pose.position);
				duration = distance / config.robot.maxVelX;
			}
			
			bool pointIsOk = (!limitLength || planLength + distance <= config.planning.globalPlanMaxLookaheadDistance) &&
							 (!limitDuration || planDuration + duration <= config.planning.globalPlanMaxLookaheadDuration);

			if(pointIsOk && isPointInCostmap(pose.pose.position)) {
				planLength += distance;
				planDuration += duration;
				transformedPlan.push_back(pose);
				
				// Was this the final pose ? 
				if(i == globalPlan.size()) {
					isFinalGoal = true;
				}
				
				continue;
			}
			
			// Point is outside of costmap OR to far away (distance or duration wise)
			// => Try to interpolate but only if the two points are more than one cell apart
			if(distance >= minDistanceForInterpolation) {
				// Interpolate point between last in costmap and first to lie outside OR max planning length reached
				const PoseStamped& a = transformedPlan.back();
				const PoseStamped& b = pose;
				
				// Compute delta step
				const double stepDistance = minDistanceForInterpolation;
				const double interpolationDeltaFactor = stepDistance / distance;				
				geometry_msgs::Point delta;
				delta.x = (b.pose.position.x - a.pose.position.x) * interpolationDeltaFactor;
				delta.y = (b.pose.position.y - a.pose.position.y) * interpolationDeltaFactor;
				const double stepDuration = stepDistance / config.robot.maxVelX;
				
				// Start inside costmap
				PoseStamped interpolation = a;
				int numSuccessfulSteps = 0;
				
				while(true) {
					// Interpolate
					interpolation.pose.position.x += delta.x;
					interpolation.pose.position.y += delta.y;
					
					// Check if interpolation gave us a valid point
					pointIsOk = (!limitLength || planLength + stepDistance <= config.planning.globalPlanMaxLookaheadDistance) &&
					            (!limitDuration || planDuration + stepDuration <= config.planning.globalPlanMaxLookaheadDuration);	
					
					if(pointIsOk && isPointInCostmap(interpolation.pose.position)) {
						numSuccessfulSteps++;
						planLength += stepDistance;
						planDuration += stepDuration;
					} else {
						// Roll back to last successful step and abort
						interpolation.pose.position.x -= delta.x;
						interpolation.pose.position.y -= delta.y;
						break;
					}
				}
				
				// Only add if we had at least one successful step
				if(numSuccessfulSteps > 0) {
					// Interpolation should always point towards b
					double yaw = getAngleBetweenPoints(a.pose.position.x, a.pose.position.y, b.pose.position.x, b.pose.position.y);

					tf2::Quaternion orientation;
					orientation.setRPY(0.0, 0.0, yaw);
					orientation.normalize();
					interpolation.pose.orientation.x = orientation.x();
					interpolation.pose.orientation.y = orientation.y();
					interpolation.pose.orientation.z = orientation.z();
					interpolation.pose.orientation.w = orientation.w();

					transformedPlan.push_back(interpolation);
				}				
			}

			// Skip remaining global path as end criteria are met (it does not matter if we interpolated the last pose)
			break;
		}

		if(!transformedPlan.empty()) {
			ROS_DEBUG("SEB: Successfully transformed %zu/%zu poses from initial global plan from (plan) %s -> (local costmap) %s", transformedPlan.size(), globalPlan.size(), planFrame.c_str(), localCostmapFrame.c_str());
		} else {
			// Inject goal manually
			PoseStamped pose;
			tf2::doTransform(globalPlan.back(), pose, planToCostmapTransform);
			transformedPlan.push_back(pose);
			isFinalGoal = true;
			ROS_WARN("SEB: Tried to transform initial global path but no pose meet criteria! Inserting goal pose manually.");
		}
	}
	catch(const tf2::LookupException& ex) {
		ROS_ERROR("No Transform available Error: %s\n", ex.what());
		return std::make_pair(transformedPlan, false);
	}
	catch(const tf2::ConnectivityException& ex) {
		ROS_ERROR("Connectivity Error: %s\n", ex.what());
		return std::make_pair(transformedPlan, false);
	}
	catch(const tf2::ExtrapolationException& ex) {
		ROS_ERROR("Extrapolation Error: %s\n", ex.what());
		return std::make_pair(transformedPlan, false);
	}

	return std::make_pair(transformedPlan, isFinalGoal);
}

void SebLocalPlannerROS::pruneGlobalPlan() {
	if(globalPlan.size() <= 1) {
		return;
	}

	// This prunes the actual global plan. These changes persist between calls to computeVelocity
	try {
		// Try to transform robot pose into planning frame, no timeout
		geometry_msgs::TransformStamped robotToPlanTransform = tf->lookupTransform(globalPlan.front().header.frame_id, robotPose.header.frame_id, robotPose.header.stamp, ros::Duration(0));
		PoseStamped transformedRobotPose;
		tf2::doTransform(robotPose, transformedRobotPose, robotToPlanTransform);
		
		double pruneDistanceSquared = config.planning.globalPlanPruneDistance * config.planning.globalPlanPruneDistance;

		// Iterate through plan and mark poses for deletion until we find a pose near enough to keep
		auto iter = globalPlan.begin();
		auto eraseUntil = iter;

		while(iter != globalPlan.end()) {
			if(getDistanceSquared(transformedRobotPose.pose.position, iter->pose.position) < pruneDistanceSquared) {
				eraseUntil = iter;
				break;
			}
			iter++;
		}

		// Erase until iterator (unless it would erase the whole plan or there is nothing to erase)
		if(eraseUntil != globalPlan.end() && eraseUntil != globalPlan.begin()) {
			int sizeBefore = globalPlan.size();
			globalPlan.erase(globalPlan.begin(), eraseUntil);
			int sizeAfter = globalPlan.size();

			if(config.isVerbosityAtLeastNormal()) {
				ROS_INFO("Pruned %d poses already passed by the robot from global plan", sizeBefore - sizeAfter);
			}
		}
	}
	// Only catch generic TransformException here
	catch(const tf2::TransformException& ex) {
		ROS_WARN("Cannot prune global path since no transform is available: %s\n", ex.what());
		return;
	}
}

void SebLocalPlannerROS::updatePlanningStatus(const PoseStamped& finalGoal) {
	if(currentState == State::GoalReached) {
		return;
	}
	
	// Check if we have reached the local goal		
	double distanceToGoal = getDistance(robotPose.pose.position, finalGoal.pose.position);
	double orientationDifference = std::abs(rad2deg(getAngleDifference(toAngle(robotPose.pose.orientation), toAngle(finalGoal.pose.orientation))));
	
	bool goalReachedXY = distanceToGoal <= config.planning.goalToleranceXY;
	bool goalReachedTheta = orientationDifference <= config.planning.goalToleranceThetaDeg;
	
	if(goalReachedXY && goalReachedTheta) {
		setCurrentState(State::GoalReached);
	} else if(goalReachedXY && config.robot.minTurningRadius == 0.0) {
		setCurrentState(State::OnlyRotationRequired);
	} else {
		setCurrentState(State::DrivingRequired);
	}	
}

void SebLocalPlannerROS::limitVelocities(double& velX, double& velTheta) const {
	double currentMaxLinearVelocityBackwards = std::min(std::abs(config.robot.maxVelXBackwards), std::abs(currentMaxLinearVelocity));
	
	// Limit velocities initially
	velX = std::clamp(velX, -currentMaxLinearVelocityBackwards, currentMaxLinearVelocity);
	velTheta = std::clamp(velTheta, -config.robot.maxVelTheta, config.robot.maxVelTheta);	

	if(config.robot.limitAcceleration) {
		// Limit acceleration based on current velocity and controller frequency
		// Acceleration is always per tick, not per second
		double accX = (velX - robotVelocity.linear.x);
		double accTheta = (velTheta - robotVelocity.angular.z);

		double maxAccX = config.robot.maxAccX / config.controllerFrequency;
		double maxAccTheta = config.robot.maxAccTheta / config.controllerFrequency;
		
		if(std::abs(accX) > maxAccX) {
			velX = robotVelocity.linear.x + std::clamp(accX, -maxAccX, maxAccX);
		}

		if(std::abs(accTheta) > maxAccTheta) {
			velTheta = robotVelocity.angular.z + std::clamp(accTheta, -maxAccTheta, maxAccTheta);
		}

		// Limit velocities again
		velX = std::clamp(velX, -currentMaxLinearVelocityBackwards, currentMaxLinearVelocity);
		velTheta = std::clamp(velTheta, -config.robot.maxVelTheta, config.robot.maxVelTheta);	
	}	
}

double SebLocalPlannerROS::convertAngularVelocityToSteeringAngle(double velX, double velTheta) const {
	if(velX == 0.0 || velTheta == 0.0) {
		return 0;
	}

	const double minTurningRadius = config.robot.minTurningRadius * 0.95;
	
	double radius = velX/velTheta;

	if(std::abs(radius) < minTurningRadius) {
		radius = sign(radius) * minTurningRadius;
	}		

	return std::atan(config.robot.wheelbase / radius);
}

bool SebLocalPlannerROS::isPointInCostmap(const geometry_msgs::Point& point) const {
	return point.x >= costmap->getOriginX() + config.planning.localCostmapSafetyMarginInMeter &&
	       point.y >= costmap->getOriginY() + config.planning.localCostmapSafetyMarginInMeter &&
	       point.x <= costmap->getOriginX() + costmap->getSizeInMetersX() - config.planning.localCostmapSafetyMarginInMeter &&
	       point.y <= costmap->getOriginY() + costmap->getSizeInMetersY() - config.planning.localCostmapSafetyMarginInMeter;
}

void SebLocalPlannerROS::reconfigureCallback(SebLocalPlannerReconfigureConfig& cfg, uint32_t level) {
	config.reconfigure(cfg);
	
	config.checkParameterValidityRegardingCostmap(costmap);
}

void SebLocalPlannerROS::setCurrentState(SebLocalPlannerROS::State state) {
	if(currentState == state) {
		return;
	}
	
	if(state == State::OnlyRotationRequired && config.robot.minTurningRadius > 0.0) {
		ROS_ERROR("State changed to 'Only Rotation Required', however, minTurningRadius > 0 which means the robot can not turn on spot. This should never happen");
	}
	
	std::string name;
	switch(state) {
		case State::GoalReached:
			name = "GoalReached";
			break;
		case State::OnlyRotationRequired:
			name = "OnlyRotationRequired";
			break;
		case State::DrivingRequired:
			name = "DrivingRequired";
			break;
	}
	
	if(config.isVerbosityAtLeastNormal()) {
		ROS_INFO("=== Switching to state: %s ===", name.c_str());
	}
	currentState = state;
}

bool SebLocalPlannerROS::isSimilarToGlobalPlan(const std::vector<PoseStamped>& plan) const {
	if(globalPlan.empty() || plan.empty() || globalPlan.front().header.frame_id != plan.front().header.frame_id) {
		return false;
	}
	
	// Verify that goals are identical
	if(plan.back().pose != globalPlan.back().pose) {
		return false;
	}

	const double maxDist = config.planning.maxGlobalPlanReuseDistance;
	if(maxDist <= 0.0) {
		return false;
	}
	
	const double maxDistSquared = maxDist * maxDist;
	
	// If path contains many poses (e.g. coming from A-Star) this may take a while
	// Verify that each pose of the new plan is near enough to the existing plan
	for(const PoseStamped& poseStamped : plan) {
		bool isPoseNearGlobalPlan = false;
		
		// Check against each pose pair (i, i+1) of the existing global plan
		for(int i = 0; i < globalPlan.size() - 1; i++) {
			double distSquared = getDistanceToLineSegmentSquared(poseStamped.pose.position.x, poseStamped.pose.position.y,
								    		                     globalPlan.at(i).pose.position.x, globalPlan.at(i).pose.position.y,
										                         globalPlan.at(i + 1).pose.position.x, globalPlan.at(i + 1).pose.position.y);
			
			if(distSquared <= maxDistSquared) {
				isPoseNearGlobalPlan = true;
				break;
			}
		}
		
		if(!isPoseNearGlobalPlan) {
			return false;
		}
	}

	return true;	
}

void SebLocalPlannerROS::updateMaximumVelocity(const std::vector<std::unique_ptr<DetectedObject>>& detectedObjects) {
	currentMaxLinearVelocity = config.robot.maxVelX;
		
	for(const std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
		switch(detectedObject->getObjectType()) {
			case DetectedObject::ObjectType::Person:
			{
				DetectedPerson* person = reinterpret_cast<DetectedPerson*>(detectedObject.get());
				double distance = getDistance(robotPose.pose.position.x, robotPose.pose.position.y, person->pose.pose.position.x, person->pose.pose.position.y);
				double ratio;
				
				if(distance < person->lowerDistanceBound) {
					ratio = person->velocityReduction;
				} else if(distance > person->upperDistanceBound) {
					ratio = 1.0;
				} else {
					ratio = (distance - person->lowerDistanceBound) / (person->upperDistanceBound - person->lowerDistanceBound); // 0 -> completely inner, 1 -> completely outer
					ratio = person->velocityReduction + ratio * (1.0 - person->velocityReduction);
				}

				// Compute maximally reduced speed		
				currentMaxLinearVelocity = std::min(currentMaxLinearVelocity, config.robot.maxVelX * ratio);
				break;
			}
			case DetectedObject::ObjectType::AGV:
				break;
			case DetectedObject::ObjectType::Corridor:
				break;
		}
	}
}

}
