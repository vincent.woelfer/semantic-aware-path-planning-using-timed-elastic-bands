#include <costmap_2d/cost_values.h>
#include "semantic_elastic_bands/SebConfig.h"

namespace semantic_elastic_bands {


void SebConfig::loadFromRosNodeHandle(const ros::NodeHandle& handle) {
	// Get controller frequency
	ros::NodeHandle handleMoveBase("~");
	if(!handleMoveBase.param("controller_frequency", controllerFrequency, controllerFrequency)) {
		ROS_ERROR("Could not read controller_frequency from move_base, defaulting to %f. Make sure this parameter is set!", controllerFrequency);
	}

	handle.param("odomTopic", odomTopic, odomTopic);
	handle.param("mapFrame", mapFrame, mapFrame);
	handle.param("detectedObjectsTopic", detectedObjectsTopic, detectedObjectsTopic);
	handle.param("trackedObjectsTopic", trackedObjectsTopic, trackedObjectsTopic);
	
	std::string verbosityLevel_ = handle.param("verbosityLevel", verbosityLevelToString());
	parseVerbosityLevel(verbosityLevel_);
	
	// Robot
	handle.param("maxVelX", robot.maxVelX, robot.maxVelX);
	handle.param("maxVelXBackwards", robot.maxVelXBackwards, robot.maxVelXBackwards);
	handle.param("maxVelTheta", robot.maxVelTheta, robot.maxVelTheta);
	handle.param("maxAccX", robot.maxAccX, robot.maxAccX);
	handle.param("maxAccTheta", robot.maxAccTheta, robot.maxAccTheta);
	
	handle.param("minTurningRadius", robot.minTurningRadius, robot.minTurningRadius);
	handle.param("steeringAngleInsteadVelocity", robot.steeringAngleInsteadVelocity, robot.steeringAngleInsteadVelocity);
	handle.param("wheelbase", robot.wheelbase, robot.wheelbase);
	
	handle.param("limitAcceleration", robot.limitAcceleration, robot.limitAcceleration);
	
	// Planning
	handle.param("globalPlanPruneDistance", planning.globalPlanPruneDistance, planning.globalPlanPruneDistance);
	handle.param("maxGlobalPlanReuseDistance", planning.maxGlobalPlanReuseDistance, planning.maxGlobalPlanReuseDistance);
	handle.param("maxBandReuseGoalDistance", planning.maxBandReuseGoalDistance, planning.maxBandReuseGoalDistance);
	handle.param("maxBandReuseGoalAngleDeg", planning.maxBandReuseGoalAngleDeg, planning.maxBandReuseGoalAngleDeg);
	handle.param("globalPlanMaxLookaheadDistance", planning.globalPlanMaxLookaheadDistance, planning.globalPlanMaxLookaheadDistance);
	handle.param("globalPlanMaxLookaheadDuration", planning.globalPlanMaxLookaheadDuration, planning.globalPlanMaxLookaheadDuration);
	handle.param("localCostmapSafetyMarginInMeter", planning.localCostmapSafetyMarginInMeter, planning.localCostmapSafetyMarginInMeter);
	handle.param("localCostmapSafetyMarginInMeter", planning.localCostmapSafetyMarginInMeter, planning.localCostmapSafetyMarginInMeter);
	handle.param("goalToleranceXY", planning.goalToleranceXY, planning.goalToleranceXY);
	handle.param("goalToleranceThetaDeg", planning.goalToleranceThetaDeg, planning.goalToleranceThetaDeg);
	handle.param("velocityCommandLookaheadTime", planning.velocityCommandLookaheadTime, planning.velocityCommandLookaheadTime);
	handle.param("maxTraversalCost", planning.maxTraversalCost, planning.maxTraversalCost);
	handle.param("initializationMaxSpeedPercentage", planning.initializationMaxSpeedPercentage, planning.initializationMaxSpeedPercentage);
	
	// Optimization
	handle.param("weight_optimalTime", optimization.weight_optimalTime, optimization.weight_optimalTime);
	handle.param("weight_shortestPath", optimization.weight_shortestPath, optimization.weight_shortestPath);
	handle.param("weight_occupancyGrid", optimization.weight_occupancyGrid, optimization.weight_occupancyGrid);
	handle.param("weight_velocity", optimization.weight_velocity, optimization.weight_velocity);
	handle.param("weight_acceleration", optimization.weight_acceleration, optimization.weight_acceleration);
	handle.param("weight_kinematicNonHolonomic", optimization.weight_kinematicNonHolonomic, optimization.weight_kinematicNonHolonomic);
	handle.param("weight_kinematicDriveDirection", optimization.weight_kinematicDriveDirection, optimization.weight_kinematicDriveDirection);
	handle.param("weight_kinematicTurningRadius", optimization.weight_kinematicTurningRadius, optimization.weight_kinematicTurningRadius);
	handle.param("weight_reducedVelocity", optimization.weight_reducedVelocity, optimization.weight_reducedVelocity);
	handle.param("weight_rotationDirection", optimization.weight_rotationDirection, optimization.weight_rotationDirection);

	// Resizing and local path construction
	handle.param("referenceTimeDifference", optimization.referenceTimeDifference, optimization.referenceTimeDifference);
	handle.param("hysteresisTimeDiffFactor", optimization.hysteresisTimeDiffFactor, optimization.hysteresisTimeDiffFactor);
	handle.param("hysteresisIncreaseFactor", optimization.hysteresisIncreaseFactor, optimization.hysteresisIncreaseFactor);
	handle.param("minElasticBandSamples", optimization.minElasticBandSamples, optimization.minElasticBandSamples);
	handle.param("maxElasticBandSamples", optimization.maxElasticBandSamples, optimization.maxElasticBandSamples);
	handle.param("maxResizeIterations", optimization.maxResizeIterations, optimization.maxResizeIterations);
	handle.param("isSolutionUsableToleranceFactor", optimization.isSolutionUsableToleranceFactor, optimization.isSolutionUsableToleranceFactor);

	handle.param("fixStartOrientation", optimization.fixStartOrientation, optimization.fixStartOrientation);
	handle.param("fixGoalOrientation", optimization.fixGoalOrientation, optimization.fixGoalOrientation);
	handle.param("allowFreeGoalVelocity", optimization.allowFreeGoalVelocity, optimization.allowFreeGoalVelocity);	
	handle.param("allowBackwardsInitialization", optimization.allowBackwardsInitialization, optimization.allowBackwardsInitialization);

	// Ceres
	handle.param("numOptimizerIterations", optimization.numOptimizerIterations, optimization.numOptimizerIterations);
	handle.param("ceresMaxSolverTime", optimization.ceresMaxSolverTime, optimization.ceresMaxSolverTime);
	handle.param("ceresMaxNumIterations", optimization.ceresMaxNumIterations, optimization.ceresMaxNumIterations);
	handle.param("ceresNumThreads", optimization.ceresNumThreads, optimization.ceresNumThreads);
	
	handle.param("useExactArcLength", optimization.useExactArcLength, optimization.useExactArcLength);
	
	checkParameterValidity();
}

void SebConfig::reconfigure(SebLocalPlannerReconfigureConfig& config) {
	std::scoped_lock<std::mutex> lock(mutex);
	
	odomTopic = config.odomTopic;
	mapFrame = config.mapFrame;
	detectedObjectsTopic = config.detectedObjectsTopic;
	trackedObjectsTopic = config.trackedObjectsTopic;

	parseVerbosityLevel(config.verbosityLevel);
	
	// Robot
	robot.maxVelX = config.maxVelX;
	robot.maxVelXBackwards = config.maxVelXBackwards;
	robot.maxVelTheta = config.maxVelTheta;
	robot.maxAccX = config.maxAccX;
	robot.maxAccTheta = config.maxAccTheta;
	
	robot.minTurningRadius = config.minTurningRadius;
	robot.steeringAngleInsteadVelocity = config.steeringAngleInsteadVelocity;
	robot.wheelbase = config.wheelbase;
	
	robot.limitAcceleration = config.limitAcceleration;
	
	// Planning
	planning.globalPlanPruneDistance = config.globalPlanPruneDistance;
	planning.maxGlobalPlanReuseDistance = config.maxGlobalPlanReuseDistance;
	planning.maxBandReuseGoalDistance = config.maxBandReuseGoalDistance;
	planning.maxBandReuseGoalAngleDeg = config.maxBandReuseGoalAngleDeg;
	planning.globalPlanMaxLookaheadDistance = config.globalPlanMaxLookaheadDistance;
	planning.globalPlanMaxLookaheadDuration = config.globalPlanMaxLookaheadDuration;
	planning.localCostmapSafetyMarginInMeter = config.localCostmapSafetyMarginInMeter;
	planning.goalToleranceXY = config.goalToleranceXY;
	planning.goalToleranceThetaDeg = config.goalToleranceThetaDeg;
	planning.velocityCommandLookaheadTime = config.velocityCommandLookaheadTime;
	planning.maxTraversalCost = config.maxTraversalCost;
	planning.initializationMaxSpeedPercentage = config.initializationMaxSpeedPercentage;
	
	// Optimization
	optimization.weight_optimalTime = config.weight_optimalTime;
	optimization.weight_shortestPath = config.weight_shortestPath;
	optimization.weight_occupancyGrid = config.weight_occupancyGrid;
	optimization.weight_velocity = config.weight_velocity;
	optimization.weight_acceleration = config.weight_acceleration;
	optimization.weight_kinematicNonHolonomic = config.weight_kinematicNonHolonomic;
	optimization.weight_kinematicDriveDirection = config.weight_kinematicDriveDirection;
	optimization.weight_kinematicTurningRadius = config.weight_kinematicTurningRadius;
	optimization.weight_reducedVelocity = config.weight_reducedVelocity;
	optimization.weight_rotationDirection = config.weight_rotationDirection;

	// Resizing and local path construction
	optimization.referenceTimeDifference = config.referenceTimeDifference;
	optimization.hysteresisTimeDiffFactor = config.hysteresisTimeDiffFactor;
	optimization.hysteresisIncreaseFactor = config.hysteresisIncreaseFactor;
	optimization.minElasticBandSamples = config.minElasticBandSamples;
	optimization.maxElasticBandSamples = config.maxElasticBandSamples;
	optimization.maxResizeIterations = config.maxResizeIterations;
	optimization.isSolutionUsableToleranceFactor = config.isSolutionUsableToleranceFactor;
	
	optimization.fixStartOrientation = config.fixStartOrientation;
	optimization.fixGoalOrientation = config.fixGoalOrientation;
	optimization.allowFreeGoalVelocity = config.allowFreeGoalVelocity;
	optimization.allowBackwardsInitialization = config.allowBackwardsInitialization;
	
	optimization.numOptimizerIterations = config.numOptimizerIterations;
	optimization.ceresMaxSolverTime = config.ceresMaxSolverTime;
	optimization.ceresMaxNumIterations = config.ceresMaxNumIterations;
	optimization.ceresNumThreads = config.ceresNumThreads;
	
	optimization.useExactArcLength = config.useExactArcLength;
	
	
	checkParameterValidity();	
}

void SebConfig::checkParameterValidity() const {
	if(odomTopic.empty()) {
		ROS_WARN("SebLocalPlannerConfig: Param odomTopic is empty!");
	}
	if(mapFrame.empty()) {
		ROS_WARN("SebLocalPlannerConfig: Param mapFrame is empty!");
	}
	if(detectedObjectsTopic.empty()) {
		ROS_WARN("SebLocalPlannerConfig: Param detectedObjectsTopic is empty!");
	}
	if(trackedObjectsTopic.empty()) {
		ROS_WARN("SebLocalPlannerConfig: Param trackedObjectsTopic is empty!");
	}
		
	
	if(robot.steeringAngleInsteadVelocity && robot.minTurningRadius <= 0.0) {
		ROS_WARN("SebLocalPlannerConfig: Param minTurningRadius <= 0.0 indicates diff-drive robot but steeringAngleInsteadVelocity == true which is only used for carlike robots!");
	}
	if(robot.steeringAngleInsteadVelocity && robot.wheelbase <= 0.0) {
		ROS_WARN("SebLocalPlannerConfig: Param steeringAngleInsteadVelocity == true but wheelbase <= 0.0 so steering angle cannot be computed!");
	}
	
	if(robot.maxVelX <= 0.01) {
		ROS_WARN("SebLocalPlannerConfig: Param maxVelX < 0.01, consider increasing it!");
	}
	if(robot.maxVelXBackwards <= 0.01) {
		ROS_WARN("SebLocalPlannerConfig: Param maxVelXBackwards < 0.01, consider increasing it!");
	}
	if(planning.localCostmapSafetyMarginInMeter < 0.1) {
		ROS_WARN("SebLocalPlannerConfig: Param localCostmapSafetyMarginInMeter < 0.1, consider increasing it!");
	}
	
	if(optimization.referenceTimeDifference < 0.01) {
		ROS_WARN("SebLocalPlannerConfig: Param referenceTimeDifference < 0.01, consider increasing it!");
	}
	if(optimization.referenceTimeDifference > 3.0) {
		ROS_WARN("SebLocalPlannerConfig: Param referenceTimeDifference > 3.0, consider decreasing it!");
	}
	if(optimization.hysteresisTimeDiffFactor < 0.01 || optimization.hysteresisTimeDiffFactor > 1.0) {
		ROS_WARN("SebLocalPlannerConfig: Param hysteresisTimeDiffFactor must be in interval ]0.01 , 1.0[ !");
	}
	
	if(planning.globalPlanMaxLookaheadDistance != 0.0 && planning.globalPlanMaxLookaheadDistance < 0.1) {
		ROS_WARN("SebLocalPlannerConfig: Param globalPlanMaxLookaheadDistance should be >= 0.1 or 0.0 to disable it");
	}
	if(planning.globalPlanMaxLookaheadDuration != 0.0 && planning.globalPlanMaxLookaheadDuration < 0.5) {
		ROS_WARN("SebLocalPlannerConfig: Param globalPlanMaxLookaheadDuration should be >= 0.5 or 0.0 to disable it");
	}
	
	if(planning.maxTraversalCost == costmap_2d::LETHAL_OBSTACLE || planning.maxTraversalCost == costmap_2d::INSCRIBED_INFLATED_OBSTACLE) {
		ROS_ERROR("SebLocalPlannerConfig: Param maxTraversalCost is set to LETHAL_OBSTACLE or INSCRIBED_INFLATED_OBSTACLE, this enables the band to pass through obstacles");
	}

	if(planning.initializationMaxSpeedPercentage > 1.0 || planning.initializationMaxSpeedPercentage < 0.1) {
		ROS_ERROR("SebLocalPlannerConfig: Param initializationMaxSpeedPercentage is set to should be in interval [0.1, 1.0[ !");
	}
}

void SebConfig::checkParameterValidityRegardingCostmap(const costmap_2d::Costmap2D* costmap) const {
	const double costmapSize = std::min(costmap->getSizeInMetersX(), costmap->getSizeInMetersY());
	
	if(costmapSize <= planning.localCostmapSafetyMarginInMeter * 3.0) {
		ROS_WARN("The localCostmapSafetyMargin reduces the local costmap to below 1/9 of its size, consider increasing the costmap or reducing the margin");
	}
	if(costmapSize <= planning.localCostmapSafetyMarginInMeter * 2.0) {
		ROS_ERROR("The localCostmapSafetyMargin completely covers the local costmap which would render every plan unusable!");
	}
	if(costmapSize / 2.0 - planning.localCostmapSafetyMarginInMeter < planning.globalPlanMaxLookaheadDistance) {
		ROS_WARN("The localCostmapSafetyMargin reduces the usable local costmap size below the globalPlanMaxLookaheadDistance parameter!");
	}
}

std::mutex& SebConfig::getConfigMutex() {
	return mutex;
}

bool SebConfig::parseVerbosityLevel(const std::string& level) {
	if(level == "none") {
		verbosityLevel = VerbosityLevel::none;
		return true;
	}
	if(level == "normal") {
		verbosityLevel = VerbosityLevel::normal;
		return true;
	}
	if(level == "full") {
		verbosityLevel = VerbosityLevel::full;
		return true;
	}

	ROS_WARN("VerbosityLevel '%s', is not a valid verbosity level. Valid options are: 'none', 'normal', 'full'", level.c_str());	
	return false;
}

std::string SebConfig::verbosityLevelToString() const {
	switch(verbosityLevel) {
		case VerbosityLevel::none:
			return "none";
		case VerbosityLevel::normal:
			return "normal";
		case VerbosityLevel::full:
			return "full";
	}
}

bool SebConfig::isVerbosityAtLeastNormal() const {
	return (verbosityLevel == VerbosityLevel::normal || verbosityLevel == VerbosityLevel::full); 
}

bool SebConfig::isVerbosityAtLeastFull() const {
	return verbosityLevel == VerbosityLevel::full;
}

}