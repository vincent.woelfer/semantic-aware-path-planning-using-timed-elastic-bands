#include <cmath>
#include "semantic_elastic_bands/LineOfSightCalculations/LineOfSightCalculatorExact.h"

namespace semantic_elastic_bands {

template<class T>
LineOfSightCalculatorExact<T>::LineOfSightCalculatorExact(const T* mapData, unsigned int mapWidth, float startX, float startY, float endX, float endY) :
		mapData(mapData),
		mapWidth(mapWidth),
		startX(startX),
		startY(startY),
		endX(endX),
		endY(endY)
{}

template<class T>
float LineOfSightCalculatorExact<T>::getLineOfSightCost(float costOffset, float maxTraversalCost) {
	// see https://playtechs.blogspot.com/2007/03/raytracing-on-grid.html
	float cost = 0.0;

	float dx = std::abs(endX - startX);
	float dy = std::abs(endY - startY);
	int x = std::floor(startX);
	int y = std::floor(startY);
	int n = 1;
	
	int x_inc;
	int y_inc;
	float error;
	
	// Determine X step
	if(dx == 0) {
		x_inc = 0;
		error = std::numeric_limits<float>::infinity();
	} else if(endX > startX) {
		x_inc = 1;
		n += static_cast<int>(std::floor(endX)) - x;
		error = (std::floor(startX) + 1 - startX) * dy;
	} else {
		x_inc = -1;
		n += x - static_cast<int>(std::floor(endX));
		error = (startX - std::floor(startX)) * dy;
	}

	// Determine Y step
	if(dy == 0) {
		y_inc = 0;
		error -= std::numeric_limits<float>::infinity();
	} else if(endY > startY) {
		y_inc = 1;
		n += static_cast<int>(std::floor(endY)) - y;
		error -= (std::floor(startY) + 1 - startY) * dx;
	} else {
		y_inc = -1;
		n += y - static_cast<int>(std::floor(endY));
		error -= (startY - std::floor(startY)) * dx;
	}

	CohenSutherlandClipping cohenSutherlandClipping(startX, startY, endX, endY);
	
	// Loop over cells covering line-of-sight
	for(; n > 0; --n) {
		float cell = getCellCost(x, y);

		// Abort if cell is impassable
		if(cell > maxTraversalCost) {
			return -1.0;
		}

		float length = cohenSutherlandClipping.getLengthThroughCell(x, y);
		if(length > 0.0) {
			cost += (cell + costOffset) * length;
		}

		if(error > 0) {
			y += y_inc;
			error -= dx;
		} else {
			x += x_inc;
			error += dy;
		}
	}

	return cost;
}

template<class T>
float LineOfSightCalculatorExact<T>::getCellCost(int x, int y) const {
	return static_cast<float>(mapData[y * mapWidth + x]);
}

// Explicitly instantiate the required template classes
template class LineOfSightCalculatorExact<int8_t>;
template class LineOfSightCalculatorExact<unsigned char>;

}