#include <cmath>
#include "semantic_elastic_bands/LineOfSightCalculations/LineOfSightCalculatorCellCenter.h"

namespace semantic_elastic_bands {

template<class T>
LineOfSightCalculatorCellCenter<T>::LineOfSightCalculatorCellCenter(const T* mapData, unsigned int mapWidth, int startX, int startY, int endX, int endY) :
		mapData(mapData),
		mapWidth(mapWidth),
		startX(startX),
		startY(startY),
		endX(endX),
		endY(endY)
{}

template<class T>
float LineOfSightCalculatorCellCenter<T>::getLineOfSightCost(float costOffset, float maxTraversalCost) {
	// This algorithm is the specialized integer-only variant taken from
	// https://playtechs.blogspot.com/2007/03/raytracing-on-grid.html
	// Also see: https://playtechs.blogspot.com/2007/03/raytracing-on-grid.html?showComment=1281448902099#c3785285092830049685
	
	float cost = 0.0;

	int dx = std::abs(endX - startX);
	int dy = std::abs(endY - startY);
	int x = startX;
	int y = startY;
	int n = 1 + dx + dy;
	int x_inc = (endX > startX) ? 1 : -1;
	int y_inc = (endY > startY) ? 1 : -1;
	int error = dx - dy;
	dx *= 2;
	dy *= 2;
	
	CohenSutherlandClipping cohenSutherlandClipping(static_cast<float>(startX) + 0.5f, static_cast<float>(startY) + 0.5f,
												    static_cast<float>(endX) + 0.5f, static_cast<float>(endY) + 0.5f);

	// Loop over cells covering line-of-sight
	for(; n > 0; --n) {
		float cell = getCellCost(x, y);

		// Abort if cell is impassable
		if(cell > maxTraversalCost) {
			return -1.0;
		}

		float length = cohenSutherlandClipping.getLengthThroughCell(x, y);
		if(length > 0.0) {
			cost += (cell + costOffset) * length;
		}

		if(error > 0) {
			x += x_inc;
			error -= dy;
		} else {
			y += y_inc;
			error += dx;
		}
	}

	return cost;
}

template<class T>
float LineOfSightCalculatorCellCenter<T>::getCellCost(int x, int y) const {
	return static_cast<float>(mapData[y * mapWidth + x]);
}

// Explicitly instantiate the required template classes
template class LineOfSightCalculatorCellCenter<int8_t>;
template class LineOfSightCalculatorCellCenter<unsigned char>;

}