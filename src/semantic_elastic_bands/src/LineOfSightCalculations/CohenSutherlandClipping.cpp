#include "semantic_elastic_bands/LineOfSightCalculations/CohenSutherlandClipping.h"

#include <ros/ros.h>
#include <cmath>

namespace semantic_elastic_bands {

CohenSutherlandClipping::CohenSutherlandClipping(float startX, float startY, float endX, float endY) :
		startXConst(startX),
		startYConst(startY),
		endXConst(endX),
		endYConst(endY)
{}

float CohenSutherlandClipping::getLengthThroughCell(int x, int y) {
	// See https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm

	resetCurrentCell((float) x, (float) x + 1.0f, (float) y, (float) y + 1.0f);

	unsigned int outCodeStart = computeOutCode(startX, startY);
	unsigned int outCodeEnd = computeOutCode(endX, endY);

	while(true) {
		if(!(outCodeStart | outCodeEnd)) {
			// Both are inside, finished
			float dx = endX - startX;
			float dy = endY - startY;
			return std::sqrt(dx * dx + dy * dy);

		} else if(ROS_UNLIKELY(outCodeStart & outCodeEnd)) {
			// Both are outside on the same site -> error
			ROS_ERROR("Error in Cohen-Sutherland Clipping");
			return -1.0;

		} else {
			float newX, newY;

			// At least one endpoint is outside, pick it
			unsigned int outCode = outCodeEnd > outCodeStart ? outCodeEnd : outCodeStart;

			if(outCode & TOP) {
				newX = startX + (endX - startX) * (maxY - startY) / (endY - startY);
				newY = maxY;
			} else if(outCode & BOTTOM) {
				newX = startX + (endX - startX) * (minY - startY) / (endY - startY);
				newY = minY;
			} else if(outCode & RIGHT) {
				newY = startY + (endY - startY) * (maxX - startX) / (endX - startX);
				newX = maxX;
			} else if(outCode & LEFT) {
				newY = startY + (endY - startY) * (minX - startX) / (endX - startX);
				newX = minX;
			}

			if(outCode == outCodeStart) {
				startX = newX;
				startY = newY;
				outCodeStart = computeOutCode(startX, startY);
			} else {
				endX = newX;
				endY = newY;
				outCodeEnd = computeOutCode(endX, endY);
			}
		}
	}
}

inline void CohenSutherlandClipping::resetCurrentCell(float minX_, float maxX_, float minY_, float maxY_) {
	minX = minX_;
	maxX = maxX_;
	minY = minY_;
	maxY = maxY_;

	startX = startXConst;
	startY = startYConst;
	endX = endXConst;
	endY = endYConst;
}

inline unsigned int CohenSutherlandClipping::computeOutCode(float x, float y) const {
	unsigned int code = INSIDE;

	constexpr float epsilon = 0.001;

	if(x < minX - epsilon) {
		code |= (unsigned int) LEFT;
	} else if(x > maxX + epsilon) {
		code |= (unsigned int) RIGHT;
	}

	if(y < minY - epsilon) {
		code |= (unsigned int) BOTTOM;
	} else if(y > maxY + epsilon) {
		code |= (unsigned int) TOP;
	}

	return code;
}

}
