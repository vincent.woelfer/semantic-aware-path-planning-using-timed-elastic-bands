#include "semantic_elastic_bands/Planner.h"

#include "semantic_elastic_bands/Utility.h"

#include <ros/console.h>
#include <ros/ros.h>
#include <tf/transform_datatypes.h>

#include <utility>
#include <costmap_2d/cost_values.h>
#include "semantic_elastic_bands/DetectedObjects/DetectedPerson.h"

using visualization_msgs::Marker;

namespace semantic_elastic_bands {

Planner::Planner(ros::NodeHandle& nodeHandle, const SebConfig& SebConfig, costmap_2d::Costmap2D* costmap, const std::string& localCostmapFrame) :
		config(SebConfig),
		costmap(costmap),
		localCostmapFrame(localCostmapFrame)
{
	visualisation.initialize(nodeHandle, localCostmapFrame);
}

Planner::~Planner() = default;

void Planner::visualizePreOptimization() {
	if(seb) {
		seb->updateTransformedPoses(costmap->getOriginX(), costmap->getOriginY(), costmap->getResolution());
		visualisation.publishSemanticElasticBandPreOptimization(*seb);
	}
}

void Planner::visualizePostOptimization(const std::vector<geometry_msgs::PoseStamped>& initialPath) {
	visualisation.publishInitialPath(initialPath);
	
	if(seb) {
		visualisation.publishSemanticElasticBandPlan(*seb);
		visualisation.publishSemanticElasticBand(*seb);
		visualisation.publishTrajectoryData(*seb);
	} else {
		visualisation.publishEmptySeb();
	}
}

Planner::PlanningResult Planner::plan(const std::vector<geometry_msgs::PoseStamped>& initialPath, const geometry_msgs::Twist startVelocity, bool isFinalGoal, bool forceReinitialization) {
	ros::WallTime t0 = ros::WallTime::now();

	// Update local copy of initial path and transform it to cell-space
	if(!transformInitialPathToCellspace(initialPath)) {
		ROS_WARN("Planning failed because initial path could not be transformed to cell space!");
		prevPlanningTime = 0;
		return PlanningResult::FAILURE;
	}
	
	// Check if we can reuse the previously computed path
	std::string planningType;
	bool reuseBand = !forceReinitialization && canBandBeReused();
	
	if(reuseBand) {
		planningType = "Re-Planing";
				
		seb->updateStartGoal(initialPathTransformed.front(), initialPathTransformed.back());
		seb->autoResize(true);
	} else {
		planningType = "PLANING   "; // Add padding for better output
		
		// Construct SEB in cell space
		seb = std::make_unique<SemanticElasticBand>(initialPathTransformed, isFinalGoal, config, costmap->getResolution());

		// Verify that all band poses are in passable space:
		// Because the initial global path might have become invalid we can not assume that the newly constructed SEB poses lie only in passable terrain.
		// We therefore verify this assumption and abort if it is not met
		bool bandValid = true;		
		for(const Pose& pose : seb->getPoses()) {
			const Point p = pose.toPoint();
			unsigned char cost = costmap->getCost(p.x, p.y);
			if(cost > config.planning.maxTraversalCost && cost != costmap_2d::NO_INFORMATION) {				
				bandValid = false;
				break;
			}
		}

		if(!bandValid) {
			ros::WallTime t1 = ros::WallTime::now();
			prevPlanningTime = (t1 - t0).toNSec() * 1e-6;
			ROS_WARN("Newly constructed SEB crosses impassable parts of the costmap, aborting iteration! This most likely happened because the global plane became invalid due to new/moved obstacles.");
			return PlanningResult::FAILURE;
		}		
	}	
		
	// Construct velocity constraints for start/goal
	seb->setVelocityConstraintStart(FixedVelocityConstraint(true, startVelocity.linear.x, startVelocity.angular.z));
	bool fixedGoalVelocity = isFinalGoal && (!config.optimization.allowFreeGoalVelocity);
	seb->setVelocityConstraintGoal(FixedVelocityConstraint(fixedGoalVelocity, 0.0, 0.0));
	
	// Optimize
	visualizePreOptimization();
	Optimizer optimizer(seb.get(), config, costmap, detectedObjects);
	bool successful = optimizer.optimize();
	
	int numDirectionChanges = seb->getNumberOfDirectionChanges(); 
	bool onlyUseOnce = numDirectionChanges > 1;

	ros::WallTime t1 = ros::WallTime::now();
	prevPlanningTime = (t1 - t0).toNSec() * 1e-6;

	if(successful) {
		if(config.isVerbosityAtLeastNormal()) {
			ROS_INFO("%s in %3.0f ms | Length: %3.2f m | Path-Duration: %3.2f s", planningType.c_str(), (t1 - t0).toNSec() * 1e-6, seb->getBandLength(), seb->getBandDuration());	
		}
		
		if(config.isVerbosityAtLeastNormal() && onlyUseOnce) {
			ROS_INFO("Detected %d direction changes but only %d are allowed, discarding band next iteration!", numDirectionChanges, 1);
		}
	} else {
		ROS_WARN("%s not successful, took %3.0f ms", planningType.c_str(), (t1 - t0).toNSec() * 1e-6);
		seb.reset(); // delete seb, this also removes any previous or invalid data from rviz
	}

	visualizePostOptimization(initialPath);
	
	optimizer.printSummary();

	if(!successful) {
		return PlanningResult::FAILURE;
	} else {
		if(onlyUseOnce) {
			return PlanningResult::USABLE_ONCE;
		} else {
			return PlanningResult::SUCCESS;
		}
	}
}

bool Planner::canBandBeReused() {
	if(!seb || seb->getPoses().size() < 2 || seb->getMapResolution() != costmap->getResolution()) {
		return false;
	}
	
	// Verify that start/goal of path and band dont differ too much
	Pose planStartPose = initialPathTransformed.front();
	Pose planGoalPose = initialPathTransformed.back();
	const Pose& sebStartPose = seb->getPoses().front();
	const Pose& sebGoalPose = seb->getPoses().back();
	
	// Poses are in cell space -> multiply by resolution to get distance in m
	double startDistance = getDistance(sebStartPose, planStartPose) * costmap->getResolution();
	double goalDistance = getDistance(sebGoalPose, planGoalPose) * costmap->getResolution();
	double startAngleDifference = rad2deg(std::abs(getAngleDifference(sebStartPose.theta, planStartPose.theta)));
	double goalAngleDifference = rad2deg(std::abs(getAngleDifference(sebGoalPose.theta, planGoalPose.theta)));
	
	if(startDistance > config.planning.maxBandReuseGoalDistance || goalDistance > config.planning.maxBandReuseGoalDistance ||
	   startAngleDifference > config.planning.maxBandReuseGoalAngleDeg || goalAngleDifference > config.planning.maxBandReuseGoalAngleDeg) {
		return false;
	}

	// Verify that all band poses are in passable space
	for(const Pose& pose : seb->getPoses()) {
		const Point p = pose.toPoint();
		unsigned char cost = costmap->getCost(p.x, p.y); 
		if(cost > config.planning.maxTraversalCost && cost != costmap_2d::NO_INFORMATION) {
			ROS_ERROR("Existing band can't be reused because it crosses non-traversable parts of the map!");
			return false;
		}
	}
	
	return true;
}

bool Planner::transformInitialPathToCellspace(const std::vector<geometry_msgs::PoseStamped>& initialPath) {
	// Sanity Checks
	if(initialPath.size() < 2 || initialPath.front() == initialPath.back()) {
		ROS_WARN("Cannot transform initial path because it is not valid, aborting!");
		return false;
	}
	if(initialPath.front().header.frame_id != localCostmapFrame) {
		ROS_WARN("SEB: Initial path is in '%s' fame, expected '%s' frame", initialPath.front().header.frame_id.c_str(), localCostmapFrame.c_str());
	}

	// Clear previous path
	initialPathTransformed.clear();
	int numInvalidPoses = 0;
	const double marginInCells = config.planning.localCostmapSafetyMarginInMeter / costmap->getResolution();
	
	for(const geometry_msgs::PoseStamped& pose : initialPath) {
		Pose transformedPose;
		
		if(!convertPoseToCellSpace(costmap, pose.pose.position.x, pose.pose.position.y, toAngle(pose.pose.orientation), transformedPose, marginInCells)) {
			numInvalidPoses++;
		} else {
			initialPathTransformed.push_back(transformedPose);	
		}				
	}	
	
	// Print if any invalid poses
	if(numInvalidPoses > 0) {
		ROS_ERROR("SEB: %d/%zu poses from initial plan are outside of costmap in cell space!", numInvalidPoses, initialPath.size());
		return false;
	}
	
	return true;
}

void Planner::computeCmdVel(double& vX, double& vTheta) {
	// Find end pose for calculation.
	int index = 0;
	double dt = 0.0;
	
	while(dt < config.planning.velocityCommandLookaheadTime && index < seb->getTimeDiffs().size()) {
		dt += seb->getTimeDiffs().at(index++);
	}	
	
	const Pose& a = seb->getTransformedPoses()[0];
	const Pose& b = seb->getTransformedPoses()[index];
	
	double dx = b.position.x() - a.position.x();
	double dy = b.position.y() - a.position.y();
	
	// Translational velocity
	double dir = dot(dx, dy, std::cos(a.theta), std::sin(a.theta));	
	double length = std::sqrt(dx*dx + dy*dy);
	vX = sign(dir) * length / dt;

	// Rotational velocity
	double angleDiff = getAngleDifference(a.theta, b.theta);
	vTheta = angleDiff / dt;	
}

void Planner::updateDetectedObjects(std::vector<std::unique_ptr<DetectedObject>> newDetectedObjects) {
	detectedObjects.clear(); // Calls destructor via smart pointer
	
	// Move semantics to avoid copying unique pointer
	detectedObjects = std::move(newDetectedObjects);
	
	// Transform to cell space
	for(std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
		detectedObject->convertToCellSpace(costmap);
	}
}


}
