#include <geometry_msgs/PointStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "semantic_elastic_bands/DetectedObjects/DetectedObjectParser.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedPerson.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedAGV.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedCorridor.h"

using geometry_msgs::TransformStamped;
using geometry_msgs::PoseStamped;
using geometry_msgs::Pose;

namespace semantic_elastic_bands {

DetectedObjectParser::DetectedObjectParser(const std::string& detectedTopicName, const std::string& trackedTopicName) {
	
	ros::NodeHandle nh("~");
	subscriberDetectedObjects = nh.subscribe(detectedTopicName, 1, &DetectedObjectParser::detectedObjectCallback, this);
	subscriberTrackedObjects = nh.subscribe(trackedTopicName, 1, &DetectedObjectParser::trackedObjectCallback, this);
	
	ROS_INFO("DetectedObjectParser subscribing to topic: %s", subscriberDetectedObjects.getTopic().c_str());
	ROS_INFO("TrackedObjectParser subscribing to topic: %s", subscriberTrackedObjects.getTopic().c_str());
}

DetectedObjectParser::~DetectedObjectParser() {
	for(DetectedObject* detectedObject : detectedObjects) {
		delete detectedObject;
	}
}

void DetectedObjectParser::matchDetectionsWithTrackings() {
	// Mutex is already locked by calling function	
	detectedObjects.clear();
	
	std::set<unsigned long> detectionsCoveredByTrackingMessages;
	
	// Add all trackings
	for(const semantic_elastic_bands::TrackedObjectMsg& trackedObject : latestTrackedObjectsMsg.tracks) {
		// Get stamped pose
		PoseStamped poseStamped;
		poseStamped.pose = trackedObject.pose.pose;
		poseStamped.header = latestTrackedObjectsMsg.header;

		unsigned long trackingId = trackedObject.track_id;
		unsigned long detectionId = trackedObject.is_matched ? trackedObject.detection_id : 0;
		double linearVelocityX = trackedObject.twist.twist.linear.x;
		
		bool trackingWasAdded = true;

		// Person
		if(trackedObject.class_id == CLASS_HUMAN) {
			detectedObjects.push_back(new DetectedPerson(detectionId, trackingId, poseStamped, linearVelocityX));
		}
		// AGV
		else if(trackedObject.class_id == CLASS_AGV) {
			detectedObjects.push_back(new DetectedAGV(detectionId, trackingId, poseStamped, linearVelocityX));
		}
		// Unknown Object Type
		else {
			trackingWasAdded = false;
			ROS_WARN("Got TrackedObject message with unknown class_id: %d", trackedObject.class_id);
		}
		
		if(trackingWasAdded && detectionId != 0) {
			detectionsCoveredByTrackingMessages.insert(detectionId);
		}
	}
	
	// Go over all detections not covered by trackings 
	for(const semantic_elastic_bands::DetectedObjectMsg& detectedObject : latestDetectedObjectsMsg.detections) {
		
		// Skip detections already added as matched by a TrackedObject message
		if(detectionsCoveredByTrackingMessages.count(detectedObject.detection_id)) {
			continue;
		}
		
		// Get stamped pose
		PoseStamped poseStamped;
		poseStamped.pose = detectedObject.pose.pose;
		poseStamped.header = latestDetectedObjectsMsg.header;

		// Person
		if(detectedObject.class_id == CLASS_HUMAN) {
			detectedObjects.push_back(new DetectedPerson(detectedObject.detection_id, 0, poseStamped, 0.0));
		}
		// AGV
		else if(detectedObject.class_id == CLASS_AGV) {
			detectedObjects.push_back(new DetectedAGV(detectedObject.detection_id, 0, poseStamped, 0.0));
		}
		// Unknown Object Type
		else {
			ROS_WARN("Got DetectedObject message with unknown class_id: %d", detectedObject.class_id);
		}				
	}	
}

void DetectedObjectParser::detectedObjectCallback(const semantic_elastic_bands::DetectedObjectsMsg& msg) {	
	std::scoped_lock<std::mutex> lock(mutex);

	latestDetectedObjectsMsg = msg;

	matchDetectionsWithTrackings();
}

void DetectedObjectParser::trackedObjectCallback(const semantic_elastic_bands::TrackedObjectsMsg& msg) {
	std::scoped_lock<std::mutex> lock(mutex);

	latestTrackedObjectsMsg = msg;

	matchDetectionsWithTrackings();
}

void DetectedObjectParser::createMockCorridors() {
	// Append to detected Objects
	
	// General stuff
	PoseStamped start, end;
	double width = 2.0;
	DetectedCorridor* corridor;
	
	start.header.stamp = ros::Time::now();
	end.header.stamp = ros::Time::now();
	start.header.frame_id = "map";
	end.header.frame_id = "map";
	start.pose.position.z = 0;
	end.pose.position.z = 0;
	
	// Left
	start.pose.position.x = -8;	
	start.pose.position.y = 0;	
	end.pose.position.x = -1;
	end.pose.position.y = 0;
	corridor = new DetectedCorridor(start, end, width);
	detectedObjects.push_back(corridor);

	// Right
	start.pose.position.x = 1;
	start.pose.position.y = 0;
	end.pose.position.x = 15;
	end.pose.position.y = 0;
	corridor = new DetectedCorridor(start, end, width);
	detectedObjects.push_back(corridor);

	// Top
	start.pose.position.x = 0;
	start.pose.position.y = 1;
	end.pose.position.x = 0;
	end.pose.position.y = 7;
	corridor = new DetectedCorridor(start, end, width);
	detectedObjects.push_back(corridor);
	
	// Bot
	start.pose.position.x = 0;
	start.pose.position.y = -1;
	end.pose.position.x = 0;
	end.pose.position.y = -6;
	corridor = new DetectedCorridor(start, end, width);
	detectedObjects.push_back(corridor);
}

std::vector<std::unique_ptr<DetectedObject>> DetectedObjectParser::getTransformedObjects(tf2_ros::Buffer* tf, const std::string& targetFrame, const ros::Duration& timeout) {
	std::scoped_lock<std::mutex> lock(mutex);
	
	createMockCorridors();
	
	std::vector<std::unique_ptr<DetectedObject>> transformedObjects;
	transformedObjects.reserve(detectedObjects.size());

	// Store previous transform to reuse it if possible
	std::string previousFrame;
	ros::Time previousTime;
	TransformStamped previousTransform;
	
	ros::Time timeNow = ros::Time::now();
	
	// Transform each object
	for(DetectedObject* detectedObject : detectedObjects) {
		try {
			switch(detectedObject->getObjectType()) {
				case DetectedObject::ObjectType::Person:
				{
					DetectedPerson* detectedPerson = reinterpret_cast<DetectedPerson*>(detectedObject);

					TransformStamped transform;
					const std::string& frame = detectedPerson->pose.header.frame_id;
					const ros::Time& time = detectedPerson->pose.header.stamp;
					
					// Can we reuse the previous transform ?
					if(previousFrame == frame && previousTime == time) {
						transform = previousTransform;
					} else {
						transform = tf->lookupTransform(targetFrame, timeNow, frame, time, frame, timeout);
						previousTransform = transform;
						previousFrame = frame;
						previousTime = time;
					}
					
					PoseStamped transformedPose;
					tf2::doTransform(detectedPerson->pose, transformedPose, transform);

					transformedObjects.emplace_back(std::make_unique<DetectedPerson>(detectedPerson->getDetectionId(), detectedObject->getTrackingId(),
																	  transformedPose, detectedPerson->extensionLength));

					break;
				}				
				case DetectedObject::ObjectType::AGV:
				{
					DetectedAGV* detectedAGV = reinterpret_cast<DetectedAGV*>(detectedObject);

					TransformStamped transform;
					const std::string& frame = detectedAGV->pose.header.frame_id;
					const ros::Time& time = detectedAGV->pose.header.stamp;

					// Can we reuse the previous transform ?
					if(previousFrame == frame && previousTime == time) {
						transform = previousTransform;
					} else {
						transform = tf->lookupTransform(targetFrame, timeNow, frame, time, frame, timeout);
						previousTransform = transform;
						previousFrame = frame;
						previousTime = time;
					}

					PoseStamped transformedPose;
					tf2::doTransform(detectedAGV->pose, transformedPose, transform);

					transformedObjects.emplace_back(std::make_unique<DetectedAGV>(detectedAGV->getDetectionId(), detectedObject->getTrackingId(),
																   transformedPose, detectedAGV->extensionLength));

					break;
				}
				case DetectedObject::ObjectType::Corridor:
				{
					DetectedCorridor* detectedCorridor = reinterpret_cast<DetectedCorridor*>(detectedObject);

					TransformStamped transform;
					const std::string& frame = detectedCorridor->poseStart.header.frame_id;
					const ros::Time& time = detectedCorridor->poseStart.header.stamp;

					// Can we reuse the previous transform ?
					if(previousFrame == frame && previousTime == time) {
						transform = previousTransform;
					} else {
						transform = tf->lookupTransform(targetFrame, timeNow, frame, time, frame, timeout);
						previousTransform = transform;
						previousFrame = frame;
						previousTime = time;
					}

					PoseStamped transformedPoseStart;
					PoseStamped transformedPoseEnd;
					tf2::doTransform(detectedCorridor->poseStart, transformedPoseStart, transform);
					tf2::doTransform(detectedCorridor->poseEnd, transformedPoseEnd, transform);

					transformedObjects.emplace_back(std::make_unique<DetectedCorridor>(transformedPoseStart, transformedPoseEnd, detectedCorridor->width));

					break;
				}				
				default:
					ROS_ERROR("Transform for DetectedObject of type %d is not implemented!", (int)detectedObject->getObjectType());
					break;
			}
		}
		catch(const tf2::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			continue;
		}
		catch(const tf2::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			continue;
		}
		catch(const tf2::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			continue;
		}		
	}
	
	return transformedObjects;
}
}