#include "semantic_elastic_bands/DetectedObjects/DetectedCorridor.h"

#include "semantic_elastic_bands/Utility.h"
#include <ros/ros.h>
#include <tf/transform_datatypes.h>

namespace semantic_elastic_bands {

semantic_elastic_bands::DetectedCorridor::DetectedCorridor(geometry_msgs::PoseStamped poseStart, geometry_msgs::PoseStamped poseEnd, double width) :
		DetectedObject(ObjectType::Corridor, 0, 0),
		poseStart(std::move(poseStart)),
		poseEnd(std::move(poseEnd)),
		width(width)
{
	computeBounds();
}

void DetectedCorridor::convertToCellSpace(costmap_2d::Costmap2D* costmap) {
	convertPointToCellSpaceNoBounds(costmap, poseStart.pose.position.x, poseStart.pose.position.y, pointStartCellSpace);
	convertPointToCellSpaceNoBounds(costmap, poseEnd.pose.position.x, poseEnd.pose.position.y, pointEndCellSpace);
	
	Point pointMin;
	Point pointMax;
	convertPointToCellSpaceNoBounds(costmap, minXMeterSpace, minYMeterSpace, pointMin);
	convertPointToCellSpaceNoBounds(costmap, maxXMeterSpace, maxYMeterSpace, pointMax);
	minXCellSpace = pointMin.x;
	minYCellSpace = pointMin.y;
	maxXCellSpace = pointMax.x;
	maxYCellSpace = pointMax.y;
	
	// Compute data for faster cost calculation
	corridorVectorX = pointEndCellSpace.x - pointStartCellSpace.x;
	corridorVectorY = pointEndCellSpace.y - pointStartCellSpace.y;
	corridorVectorLengthSquared = corridorVectorX*corridorVectorX + corridorVectorY*corridorVectorY;
	halfWidthCellSpace = (width/2.0) / costmap->getResolution();
	optimalTrackOffsetCellSpace = halfWidthCellSpace * 0.5; // 0.5 -> center of left/right corridor half. Larger than 0.5 -> further away from center
	
	// This factor can be used to determine the distance to the corridor start/end based on a t value.
	tToDistanceFactor = std::sqrt(corridorVectorLengthSquared);	
}

void DetectedCorridor::updateCostmapBounds(double* min_x, double* min_y, double* max_x, double* max_y) const {
	// Enlarge area by own updated area -> OR areas
	*min_x = std::min(*min_x, minXMeterSpace);
	*min_y = std::min(*min_y, minYMeterSpace);
	*max_x = std::max(*max_x, maxXMeterSpace);
	*max_y = std::max(*max_y, maxYMeterSpace);
}

void DetectedCorridor::getCostmapUpdateArea(double costmapResolution, int& minX, int& minY, int& maxX, int& maxY) const {
	// Shrink area to get overlap of given and own updated area -> AND areas
	minX = std::max(minX, minXCellSpace);
	minY = std::max(minY, minYCellSpace);
	maxX = std::min(maxX, maxXCellSpace);
	maxY = std::min(maxY, maxYCellSpace);	
}

bool DetectedCorridor::skipBasedOnRobotPose(const Pose& robotPose) {
	double vectorStartPointX = robotPose.position.x() - pointStartCellSpace.x;
	double vectorStartPointY = robotPose.position.y() - pointStartCellSpace.y;

	// Project point onto line segment
	double t = dot(vectorStartPointX, vectorStartPointY, corridorVectorX, corridorVectorY) / corridorVectorLengthSquared;
	
	t = std::clamp(t, -0.1, 1.1);
	
	// Project onto corridor line segment
	double projectionX = pointStartCellSpace.x + t * corridorVectorX;
	double projectionY = pointStartCellSpace.y + t * corridorVectorY;
	double distance = getDistance(robotPose.position.x(), robotPose.position.y(), projectionX, projectionY);

	// Check if robot is "near" corridor
	if(distance > halfWidthCellSpace * 3.0) {
		// Skip
		return true;
	}
	
	// Check which direction is "right". dot product. 1 => collinear, < 0 => deg over 90° -> flip side
	robotColinearToCorridor = (dot(corridorVectorX, corridorVectorY, std::cos(robotPose.theta), std::sin(robotPose.theta)) >= 0.0);	
}

unsigned char DetectedCorridor::getCellCost(double costmapResolution, const Point& p) const {
	double vectorStartPointX = p.x - pointStartCellSpace.x;
	double vectorStartPointY = p.y - pointStartCellSpace.y;

	// Project point onto line segment
	double t = dot(vectorStartPointX, vectorStartPointY, corridorVectorX, corridorVectorY) / corridorVectorLengthSquared;

	// If point is not on one side of the corridor line segment
	if(t < 0.0 || t > 1.0) {
		return 0;
	}

	// Project onto corridor line segment
	double projectionX = pointStartCellSpace.x + t * corridorVectorX;
	double projectionY = pointStartCellSpace.y + t * corridorVectorY;
	double distance = getDistance((double) p.x, (double) p.y, projectionX, projectionY);

	// Check if point in corridor width-wise
	if(distance > halfWidthCellSpace) {
		return 0;
	}

	// Find our which side of the line the point lies on
	// side negative -> right, positive -> left
	double side = corridorVectorX * vectorStartPointY - corridorVectorY * vectorStartPointX;

	// Adjust distance so the optimal "track" is in the center "correct" corridor side
	// Per default positive = right side. This can be altered via "driveOnRightSide = false" or 
	// If the robot is detected to be coming from the other side so "right" needs to be flipped (robotColinearToCorridor = false)
	bool correctSide = side >= 0.0;

	if(!driveOnRightSide) {
		correctSide = !correctSide;
	}
	if(!robotColinearToCorridor) {
		correctSide = !correctSide;
	}

	if(correctSide) {
		// left side -> increase distance in all cases
		distance += optimalTrackOffsetCellSpace;
	} else {
		// right side
		distance = std::abs(distance - optimalTrackOffsetCellSpace);
	}

	// Now calculate distance to corridor start/end to get "round corners"
	double distanceCorridorStartEnd = std::min(t, 1.0 - t) * tToDistanceFactor;
	distance = std::min(distance, distanceCorridorStartEnd);

	// Normalize distance
	double maxDistance = halfWidthCellSpace + optimalTrackOffsetCellSpace; // Used to normalize distance to [0, 1]
	distance = distance / maxDistance;

	// Reverse exponential falloff (-> strong cost increase at center, slow increase near corridor walls)
	// for f(x) = 1.0 - e^(-1.0 * x) the function reaches a value near 1.0, called the "maximum value", for x = normalizationFactor.
	// This factor can then be used to normalize the function so this "maximum value" is reached at x=1.0 which can then be
	// further modified to be at distance = maxDistance.
	// We here use factor = 3.0 which corresponds to a maximum value of 0.95
	// A larger factor brings the maximumValue closer to 1.0 but also significantly flattens the curve
	constexpr double normalizationFactor = 3.0;

	double factor = 1.0 - std::exp(-1.0 * distance * normalizationFactor);

	return static_cast<unsigned char>(maxCost * factor);
}

void DetectedCorridor::computeBounds() {
	constexpr double halfPi = M_PI / 2.0;
	const double halfWidth = width / 2.0;
	
	double corridorAngle = getAngleBetweenPoints(poseStart.pose.position.x, poseStart.pose.position.y, poseEnd.pose.position.x, poseEnd.pose.position.y);
	
	double aX = poseStart.pose.position.x + std::cos(corridorAngle - halfPi) * halfWidth;
	double aY = poseStart.pose.position.y + std::sin(corridorAngle - halfPi) * halfWidth;
	double bX = poseStart.pose.position.x + std::cos(corridorAngle + halfPi) * halfWidth;
	double bY = poseStart.pose.position.y + std::sin(corridorAngle + halfPi) * halfWidth;

	double cX = poseEnd.pose.position.x + std::cos(corridorAngle - halfPi) * halfWidth;
	double cY = poseEnd.pose.position.y + std::sin(corridorAngle - halfPi) * halfWidth;
	double dX = poseEnd.pose.position.x + std::cos(corridorAngle + halfPi) * halfWidth;
	double dY = poseEnd.pose.position.y + std::sin(corridorAngle + halfPi) * halfWidth;
	
	minXMeterSpace = std::min({aX, bX, cX, dX});
	minYMeterSpace = std::min({aY, bY, cY, dY});
	maxXMeterSpace = std::max({aX, bX, cX, dX});
	maxYMeterSpace = std::max({aY, bY, cY, dY});
}

}
