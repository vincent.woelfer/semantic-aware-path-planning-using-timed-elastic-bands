#include "semantic_elastic_bands/DetectedObjects/DetectedPerson.h"

#include "semantic_elastic_bands/Utility.h"
#include <costmap_2d/cost_values.h>
#include <ros/ros.h>
#include <tf/transform_datatypes.h>

#include <utility>

using costmap_2d::LETHAL_OBSTACLE;
using costmap_2d::INSCRIBED_INFLATED_OBSTACLE;
using costmap_2d::NO_INFORMATION;
using costmap_2d::FREE_SPACE;

namespace semantic_elastic_bands {

semantic_elastic_bands::DetectedPerson::DetectedPerson(unsigned long detectionId, unsigned long trackingId, geometry_msgs::PoseStamped pose, double extensionLength) :
		DetectedObject(ObjectType::Person, detectionId, trackingId),
		pose(std::move(pose)),
		cf_reducedVelocity(nullptr),
		extensionLength(extensionLength),
		maxCostRadiusCellSpace(0)
{
	double theta = toAngle(pose.pose.orientation);
	extensionPoint.x = pose.pose.position.x + std::cos(theta) * extensionLength;
	extensionPoint.y = pose.pose.position.y + std::sin(theta) * extensionLength;
}

DetectedPerson::~DetectedPerson() {
	delete cf_reducedVelocity;
}

void DetectedPerson::convertToCellSpace(costmap_2d::Costmap2D* costmap) {
	convertPoseToCellSpaceNoBounds(costmap, pose.pose.position.x, pose.pose.position.y, toAngle(pose.pose.orientation), poseCellSpace);
	pointCellSpace = poseCellSpace.toPoint();
	
	convertPointToCellSpaceNoBounds(costmap, extensionPoint.x, extensionPoint.y, extensionPointCellSpace);
	maxCostRadiusCellSpace = maxCostRadius / costmap->getResolution();
	lethalRadiusCellspace = lethalRadius / costmap->getResolution();
}

void DetectedPerson::updateCostmapBounds(double* min_x, double* min_y, double* max_x, double* max_y) const {
	// Enlarge area by own updated area -> OR areas
	*min_x = std::min(*min_x, std::min(pose.pose.position.x, extensionPoint.x) - maxCostRadius);
	*min_y = std::min(*min_y, std::min(pose.pose.position.y, extensionPoint.y) - maxCostRadius);
	*max_x = std::max(*max_x, std::max(pose.pose.position.x, extensionPoint.x) + maxCostRadius);
	*max_y = std::max(*max_y, std::max(pose.pose.position.y, extensionPoint.y) + maxCostRadius);
}

void DetectedPerson::getCostmapUpdateArea(double costmapResolution, int& minX, int& minY, int& maxX, int& maxY) const {
	int updateRadius = std::ceil(maxCostRadiusCellSpace);
	
	// Shrink area to get overlap of given and own updated area -> AND areas
	minX = std::max(minX, std::min(pointCellSpace.x, extensionPointCellSpace.x) - updateRadius);
	minY = std::max(minY, std::min(pointCellSpace.y, extensionPointCellSpace.y) - updateRadius);
	maxX = std::min(maxX, std::max(pointCellSpace.x, extensionPointCellSpace.x) + updateRadius);
	maxY = std::min(maxY, std::max(pointCellSpace.y, extensionPointCellSpace.y) + updateRadius);	
}

bool DetectedPerson::skipBasedOnRobotPose(const Pose& robotPose) {
	// Do nothing
	return false;
}

unsigned char DetectedPerson::getCellCost(double costmapResolution, const Point& p) const {
	double distToLine = getDistanceToLineSegment(p, pointCellSpace, extensionPointCellSpace);
	if(distToLine > maxCostRadiusCellSpace) {
		return 0;
	}

	double distToCenter = getDistance(p, pointCellSpace);
	
	if(distToCenter <= lethalRadiusCellspace) {
		return static_cast<unsigned char>(LETHAL_OBSTACLE);
	}
	
	// Normalize distances
	distToLine = distToLine / maxCostRadiusCellSpace;
	distToCenter = distToCenter / maxCostRadiusCellSpace;
	
	double costCenter = maxCostCenter * cosineCostFunction(distToCenter);
	double costLine = maxCostExtension * cosineCostFunction(distToLine);

	double value = std::max(costCenter, costLine);
	
	return static_cast<unsigned char>(value);		
}

}
