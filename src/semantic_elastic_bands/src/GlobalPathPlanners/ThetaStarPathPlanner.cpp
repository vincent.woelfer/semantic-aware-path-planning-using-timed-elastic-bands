#include "semantic_elastic_bands/GlobalPathPlanners/ThetaStarPathPlanner.h"
#include "semantic_elastic_bands/LineOfSightCalculations/LineOfSightCalculatorCellCenter.h"
#include <algorithm>
#include <ros/ros.h>
#include <chrono>
#include <utility>
#include <ros/console.h>

namespace semantic_elastic_bands {

float ThetaStarPathPlanner::costOffsetInternal;
float ThetaStarPathPlanner::maxTraversalCostInternal;

/* Implementation of the well-known Theta-Star pathfinding algorithm. This implementation is multi-threaded and uses four threads to explore the four adjacent cells during the expansion step.
 * This includes a line of sight check from the target cell to the previous cell to check if a direct connection which skips the current cell is possible. 
 * This line-of-sight check is performed using the Cohen-Sutherland clipping algorithm, this enables calculation of the cost of the direct connection in the presence of non-uniform traversal costs.
 * This algorithm finds better paths than A*, however, this comes at a much higher runtime cost.
 * */
ThetaStarPathPlanner::ThetaStarPathPlanner(costmap_2d::Costmap2D* map, std::string globalFrameId, int costOffset, int maxTraversalCost) :
		BasePathPlanner(map, std::move(globalFrameId), costOffset, maxTraversalCost),
		threadInformation(map)	
{
	// Also update Theta* internal static variables
	costOffsetInternal = static_cast<float>(costOffset);
	maxTraversalCostInternal = static_cast<float>(maxTraversalCost);
}

std::vector<BasePathPlanner::Point> ThetaStarPathPlanner::computePath(BasePathPlanner::Point start, BasePathPlanner::Point goal) {
	// Init start node
	Node startNode(start);
	startNode.gScore = 0.0;
	startNode.fScore = heuristic(start, goal);

	// Create open/explored set
	NodeMap exploredSet;
	exploredSet.reserve(estimateExploredSetSize(startNode.pos, goal));
	std::vector<Node*> openSetContainer;
	openSetContainer.reserve(estimateOpenSetSize(startNode.pos, goal));
	NodePriorityQueue openSet(CompareNodeFScore(), std::move(openSetContainer));
	
	exploredSet.insert(std::make_pair(startNode.pos, startNode));
	openSet.push(&exploredSet.at(startNode.pos));
	
	// Init Threading
	initThreadPool(&exploredSet, &openSet, goal);

	while(!openSet.empty()) {
		Node* current = openSet.top();		

		// Check if goal reached
		if(ROS_UNLIKELY(current->pos == goal)) {
			initThreadPoolTermination();
			
			std::vector<Point> path;

			// Add path points including start and goal
			while(current != nullptr) {
				path.emplace_back(current->pos.x, current->pos.y);
				current = current->prev;
			}

			std::reverse(path.begin(), path.end());

			ROS_DEBUG("Theta* explored %lu cells, g-Score: %f", exploredSet.size(), exploredSet.at(goal).gScore);

			finishThreadPoolTermination();
			
			return path;
		}

		// Acquire locks
		while(threadInformation.openSetFlag.test_and_set(std::memory_order_acquire)) { }
		while(threadInformation.exploredSetFlag.test_and_set(std::memory_order_acquire)) { } 

		// Start threads
		threadInformation.currentNode = current;
		threadInformation.finishedThreads.store(0, memoryOrder);
		threadInformation.currentNodeId.fetch_add(1, memoryOrder);
				
		// Already neighbors add to explored set
		Point neighbourA(current->pos.x - 1, current->pos.y);
		Point neighbourB(current->pos.x + 1, current->pos.y);
		Point neighbourC(current->pos.x, current->pos.y - 1);
		Point neighbourD(current->pos.x, current->pos.y + 1);		
		
		exploredSet.emplace(neighbourA, Node(neighbourA));
		exploredSet.emplace(neighbourB, Node(neighbourB));
		exploredSet.emplace(neighbourC, Node(neighbourC));
		exploredSet.emplace(neighbourD, Node(neighbourD));
		threadInformation.exploredSetFlag.clear(std::memory_order_release);

		openSet.pop();
		threadInformation.openSetFlag.clear(std::memory_order_release);
		
		// Wait for threads to finish
		while(threadInformation.finishedThreads.load(memoryOrder) < numThreads) { }
	}

	// No path found!
	initThreadPoolTermination();
	finishThreadPoolTermination();
	return {};
}

void ThetaStarPathPlanner::reconfigure(int costOffset, int maxTraversalCost) {
	std::scoped_lock<std::mutex> lock(mutex);
	
	this->costOffset = costOffset;
	this->maxTraversalCost = maxTraversalCost;
	
	// Also update Theta* internal static variables
	costOffsetInternal = static_cast<float>(costOffset);
	maxTraversalCostInternal = static_cast<float>(maxTraversalCost);
}

void ThetaStarPathPlanner::threadPoolFunction(int index, ThreadInformation* info) {
	unsigned long previousNodeId = 1;
	Node* currentNode;

	while(true) {
		// Busy-wait for new node
		while(info->currentNodeId.load(memoryOrder) == previousNodeId) { }
		
		currentNode = info->currentNode;
		previousNodeId = info->currentNodeId;

		// Terminate if requested
		if(ROS_UNLIKELY(!info->isRunning.load(memoryOrder))) {
			break;
		}

		// Generate neighbour
		Point neighbourPoint = currentNode->pos;
		bool canExpand;
		switch(index) {
			case 0:
				neighbourPoint.x -= 1;
				canExpand = neighbourPoint.x >= 0;
				break;
			case 1:
				neighbourPoint.x += 1;
				canExpand = neighbourPoint.x < info->map->getSizeInCellsX();
				break;
			case 2:
				neighbourPoint.y -= 1;
				canExpand = neighbourPoint.y >= 0;
				break;
			case 3:
				neighbourPoint.y += 1;
				canExpand = neighbourPoint.y < info->map->getSizeInCellsY();
				break;
			default:
				ROS_BREAK();				
		}

		// Never connect to previous
		if(currentNode->prev != nullptr && currentNode->prev->pos == neighbourPoint) {
			canExpand = false;
		}

		if(!canExpand) {
			// Signal finished
			info->finishedThreads.fetch_add(1, memoryOrder);
			continue;
		}

		const float targetCost = static_cast<float>(info->map->getCost(neighbourPoint.x, neighbourPoint.y));
		
		if(targetCost > maxTraversalCostInternal) {
			// Signal finished
			info->finishedThreads.fetch_add(1, memoryOrder);
			continue;
		}

		const float sourceCost = static_cast<float>(info->map->getCost(currentNode->pos.x, currentNode->pos.y));
		const float neighbourCost = (sourceCost + targetCost) / 2.0f + costOffsetInternal;
		
		// Perform line-of-sight check
		bool isLineOfSighValid = false;
		float lineOfSightCost;
		if(currentNode->prev != nullptr) {
			lineOfSightCost = lineOfSight(currentNode->prev->pos, neighbourPoint, info->map);
			isLineOfSighValid = lineOfSightCost >= 0.0f;
			lineOfSightCost += currentNode->prev->gScore;
		}

		float viaCurrentCost = currentNode->gScore + neighbourCost;

		// Determine which connection to make
		Node* connectionPartner;
		float g;

		if(isLineOfSighValid && lineOfSightCost <= viaCurrentCost) {
			g = lineOfSightCost;
			connectionPartner = currentNode->prev;
		} else {
			g = viaCurrentCost;
			connectionPartner = currentNode;
		}

		// Access Explored Set
		while(info->exploredSetFlag.test_and_set(std::memory_order_acquire)) {} // Acquire lock
		Node* neighbourNode = &info->exploredSet->at(neighbourPoint); // This only works because the main thread acquires the lock before starting this thread
		info->exploredSetFlag.clear(std::memory_order_release);
		
		bool isNew = neighbourNode->gScore == Node::maxScore;

		// Check if a connection should be made at all	
		if(isNew || g < neighbourNode->gScore) {
			if(isNew) {
				neighbourNode->fScore = g + heuristic(neighbourPoint, info->goalPoint);
			} else {
				// Reuse heuristic
				neighbourNode->fScore = (neighbourNode->fScore - neighbourNode->gScore) + g;
			}
			neighbourNode->gScore = g;
			neighbourNode->prev = connectionPartner;

			// Access Open Set
			while(info->openSetFlag.test_and_set(std::memory_order_acquire)) {} // Acquire lock
			info->openSet->push(neighbourNode);
			info->openSetFlag.clear(std::memory_order_release);
		}		

		// Signal finished
		info->finishedThreads.fetch_add(1, memoryOrder);
	}
}

float ThetaStarPathPlanner::heuristic(const Point& p, const Point& goalPoint) {
	int dX = p.x - goalPoint.x;
	int dY = p.y - goalPoint.y;

	return (float)std::sqrt(dX * dX + dY * dY) * costOffsetInternal;
}

float ThetaStarPathPlanner::lineOfSight(const Point& a, const Point& b, const costmap_2d::Costmap2D* map) {
	LineOfSightCalculatorCellCenter<unsigned char> lineOfSightCalculator(map->getCharMap(), map->getSizeInCellsX(), a.x, a.y, b.x, b.y);

	return lineOfSightCalculator.getLineOfSightCost(costOffsetInternal, maxTraversalCostInternal);
}

int ThetaStarPathPlanner::estimateExploredSetSize(const Point& a, const Point& b) {
	int dX = std::abs(a.x - b.x);
	int dY = std::abs(a.y - b.y);

	return (dX + dY) * 64;
}

int ThetaStarPathPlanner::estimateOpenSetSize(const Point& a, const Point& b) {
	int dX = std::abs(a.x - b.x);
	int dY = std::abs(a.y - b.y);

	return (dX + dY) * 4;
}

int ThetaStarPathPlanner::estimatePathSize(const Point& a, const Point& b) {
	int dX = std::abs(a.x - b.x);
	int dY = std::abs(a.y - b.y);

	return std::max(dX, dY);
}

void ThetaStarPathPlanner::initThreadPool(NodeMap* exploredSet, NodePriorityQueue* openSet, Point goalPoint) {
	// Fil thread information
	threadInformation.exploredSet = exploredSet;
	threadInformation.openSet = openSet;
	threadInformation.goalPoint = goalPoint;
	
	threadInformation.isRunning = true;
	threadInformation.currentNode = nullptr;
	threadInformation.currentNodeId = 1;

	threadInformation.exploredSetFlag.clear();
	threadInformation.openSetFlag.clear();	
		
	for(int i = 0; i < numThreads; i++) {
		threadPool[i] = std::thread(threadPoolFunction, i, &threadInformation);
	}
}

void ThetaStarPathPlanner::initThreadPoolTermination() {
	threadInformation.isRunning.store(false, memoryOrder);
	threadInformation.currentNodeId.store(0, memoryOrder);	
}

void ThetaStarPathPlanner::finishThreadPoolTermination() {
	// Wait for threads to finish
	for(auto & i : threadPool) {
		i.join();
	}
}


}