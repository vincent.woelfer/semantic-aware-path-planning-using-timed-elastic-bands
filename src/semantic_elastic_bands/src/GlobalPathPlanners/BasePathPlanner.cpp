#include "semantic_elastic_bands/GlobalPathPlanners/BasePathPlanner.h"
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <std_msgs/Float64.h>

#include "semantic_elastic_bands/Utility.h"

namespace semantic_elastic_bands {

BasePathPlanner::BasePathPlanner(costmap_2d::Costmap2D* map, std::string globalFrameId, int costOffset, int maxTraversalCost) :
	map(map),
	globalFrameId(std::move(globalFrameId)),
	costOffset(costOffset),
	maxTraversalCost(maxTraversalCost)
{
	ROS_ASSERT(map != nullptr);
	ros::NodeHandle handle("/move_base/");
	planningTimeGlobalPublisher = handle.advertise<std_msgs::Float64>("planning_time_global", 50, true);
}

std::vector<geometry_msgs::PoseStamped> BasePathPlanner::findPath(const geometry_msgs::PoseStamped& start, const geometry_msgs::PoseStamped& goal) {
	ros::WallTime t0 = ros::WallTime::now();

	// Lock this path planner so no other planner can modify the configuration during this planners runtime
	std::scoped_lock<std::mutex> lock(mutex);
	
	/* Do some sanity checks */ 
	// Ensure that start & goal are in the same frame as the costmap
	if(start.header.frame_id != globalFrameId || goal.header.frame_id != globalFrameId) {
		ROS_ERROR("The start and goal pose passed to this planner must be in the same frame as it's costmap!");
	}

	// Convert start & goal to map coordinates
	Point startPoint = Point();
	Point goalPoint = Point();
	
	if(!convertWorldToCostmap(start.pose.position.x, start.pose.position.y, startPoint)) {
		ROS_WARN("GlobalPlanner: Start position is off the global costmap. Planning will always fail from this start.");
		return {};
	}
	if(!convertWorldToCostmap(goal.pose.position.x, goal.pose.position.y, goalPoint)) {
		ROS_WARN("GlobalPlanner: Goal position is off the global costmap. Planning will always fail to this goal.");
		return {};
	}

	ROS_DEBUG("GlobalPlanner: Planning from %f/%f to %f/%f in '%s' frame (%d/%d -> %d/%d in costmap frame)", start.pose.position.x, start.pose.position.y, goal.pose.position.x, goal.pose.position.y, globalFrameId.c_str(),
		                                                                                                startPoint.x, startPoint.y, goalPoint.x, goalPoint.y);

	std::vector<geometry_msgs::PoseStamped> finalPath;

	// Check if we even need to plan
	if(startPoint == goalPoint) {
		// Just return "path" containing original start and goal
		finalPath.push_back(start);
		finalPath.push_back(goal);		
		
	} else {
		// Actually compute path
		std::vector<Point> pointPath = computePath(startPoint, goalPoint);

		ros::WallTime t1 = ros::WallTime::now();

		std_msgs::Float64 msg;
		msg.data = (t1 - t0).toNSec() * 1e-6;
		planningTimeGlobalPublisher.publish(msg);

		if(pointPath.empty()) {
			ROS_WARN("Global planner failed to find a path!");
			return {};
		}

		// Extract path
		finalPath.reserve(pointPath.size() + 2);
		const double pointsRoughlyEqualDist = map->getResolution() * 0.9;

		// Construct the "base" pose
		geometry_msgs::PoseStamped pose;
		pose.header.frame_id = globalFrameId;
		pose.header.stamp = ros::Time::now();
		pose.pose.position.z = 0.0;

		// Prepend exact starting pose in world frame - it's orientation does not get overwritten later on 
		finalPath.push_back(start);
		finalPath.front().header.stamp = pose.header.stamp;

		for(auto& p : pointPath) {
			// Convert points back to world coordinates and interpolate orientation
			double worldX;
			double worldY;

			map->mapToWorld(p.x, p.y, worldX, worldY);

			pose.pose.position.x = worldX;
			pose.pose.position.y = worldY;

			// Only add if different enough compared to previous pose
			if(getDistance(pose.pose.position, finalPath.back().pose.position) > pointsRoughlyEqualDist) {
				finalPath.push_back(pose);
			}
		}

		// Append exact goal or replace if the current last pose is roughly equal
		if(getDistance(finalPath.back().pose.position, goal.pose.position) <= pointsRoughlyEqualDist) {
			finalPath.pop_back();
		}
		finalPath.push_back(goal);
		finalPath.back().header.stamp = pose.header.stamp;

		// Now estimate orientation of all poses to point to the next one except exact start and goal poses
		for(int i = 1; i < finalPath.size() - 1; i++) {
			// Every pose points towards its successor
			const double yaw = getAngleBetweenPoints(finalPath.at(i).pose.position.x, finalPath.at(i).pose.position.y,
											         finalPath.at(i + 1).pose.position.x, finalPath.at(i + 1).pose.position.y);

			tf2::Quaternion orientation;
			orientation.setRPY(0.0, 0.0, yaw);
			orientation.normalize();

			finalPath.at(i).pose.orientation.x = orientation.x();
			finalPath.at(i).pose.orientation.y = orientation.y();
			finalPath.at(i).pose.orientation.z = orientation.z();
			finalPath.at(i).pose.orientation.w = orientation.w();
		}
	}	
	
	//ROS_INFO("GlobalPlanner: Path found in %f ms", (t1 - t0).toNSec() * 1e-6);
	return finalPath;
}

bool BasePathPlanner::convertWorldToCostmap(double worldX, double worldY, BasePathPlanner::Point& mapPoint) const {
	if(worldX < map->getOriginX() || worldY < map->getOriginY()) {
		return false;
	}
	
	mapPoint.x = std::floor((worldX - map->getOriginX()) / map->getResolution());
	mapPoint.y = std::floor((worldY - map->getOriginY()) / map->getResolution());
	
	if(mapPoint.x >= map->getSizeInCellsX() || mapPoint.y >= map->getSizeInCellsY()) {
		return false;
	}
	
	return true;
}


}