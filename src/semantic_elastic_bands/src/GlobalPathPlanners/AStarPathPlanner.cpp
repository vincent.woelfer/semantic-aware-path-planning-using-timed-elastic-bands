#include "semantic_elastic_bands/GlobalPathPlanners/AStarPathPlanner.h"
#include <queue>
#include <algorithm>
#include <utility>
#include <ros/ros.h>
#include <ros/console.h>
#include "semantic_elastic_bands/LineOfSightCalculations/LineOfSightCalculatorCellCenter.h"

namespace semantic_elastic_bands {

AStarPathPlanner::AStarPathPlanner(costmap_2d::Costmap2D* map, std::string globalFrameId, int costOffset, int maxTraversalCost, bool smoothing) :
		BasePathPlanner(map, std::move(globalFrameId), costOffset, maxTraversalCost),
		smoothing(smoothing),
		goalPoint(-1, -1)
{
}

std::vector<BasePathPlanner::Point> AStarPathPlanner::computePath(BasePathPlanner::Point start, BasePathPlanner::Point goal) {
	// Set goal point for heuristic
	goalPoint = goal;
	
	// Init start node
	Node startNode(start);
	startNode.gScore = 0;
	startNode.fScore = heuristic(startNode.pos); // Requires goal point to be set

	NodeMap exploredSet;
	exploredSet.reserve(estimateExploredSetSize(startNode.pos, goalPoint));
	std::vector<Node*> openSetContainer;
	openSetContainer.reserve(estimateOpenSetSize(startNode.pos, goalPoint));
	NodePriorityQueue openSet(CompareNodeFScore(), std::move(openSetContainer));

	exploredSet.insert(std::make_pair(startNode.pos, startNode));
	openSet.push(&exploredSet.at(startNode.pos));
	
	while(!openSet.empty()) {
		Node* current = openSet.top();
		openSet.pop();
		
		if(current->expanded) {
			continue;
		}
		current->expanded = true;

		// Check if goal reached
		if(ROS_UNLIKELY(current->pos == goalPoint)) {
			std::vector<Point> path;
			path.reserve(estimatePathSize(startNode.pos, goalPoint));

			// Add path points including start and goal
			while(current != nullptr) {
				path.emplace_back(current->pos.x, current->pos.y);
				current = current->prev;
			}

			std::reverse(path.begin(), path.end());

			//ROS_DEBUG("A* explored %lu cells, g-Score: %d", exploredSet.size(), exploredSet.at(goalPoint).gScore);
			
			if(smoothing) {
				path = smoothPath(path);
				path = smoothPath(path);	
			}		

			return path;
		}

		generateNeighbours(current->pos);
		Neighbour* n = &neighbourList[0];

		while(n < lastNeighbour) {
			// Get pointer to node or create if nonexistent
			Node* neighbour = &exploredSet.try_emplace(n->point, Node(n->point)).first->second;

			// Skip connection if neighbour was already expanded
			if(neighbour->expanded) {
				n++;
				continue;
			}
			
			bool isNew = neighbour->gScore == Node::maxScore;

			const int g = current->gScore + n->cost;
			if(isNew) {
				neighbour->fScore = g + heuristic(neighbour->pos);
				neighbour->gScore = g;
				neighbour->prev = current;
				openSet.push(neighbour);
			} else {
				// Node was already explored -> reuse memoized heuristic
				if(g < neighbour->gScore) {
					neighbour->fScore = neighbour->fScore + g - neighbour->gScore;
					neighbour->gScore = g;
					neighbour->prev = current;
					openSet.push(neighbour);
				}
			}

			n++;
		}
	}

	// No path found!
	return {};
}

std::vector<BasePathPlanner::Point> AStarPathPlanner::smoothPath(const std::vector<Point>& originalPath) {
	if(originalPath.size() <= 2) {
		return originalPath;
	}

	std::vector<BasePathPlanner::Point> newPath;
	newPath.push_back(originalPath.front());
	
	// We call start - end the direct connection and start - middle - end the "indirect connection".
	// We assume start - middle to always be viable
	// The startPoint is added to the new path as soon as it is assigned as new start
	int startIndex = 0;
	int middleIndex = 1;
	int endIndex = 2;
	
	const unsigned char* mapData = map->getCharMap();
	const unsigned int mapWidth = map->getSizeInCellsX();
	
	const float costOffsetFloat = static_cast<float>(costOffset);
	const float maxTraversalCostFloat = static_cast<float>(maxTraversalCost);
	
	float prevDirectCost = 0;
	bool prevDirectCostUsable = false;
	
	while(true) {
		if(endIndex >= originalPath.size()) {
			// Add middleIndex (should be last point of original path) and terminate
			ROS_ASSERT(middleIndex == originalPath.size() - 1);
			newPath.push_back(originalPath.at(middleIndex));
			break;
		}
		
		Point startPoint = originalPath.at(startIndex);
		Point middlePoint = originalPath.at(middleIndex);
		Point endPoint = originalPath.at(endIndex);
		
		// Compute direct cost and check if connection is possible
		LineOfSightCalculatorCellCenter<unsigned char> losDirect(mapData, mapWidth, startPoint.x, startPoint.y, endPoint.x, endPoint.y);
		float costDirect = losDirect.getLineOfSightCost(costOffsetFloat, maxTraversalCostFloat);
		
		if(costDirect < 0.0) {
			// Direct connection not possible -> advance and add new start
			prevDirectCostUsable = false;
			
			startIndex = middleIndex;
			middleIndex = startIndex + 1;
			endIndex = startIndex + 2;
			newPath.push_back(originalPath.at(startIndex));
			continue;
		}

		// Compute cost along path		
		float costPath;		
		if(prevDirectCostUsable) {
			costPath = prevDirectCost;
		} else {
			LineOfSightCalculatorCellCenter<unsigned char> losPathA(mapData, mapWidth, startPoint.x, startPoint.y, middlePoint.x, middlePoint.y);
			costPath = losPathA.getLineOfSightCost(costOffsetFloat, maxTraversalCostFloat);	
		}
		
		LineOfSightCalculatorCellCenter<unsigned char> losPathB(mapData, mapWidth, middlePoint.x, middlePoint.y, endPoint.x, endPoint.y);
		costPath += losPathB.getLineOfSightCost(costOffsetFloat, maxTraversalCostFloat);
		
		if(costDirect <= costPath) {
			// Direct Connection => Skip intermediate node
			middleIndex++;
			endIndex++;
			
			prevDirectCostUsable = true;
			prevDirectCost = costDirect;			
		} else {
			// Path Connection => Advance as direct connection is not cheaper and add new start
			prevDirectCostUsable = false;
			
			startIndex = middleIndex;
			middleIndex = startIndex + 1;
			endIndex = startIndex + 2;
			newPath.push_back(originalPath.at(startIndex));
		}		
	}
	
	//ROS_DEBUG("Path-Smoothing reduced size from %zu to %zu", originalPath.size(), newPath.size());
	return newPath;
}

void AStarPathPlanner::reconfigure(int costOffset, int maxTraversalCost) {
	std::scoped_lock<std::mutex> lock(mutex);
	
	this->costOffset = costOffset;
	this->maxTraversalCost = maxTraversalCost;
}

int AStarPathPlanner::heuristic(Point p) const {
	// Inspired by https://github.com/riscy/a_star_on_grids#move-costs	
	int dX = std::abs(p.x - goalPoint.x);
	int dY = std::abs(p.y - goalPoint.y);

	return ((E * std::abs(dX - dY) + D * (dX + dY)) / 2) * W * costOffset;
}

void AStarPathPlanner::generateNeighbours(const Point& pos) {
	lastNeighbour = neighbourList;

	// Generate neighbour candidates, skip one side if point is on border
	const unsigned int xLow = pos.x ? pos.x - 1 : pos.x;
	const unsigned int yLow = pos.y ? pos.y - 1 : pos.y;
	const unsigned int xHigh = (pos.x == map->getSizeInCellsX() - 1) ? map->getSizeInCellsX() - 1 : pos.x + 1;
	const unsigned int yHigh = (pos.y == map->getSizeInCellsY() - 1) ? map->getSizeInCellsY() - 1 : pos.y + 1;
	
	for(unsigned int x = xLow; x <= xHigh; x++) {
		for(unsigned int y = yLow; y <= yHigh; y++) {
			if(x != pos.x || y != pos.y) {
				int value = static_cast<int>(map->getCost(x, y));
				if(value <= maxTraversalCost) {
					// Convert value to true cost accounting for if this is a cardinal or diagonal move
					// This is technically not correct since we dont take the cost of the source into account. 
					// However, this makes no difference in practice and avoids another cost lookup
					int moveType = (x == pos.x || y == pos.y) ? C : D;
					int cost = (value + costOffset) * moveType;

					*lastNeighbour++ = Neighbour((int) x, (int) y, cost);
				}
			}
		}
	}
}

int AStarPathPlanner::estimateExploredSetSize(const Point& a, const Point& b) {
	int dX = std::abs(a.x - b.x);
	int dY = std::abs(a.y - b.y);

	return (dX + dY) * 64;
}

int AStarPathPlanner::estimateOpenSetSize(const Point& a, const Point& b) {
	int dX = std::abs(a.x - b.x);
	int dY = std::abs(a.y - b.y);

	return (dX + dY) * 4;
}

int AStarPathPlanner::estimatePathSize(const Point& a, const Point& b) {
	int dX = std::abs(a.x - b.x);
	int dY = std::abs(a.y - b.y);

	return std::max(dX, dY);
}


}