#include "semantic_elastic_bands/SemanticElasticBand.h"

#include <ros/ros.h>
#include <Eigen/Core>
#include <utility>
#include "semantic_elastic_bands/Utility.h"

namespace semantic_elastic_bands {

SemanticElasticBand::SemanticElasticBand(const std::vector<Pose>& path, bool isFinalGoal, const SebConfig& config, double mapResolution) :
	config(config),
	mapResolution(mapResolution),
	velocityConstraintStart(true, 0.0, 0.0),
	velocityConstraintGoal(true, 0.0, 0.0)
{
	constructFromInitialPath(path, isFinalGoal);
}

SemanticElasticBand::~SemanticElasticBand() = default;

void SemanticElasticBand::addPose(Eigen::Vector2d position, double theta) {
	poses.emplace_back(std::move(position), theta);
}

void SemanticElasticBand::addPose(const Pose& pose) {
	poses.emplace_back(pose);
}

void SemanticElasticBand::insertPose(int index, const Pose& pose) {
	poses.insert(poses.begin() + index, Pose(pose));
}

void SemanticElasticBand::deletePose(int index) {
	ROS_ASSERT(index < poses.size());
	poses.erase(poses.begin() + index);
}

void SemanticElasticBand::deletePoses(int startIndex, int endIndex) {
	ROS_ASSERT(startIndex < poses.size() && endIndex < poses.size());
	poses.erase(poses.begin() + startIndex, poses.begin() + endIndex);
}

void SemanticElasticBand::addTimeDiff(double timeDiff) {
	ROS_ASSERT(timeDiff > 0);
	timeDiffs.push_back(timeDiff);
}

void SemanticElasticBand::insertTimeDiff(int index, double timeDiff) {
	ROS_ASSERT(timeDiff > 0);
	timeDiffs.insert(timeDiffs.begin() + index, timeDiff);
}

void SemanticElasticBand::deleteTimeDiff(int index) {
	ROS_ASSERT(index < timeDiffs.size());
	timeDiffs.erase(timeDiffs.begin() + index);
}

void SemanticElasticBand::deleteTimeDiffs(int startIndex, int endIndex) {
	ROS_ASSERT(startIndex < timeDiffs.size() && endIndex < timeDiffs.size());
	timeDiffs.erase(timeDiffs.begin() + startIndex, timeDiffs.begin() + endIndex);
}

void SemanticElasticBand::addPoseAndTimeDiff(const Pose& pose, double timeDiff) {
	ROS_ASSERT_MSG(poses.size() == timeDiffs.size() + 1, "Add a starting pose first, only an existing band can be expanded by a pose/timeDiff pair!");
	addPose(pose);
	addTimeDiff(timeDiff);
}

const std::vector<Pose>& SemanticElasticBand::getPoses() const {
	return poses;
}

const std::vector<double>& SemanticElasticBand::getTimeDiffs() const {
	return timeDiffs;
}

const std::vector<Pose>& SemanticElasticBand::getTransformedPoses() const {
	return transformedPoses;
}

std::vector<Pose>& SemanticElasticBand::accessPoses() {
	return poses;
}

std::vector<double>& SemanticElasticBand::accessTimeDiffs() {
	return timeDiffs;
}

void SemanticElasticBand::updateTransformedPoses(double mapOriginX, double mapOriginY, double resolution) {
	ROS_ASSERT(resolution == mapResolution);
	
	transformedPoses.clear();
	transformedPoses.reserve(poses.size());
	
	for(const Pose& pose : poses) {
		// Transform cellspace to costmap. Dont add 0.5 since the poses are not constrained to cell centers
		double x = mapOriginX + (pose.position.x() * resolution);
		double y = mapOriginY + (pose.position.y() * resolution);
		
		transformedPoses.emplace_back(x, y, pose.theta);		
	}
}

int SemanticElasticBand::getNumberOfDirectionChanges() const {
	if(poses.size() < 3) {
		return 0;
	}
	
	int numDirectionChanges = 0;
	
	for(int i = 1; i < poses.size() - 1; i++) {
		double dx1 = poses.at(i).position.x() - poses.at(i - 1).position.x();
		double dy1 = poses.at(i).position.y() - poses.at(i - 1).position.y();
		double dx2 = poses.at(i + 1).position.x() - poses.at(i).position.x();
		double dy2 = poses.at(i + 1).position.y() - poses.at(i).position.y();

		// Check for backwards motion and adjust sign of vel accordingly
		double dir1 = dot(dx1, dy1, std::cos(poses.at(i - 1).theta), std::sin(poses.at(i - 1).theta));
		double dir2 = dot(dx2, dy2, std::cos(poses.at(i).theta), std::sin(poses.at(i).theta));
		
		// Direction changes when dir1 and dir2 have different signs
		if(sign(dir1) != sign(dir2)) {
			numDirectionChanges++;
		}
	}
	
	return numDirectionChanges;
}

void SemanticElasticBand::constructFromInitialPath(const std::vector<Pose>& path, bool isFinalGoal) {
	ROS_ASSERT_MSG(poses.empty() && timeDiffs.empty(), "Cant construct seb if poses/timeDiffs are already present!");
	ROS_ASSERT_MSG(path.size() >= 2, "Cant construct seb from initial path with less than two poses!");
		
	// Reserve at least enough for all required poses
	poses.reserve(path.size());
	timeDiffs.reserve(path.size());

	// This function expects the path to be in cell space -> no conversion here
	// Estimate orientation of all poses except start/goal. This is necessary to more accurately estimate deltaT
	// Init the path completely with backwards motions if the goal is behind the starting position AND near enough
	bool backwards = false;
	
	if(config.optimization.allowBackwardsInitialization && isFinalGoal) {
		const double& startX = path.front().position.x(); 
		const double& startY = path.front().position.y();
		const double& goalX = path.back().position.x();
		const double& goalY = path.back().position.y();
		
		double angleStartToGoal = getAngleBetweenPoints(startX, startY, goalX, goalY);
		double distanceToGoal = getDistance(startX, startY, goalX, goalY) * mapResolution;
		
		bool isGoalBehindStart = rad2deg(std::abs(getAngleDifference(path.front().theta, angleStartToGoal))) >= 120.0;
		bool startAndGoalSameDirection = rad2deg(std::abs(getAngleDifference(path.front().theta, path.back().theta))) <= 45.0;
		
		if(isGoalBehindStart && startAndGoalSameDirection && distanceToGoal <= 2.5) {
			backwards = true;
			ROS_INFO("Initializing band with backwards motion!");
		}	
	}
	
	// Add starting position
	addPose(path.front());
	
	// Loop over poses except first and last, add i pointing towards i + 1
	for(int i = 1; i < path.size() - 1; i++) {
		double yaw = getAngleBetweenPoints(path.at(i), path.at(i + 1));
		
		if(backwards) {
			yaw = normalizeTheta(yaw + M_PI);
		}
		
		Pose newPose(path.at(i).position, yaw);	
		const double timeDiff = estimateTimeDifference(poses.back(), newPose);
		addPoseAndTimeDiff(newPose, timeDiff);		
	}
	
	// Add goal position
	const double timeDiff = estimateTimeDifference(poses.back(), path.back());
	addPoseAndTimeDiff(path.back(), timeDiff);

	autoResize(false);	
}

void SemanticElasticBand::updateStartGoal(const Pose& start, const Pose& goal) {
	// Find pose nearest to new start and replace it. Prune any earlier states
	double minDistanceSquared = getDistanceSquared(start, poses.front());
	int nearestPoseIndex = 0;
	
	// Stop comparing if distances only grow larger
	double previousDistanceSquared = minDistanceSquared;
	int largerThanPreviousCount = 0;
	
	for(int i = 1; i < poses.size() - 1; i++) {
		double distanceSquared = getDistanceSquared(start, poses.at(i));
		
		if(distanceSquared <= minDistanceSquared) {
			minDistanceSquared = distanceSquared;
			nearestPoseIndex = i;
		}
		
		// Check if distances are only growing -> stop comparing the whole band
		if(distanceSquared > previousDistanceSquared) {
			largerThanPreviousCount++;
		} else {
			largerThanPreviousCount = 0;
		}
		previousDistanceSquared = distanceSquared;
		
		if(largerThanPreviousCount >= 5) {
			break;
		}
	}
	
	if(nearestPoseIndex > 0) {
		deletePoses(1, nearestPoseIndex);
		deleteTimeDiffs(1, nearestPoseIndex);
	}
	
	// Overwrite first pose with new start and last pose with goal, re-estimate time differences
	poses.front() = start;
	timeDiffs.front() = estimateTimeDifference(poses.at(0), poses.at(1));
	
	if(poses.back() != goal) {
		poses.back() = goal;
		timeDiffs.back() = estimateTimeDifference(poses.at(poses.size() - 2), poses.back());	
	}	
}

void SemanticElasticBand::autoResize(bool fastMode) {
	// Sanity checks
	ROS_ASSERT_MSG(poses.size() >= 2, "Cant resize seb with less than 2 poses!");
	ROS_ASSERT((poses.size() == timeDiffs.size() + 1));
	
	ros::WallTime t0 = ros::WallTime::now();
	
	int oldSize = poses.size();
	int iteration = 0;
	int maxIterations = config.optimization.maxResizeIterations;
	
	const double referenceTimeDifference = config.optimization.referenceTimeDifference;
	const double hysteresisTimeDiff = referenceTimeDifference * config.optimization.hysteresisTimeDiffFactor; 
	double currHysteresisTimeDiff = hysteresisTimeDiff;
	double hysteresisIncreaseFactor = config.optimization.hysteresisIncreaseFactor;

	if(fastMode) {
		maxIterations = std::ceil(maxIterations * 0.2);
		hysteresisIncreaseFactor *= 2.0;
	}
	
	bool modified = true;

	while(iteration < maxIterations && modified) {
		iteration++;
		modified = false;
		
		// Iterate over pose pairs
		for(int i = 0; i < poses.size() - 1; i++) {
			const double timeDiff = timeDiffs.at(i);
			
			// Add Pose
			if(poses.size() < config.optimization.maxElasticBandSamples && (timeDiff > referenceTimeDifference + currHysteresisTimeDiff)) {
				double halfTimeDiff = timeDiff / 2.0;
				timeDiffs.at(i) = halfTimeDiff;				
				insertPose(i + 1, Pose::constructAverage(poses.at(i), poses.at(i + 1)));
				insertTimeDiff(i + 1, halfTimeDiff);
				modified = true;

			// Remove Pose
			} else if(poses.size() > config.optimization.minElasticBandSamples && (timeDiff < referenceTimeDifference - currHysteresisTimeDiff)) {
				// Check if we are in the middle of the band or at the end
				if(i + 1 < poses.size() - 1) {
					// Middle of band -> Merge timeDiffs and delete pose
					timeDiffs.at(i + 1) += timeDiffs.at(i);
					deleteTimeDiff(i);
					deletePose(i + 1);
				} else {
					// End of band -> Can't delete i + 1 so we delete i and merge timeDiffs again
					timeDiffs.at(i - 1) += timeDiffs.at(i);
					deleteTimeDiff(i);
					deletePose(i);
				}
				
				modified = true;
			}
		}
		
		// Increase hysteresis by % of basis value
		currHysteresisTimeDiff += (hysteresisTimeDiff * hysteresisIncreaseFactor);
	}
	
	// Sanity checks
	ROS_ASSERT((poses.size() == timeDiffs.size() + 1) || (poses.size() == 1 && timeDiffs.empty()));

	if(iteration == maxIterations && !fastMode) {
		ROS_WARN("AutoResize aborted after %d iterations! Consider increasing the hysteresisTimeDiffFactor.", maxIterations);
	}

	ros::WallTime t1 = ros::WallTime::now();
	//ROS_INFO("SEB resized from %d -> %lu in %d iterations, took %3.2f ms", oldSize, poses.size(), iterations, (t1 - t0).toNSec() * 1e-6);
}

double SemanticElasticBand::getBandLength() const {
	double totalDistance = 0;
	
	for(int i = 0; i < poses.size() - 1; i++) {
		totalDistance += (poses.at(i).position - poses.at(i + 1).position).norm();
	}
	
	return totalDistance * mapResolution;
}

double SemanticElasticBand::getBandDuration() const {
	double totalDuration = 0;
	
	for(double timeDiff : timeDiffs) {
		totalDuration += timeDiff;
	}
	
	return totalDuration;
}

double SemanticElasticBand::getMapResolution() const {
	return mapResolution;
}

double SemanticElasticBand::estimateTimeDifference(const Pose& a, const Pose& b) const {
	// Get drive direction
	double dx = b.position.x() - a.position.x();
	double dy = b.position.y() - a.position.y();
	bool backwards = (dot(dx, dy, std::cos(a.theta), std::sin(a.theta)) < 0.0);
	double maxVelX = backwards ? config.robot.maxVelXBackwards : config.robot.maxVelX;
	
	double distance = Pose::getLinearDistance(a, b) * mapResolution;
	double angleDifference = std::abs(normalizeTheta(b.theta - a.theta));
	
	// Compute exact arc length. Ignore the parameter config.optimization.useExactArcLength since 
	// this is during the initialization where the accuracy is preferred 
	if(angleDifference > 0.0) {
		double radius = distance / (2.0 * std::sin(angleDifference / 2.0));
		distance = std::abs(angleDifference * radius);
	}

	double dtLinear = distance / (maxVelX * config.planning.initializationMaxSpeedPercentage);
	double dtRotational = angleDifference / (config.robot.maxVelTheta * config.planning.initializationMaxSpeedPercentage);
	
	return std::max({dtLinear, dtRotational, 0.1});
}

const SebConfig& SemanticElasticBand::getConfig() const {
	return config;
}

std::vector<double> SemanticElasticBand::getLinearVelocities() const {
	std::vector<double> linearVelocities;
	
	// TODO

	for(int i = 0; i < poses.size() - 1; i++) {
		Eigen::Vector2d diff = poses.at(i + 1).position - poses.at(i).position; 
		const double dist = diff.norm() * mapResolution;
		const double direction = dot(diff.x(), diff.y(), ceres::cos(poses.at(i).theta), ceres::sin(poses.at(i).theta));

		double vel = dist / std::abs(timeDiffs.at(i));
		vel *= sign(direction);		
		
		linearVelocities.push_back(vel);
	}
	
	return linearVelocities;	
}

std::vector<double> SemanticElasticBand::getRotationalVelocities() const {
	std::vector<double> rotationalVelocities;

	for(int i = 0; i < poses.size() - 1; i++) {
		const double angleDiff = normalizeTheta(poses.at(i + 1).theta - poses.at(i).theta);
		const double vel = angleDiff / std::abs(timeDiffs.at(i));
		rotationalVelocities.push_back(vel);
	}
	
	return rotationalVelocities;
}

void SemanticElasticBand::verifyBandCorrectness(const std::string& testLocation) const {
	int numIdenticalPoses = 0;
	int numNegativeTimeDiffs = 0;
	double maxNegativeTimeDiff = 1.0;
	
	// Verify that seb has no identical positions
	for(int i = 0; i < poses.size() - 1; i++) {
		if(poses.at(i).position == poses.at(i + 1).position) {
			numIdenticalPoses++;
		}
	}
	// Verify that seb has no negative timediffs
	for(double timeDiff : timeDiffs) {
		if(timeDiff <= 0.0) {
			numNegativeTimeDiffs++;
			maxNegativeTimeDiff = std::min(maxNegativeTimeDiff, timeDiff);
		}
	}

	if(numIdenticalPoses > 0 || numNegativeTimeDiffs > 0) {
		ROS_WARN("Found error in band! Test location: %s", testLocation.c_str());	
	}
	
	if(numIdenticalPoses > 0) {
		ROS_WARN("Found %d identical poses in band!", numIdenticalPoses);
	}
	
	if(numNegativeTimeDiffs > 0) {
		ROS_WARN("Found %d non-positive timediffs in band! Largest error: %f", numNegativeTimeDiffs, maxNegativeTimeDiff);
	}
}

FixedVelocityConstraint SemanticElasticBand::getVelocityConstraintStart() const {
	return velocityConstraintStart;
}

FixedVelocityConstraint SemanticElasticBand::getVelocityConstraintGoal() const {
	return velocityConstraintGoal;
}

void SemanticElasticBand::setVelocityConstraintStart(FixedVelocityConstraint constraint) {
	velocityConstraintStart = constraint;
}

void SemanticElasticBand::setVelocityConstraintGoal(FixedVelocityConstraint constraint) {
	velocityConstraintGoal = constraint;
}


}
