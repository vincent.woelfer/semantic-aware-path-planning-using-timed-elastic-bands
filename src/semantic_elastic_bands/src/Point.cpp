#include "semantic_elastic_bands/Point.h"
#include "semantic_elastic_bands/Utility.h"

namespace semantic_elastic_bands {

Point::Point() : 
		x(0),
		y(0)
{
}

Point::Point(int x, int y) :
	x(x),
	y(y)
{
}

bool Point::operator==(const Point& other) const {
	return this->x == other.x && this->y == other.y;
}

bool Point::operator!=(const Point& other) const {
	return !(other == *this);
}

}