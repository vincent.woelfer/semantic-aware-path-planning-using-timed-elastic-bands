#include "semantic_elastic_bands/Optimizer.h"

#include <ceres/ceres.h>
#include <semantic_elastic_bands/CostFunctions/CF_KinematicsCarlike.h>
#include "semantic_elastic_bands/DetectedObjects/DetectedPerson.h"
#include "semantic_elastic_bands/SemanticElasticBand.h"
#include "semantic_elastic_bands/CostFunctions/CF_ShortestPath.h"
#include "semantic_elastic_bands/CostFunctions/CF_TimeOptimal.h"
#include "semantic_elastic_bands/CostFunctions/CF_OccupancyGrid.h"
#include "semantic_elastic_bands/CostFunctions/CF_Velocity.h"
#include "semantic_elastic_bands/CostFunctions/CF_ReducedVelocity.h"
#include "semantic_elastic_bands/CostFunctions/CF_Acceleration.h"
#include "semantic_elastic_bands/CostFunctions/CF_KinematicsDiffDrive.h"

namespace semantic_elastic_bands {

Optimizer::Optimizer(SemanticElasticBand* seb, const SebConfig& config, costmap_2d::Costmap2D* costmap, const std::vector<std::unique_ptr<DetectedObject>>& detectedObjects) :
	seb(seb),
	config(config),
	costmap(costmap),
	detectedObjects(detectedObjects)
{
	ROS_ASSERT_MSG(seb->getPoses().size() >= 2, "Band with less than 2 elements can't be optimized!");
	ROS_ASSERT_MSG(seb->getPoses().size() == seb->getTimeDiffs().size() + 1, "Band structure is not valid!");
	ROS_ASSERT_MSG(seb->getMapResolution() == costmap->getResolution(), "Band was initialized with different resolution than the costmap passed to the optimizer! (%f vs %f)", seb->getMapResolution(), costmap->getResolution());

	setOptions();	
	createCostFunctions();
}

bool semantic_elastic_bands::Optimizer::optimize() {
	const int iterations = config.optimization.numOptimizerIterations;
	bool stepSuccessful;
	
	for(int i = 0; i < iterations; i++) {
		// Auto-resize only if not the first iteration
		if(i != 0) {
			seb->autoResize(true);
		}
		
		// Quickly check if current seb is a valid starting point for the optimizer
		bool progressMade;
		
		if(isBandUsableForOptimization()) {
			// Compute relevant objects
			std::vector<std::vector<DetectedObject*>> relevantObjectsPerPose = computeRelevantObjectsPerPose();

			// Create Problem
			ceres::Solver::Summary summary;
			ceres::Problem problem(problemOptions);
			constructOptimizationProblem(problem, relevantObjectsPerPose);

			ceres::Solve(solverOptions, &problem, &summary);

			stepSuccessful = isSolutionUsable(summary);
			
			if(summary.IsSolutionUsable()) {
				progressMade = summary.iterations.back().iteration > 0;

				OptimizationStep step(i,
				                      (int)(summary.iterations.back().cumulative_time_in_seconds * 1000.0),
				                      summary.iterations.back().iteration,
				                      seb->poses.size(),
				                      stepSuccessful,
				                      ceres::TerminationTypeToString(summary.termination_type));
				optimizationSummary.addStep(step);
			} else {
				// Ceres reported a serious error -> summary might not be viable	
				progressMade = false;
				OptimizationStep step(i,
				                      0,
				                      0,
				                      seb->poses.size(),
				                      stepSuccessful,
				                      "INITIAL_STATE_INVALID");
				optimizationSummary.addStep(step);
			}			
		} else {
			stepSuccessful = false;
			progressMade = false;

			OptimizationStep step(i,
			                      0,
			                      0,
			                      seb->poses.size(),
			                      stepSuccessful,
			                      "INITIAL_STATE_INVALID");
			optimizationSummary.addStep(step);
		}	
				
		// Continue only if step was successful AND progress was made
		if(!stepSuccessful || !progressMade) {
			break;
		}
	}

	deleteCostFunctions();
	
	// Update seb transformed poses for further processing by planner/visualization
	if(optimizationSummary.successful) {
		seb->updateTransformedPoses(costmap->getOriginX(), costmap->getOriginY(), costmap->getResolution());
	}
	
	return stepSuccessful;
}

bool Optimizer::isSolutionUsable(const ceres::Solver::Summary& solverSummary) const {
	if(!solverSummary.IsSolutionUsable() || seb->getPoses().size() < 2) {
		ROS_ERROR("Ceres solver returned invalid solution!");
		return false;
	}
	
	double isSolutionUsableToleranceFactor = config.optimization.isSolutionUsableToleranceFactor;
	const double maxAllowedTimeDiff = config.optimization.referenceTimeDifference * 3.0 * isSolutionUsableToleranceFactor;
	const double maxAllowedDistanceCellSpace = config.robot.maxVelX * config.optimization.referenceTimeDifference * isSolutionUsableToleranceFactor / costmap->getResolution();
	const double maxAllowedAngleDifference = config.robot.maxVelTheta * config.optimization.referenceTimeDifference * isSolutionUsableToleranceFactor;
	
	const std::vector<Pose>& poses = seb->getPoses();
	
	int posesToVerify = std::min(15, (int)poses.size());
	
	// Verify that all band poses are in passable space, have no negative time-diffs and are not too far away
	// Iterate over pose pairs (i, i+1)
	for(int i = 0; i < posesToVerify; i++) {
		// Verify pose itself
		const Pose& poseA = poses.at(i);
		const Point p = poseA.toPoint();

		unsigned char cost = costmap->getCost(p.x, p.y);
		if(cost > config.planning.maxTraversalCost && cost != costmap_2d::NO_INFORMATION) {
			ROS_ERROR("Optimization result is unusable because it crosses non-traversable parts of the map!");
			return false;
		}
		
		// Verify pose pair (i, i+1)
		if(i < posesToVerify - 1) {
			double timeDiff = seb->getTimeDiffs().at(i);	
			if(timeDiff <= 0.0 || timeDiff > maxAllowedTimeDiff) {
				ROS_ERROR("Optimization result is unusable because one timeDiff is invalid. TimeDiff: %f, max: %f", timeDiff, maxAllowedTimeDiff);
				return false;				
			}

			const Pose& poseB = poses.at(i + 1);
			
			double distance = getDistance(poseA, poseB);
			if(distance == 0.0 || distance > maxAllowedDistanceCellSpace) {
				ROS_ERROR("Optimization result is unusable because two poses are too far away or identical. Distance: %f, max: %f", distance * costmap->getResolution(), maxAllowedDistanceCellSpace * costmap->getResolution());
				return false;
			}
			
			double angleDifference = std::abs(getAngleDifference(poseA.theta, poseB.theta));
			if(angleDifference > maxAllowedAngleDifference) {
				ROS_ERROR("Optimization result is unusable because the angle difference between two poses is too high. AngleDistance: %f, max: %f", rad2deg(angleDifference), rad2deg(maxAllowedAngleDifference));
				return false;
			}
		}
	}
	
	return true;
}

bool Optimizer::isBandUsableForOptimization() const {
	const double marginInCells = config.planning.localCostmapSafetyMarginInMeter / costmap->getResolution();

	const std::vector<Pose>& poses = seb->getPoses();
	
	for(const Pose& pose : poses) {
		// Verify that pose is within costmap margin
		if(pose.position.x() < marginInCells || pose.position.x() > costmap->getSizeInCellsX() - marginInCells ||
		   pose.position.y() < marginInCells || pose.position.y() > costmap->getSizeInCellsY() - marginInCells) {
			ROS_ERROR("Band pose given to optimizer lies outside of costmap (with margin: %f cells), treating this optimization attempt as failed!", marginInCells);
			return false;
		}
		        
		// Verify that pose is in traversable part of the costmap
		const Point p = pose.toPoint();
		unsigned char cost = costmap->getCost(p.x, p.y);
		if(cost > config.planning.maxTraversalCost && cost != costmap_2d::NO_INFORMATION) {
			ROS_ERROR("Band given to optimizer crosses non-traversable parts of costmap, treating this optimization attempt as failed! (cost: %d, maxTraversable: %f)!", cost, config.planning.maxTraversalCost);
			return false;
		}		               
	}
	
	return true;
}

void Optimizer::constructOptimizationProblem(ceres::Problem& problem, const std::vector<std::vector<DetectedObject*>>& relevantObjectsPerPose) {
	// General constraints
	addConstraintTimeOptimal(problem);
	addConstraintShortestPath(problem);
	addConstraintOccupancyGrid(problem);
	addConstraintVelocity(problem);
	addConstraintAcceleration(problem);
	addConstraintKinematics(problem);
	
	// Object dependent constraints
	addConstraintReducedVelocity(problem, relevantObjectsPerPose);
	
	const std::vector<Pose>& poses = seb->getPoses();
	const std::vector<double>& timeDiffs = seb->getTimeDiffs();
	
	// Keep first/last pose fixed
	problem.SetParameterBlockConstant(poses.front().position.data());
	problem.SetParameterBlockConstant(poses.back().position.data());

	if(problem.HasParameterBlock(&poses.front().theta) && config.optimization.fixStartOrientation) {
		problem.SetParameterBlockConstant(&poses.front().theta);
	}
	if(problem.HasParameterBlock(&poses.back().theta) && config.optimization.fixGoalOrientation) {
		problem.SetParameterBlockConstant(&poses.back().theta);
	}
}

void semantic_elastic_bands::Optimizer::setOptions() {
	// Solver Options
	solverOptions.minimizer_progress_to_stdout = false;
	solverOptions.max_num_iterations = config.optimization.ceresMaxNumIterations;
	solverOptions.max_solver_time_in_seconds = config.optimization.ceresMaxSolverTime;
	solverOptions.num_threads = config.optimization.ceresNumThreads;

	solverOptions.function_tolerance = 1e-4;
	solverOptions.gradient_tolerance = solverOptions.function_tolerance * 1e-4;
	solverOptions.parameter_tolerance = solverOptions.function_tolerance * 1e-2;

	solverOptions.minimizer_type = ceres::MinimizerType::TRUST_REGION;
	solverOptions.trust_region_strategy_type = ceres::TrustRegionStrategyType::DOGLEG; // Dogleg seems significantly better than traditional Levenberg-Marquardt
	solverOptions.linear_solver_type = ceres::LinearSolverType::SPARSE_NORMAL_CHOLESKY;
	solverOptions.sparse_linear_algebra_library_type = ceres::SparseLinearAlgebraLibraryType::SUITE_SPARSE;
	solverOptions.preconditioner_type = ceres::PreconditionerType::JACOBI; // Or Identity ( no preconditioner)
	
	solverOptions.use_nonmonotonic_steps = false;
	
	solverOptions.use_inner_iterations = false;
	solverOptions.check_gradients = false;
	solverOptions.gradient_check_relative_precision = 0.1;
	
	// Problem Options
	problemOptions.cost_function_ownership = ceres::DO_NOT_TAKE_OWNERSHIP;
}

void Optimizer::createCostFunctions() {
	cf_TimeOptimal = CF_TimeOptimal::createCostFunction(config.optimization.weight_optimalTime);
	cf_ShortestPath = CF_ShortestPath::createCostFunction(config.optimization.weight_shortestPath); // * mapResolution ???
	cf_OccupancyGrid = CF_OccupancyGrid::createCostFunction(costmap, config.optimization.weight_occupancyGrid, config.planning.maxTraversalCost);
	cf_Velocity = CF_Velocity::createCostFunction(config.optimization.weight_velocity, config.optimization.weight_rotationDirection, costmap->getResolution(), config);
	
	// Acceleration
	cf_Acceleration = CF_Acceleration::createCostFunction(config.optimization.weight_acceleration, costmap->getResolution(), config);
	if(seb->getVelocityConstraintStart().isFixed) {
		cf_AccelerationStart = CF_AccelerationStart::createCostFunction(config.optimization.weight_acceleration, costmap->getResolution(), config, seb->getVelocityConstraintStart().velX, seb->getVelocityConstraintStart().velTheta);
	}
	if(seb->getVelocityConstraintGoal().isFixed) {
		cf_AccelerationGoal = CF_AccelerationGoal::createCostFunction(config.optimization.weight_acceleration, costmap->getResolution(), config, seb->getVelocityConstraintGoal().velX, seb->getVelocityConstraintGoal().velTheta);
	}

	// Kinematics	
	if(config.robot.minTurningRadius == 0.0 || config.optimization.weight_kinematicTurningRadius == 0.0) {
		cf_Kinematics = CF_KinematicsDiffDrive::createCostFunction(config.optimization.weight_kinematicNonHolonomic, config.optimization.weight_kinematicDriveDirection);
	} else {
		cf_Kinematics = CF_KinematicsCarlike::createCostFunction(config.optimization.weight_kinematicNonHolonomic,
														         config.optimization.weight_kinematicTurningRadius, costmap->getResolution(), config);
	}
	
	for(const std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
		switch(detectedObject->getObjectType()) {
			case DetectedObject::ObjectType::Person:
			{
				DetectedPerson* person = reinterpret_cast<DetectedPerson*>(detectedObject.get());
				person->cf_reducedVelocity = CF_ReducedVelocity::createCostFunction(config.optimization.weight_reducedVelocity,
				                                                                    costmap->getResolution(), config, person->poseCellSpace.position,
				                                                                    person->lowerDistanceBound, person->upperDistanceBound, person->velocityReduction);
				break;
			}
			case DetectedObject::ObjectType::AGV:
			{
				// Currently no cost function
				break;
			}
			case DetectedObject::ObjectType::Corridor:
			{
				// Currently no cost function
				break;
			}
		}
	}
}

void Optimizer::deleteCostFunctions() {
	delete cf_TimeOptimal;
	delete cf_ShortestPath;
	delete cf_OccupancyGrid;
	delete cf_Velocity;

	delete cf_Acceleration;
	if(seb->getVelocityConstraintStart().isFixed) {
		delete cf_AccelerationStart;
	}
	if(seb->getVelocityConstraintGoal().isFixed) {
		delete cf_AccelerationGoal;
	}

	delete cf_Kinematics;
	
	// Cost functions in detected Objects are deleted in their respective destructor 
}

void Optimizer::addConstraintTimeOptimal(ceres::Problem& problem) {
	if(config.optimization.weight_optimalTime <= 0.0) {
		return;
	}

	std::vector<double>& timeDiffs = seb->accessTimeDiffs();
	for(double & timeDiff : timeDiffs) {
		problem.AddResidualBlock(cf_TimeOptimal, nullptr, &timeDiff);
		problem.SetParameterLowerBound(&timeDiff, 0, 0.01);
	}
}

void Optimizer::addConstraintShortestPath(ceres::Problem& problem) {
	if(config.optimization.weight_shortestPath <= 0.0) {
		return;
	}

	std::vector<Pose>& poses = seb->accessPoses();
	for(int i = 0; i < poses.size() - 1; i++) {
		problem.AddResidualBlock(cf_ShortestPath, nullptr,
						        poses.at(i).position.data(), poses.at(i + 1).position.data());
	}
}

void Optimizer::addConstraintOccupancyGrid(ceres::Problem& problem) {
	if(config.optimization.weight_occupancyGrid <= 0.0) {
		return;
	}

	std::vector<Pose>& poses = seb->accessPoses();
	for(int i = 1; i < poses.size() - 1; i++) {
		problem.AddResidualBlock(cf_OccupancyGrid, nullptr, poses.at(i).position.data());
	}

}

void Optimizer::addConstraintVelocity(ceres::Problem& problem) {
	if(config.optimization.weight_velocity <= 0.0 && config.optimization.weight_rotationDirection == 0.0) {
		return;
	}

	std::vector<Pose>& poses = seb->accessPoses();
	std::vector<double>& timeDiffs = seb->accessTimeDiffs();
	for(int i = 0; i < poses.size() - 1; i++) {
		problem.AddResidualBlock(cf_Velocity, nullptr,
						         poses.at(i).position.data(), &poses.at(i).theta,
		                         poses.at(i + 1).position.data(), &poses.at(i + 1).theta,
		                         &timeDiffs.at(i));
	}

}

void Optimizer::addConstraintAcceleration(ceres::Problem& problem) {
	if(config.optimization.weight_acceleration <= 0.0) {
		return;
	}

	std::vector<Pose>& poses = seb->accessPoses();
	std::vector<double>& timeDiffs = seb->accessTimeDiffs();
	
	// Add special acceleration constraint if initial velocity is fixed
	if(seb->getVelocityConstraintStart().isFixed) {
		problem.AddResidualBlock(cf_AccelerationStart, nullptr,
						         poses.at(0).position.data(), &poses.at(0).theta,
						         poses.at(1).position.data(), &poses.at(1).theta,
						         &timeDiffs.at(0));
	}

	// Add residual block for accelerations in the middle of the band
	for(int i = 0; i < poses.size() - 2; i++) {
		problem.AddResidualBlock(cf_Acceleration, nullptr,
						         poses.at(i).position.data(), &poses.at(i).theta,
		                         poses.at(i + 1).position.data(), &poses.at(i + 1).theta,
		                         poses.at(i + 2).position.data(), &poses.at(i + 2).theta,
		                         &timeDiffs.at(i), &timeDiffs.at(i + 1));
	}

	// Add special acceleration constraint if final velocity is fixed
	if(seb->getVelocityConstraintGoal().isFixed) {
		problem.AddResidualBlock(cf_AccelerationGoal, nullptr,
		                         poses.at(poses.size() - 2).position.data(), &poses.at(poses.size() - 2).theta,
		                         poses.at(poses.size() - 1).position.data(), &poses.at(poses.size() - 1).theta,
		                         &timeDiffs.at(timeDiffs.size() - 1));
	}
}

void Optimizer::addConstraintKinematics(ceres::Problem& problem) {
	bool differentialDrive = config.robot.minTurningRadius <= 0.0;
	
	if(differentialDrive) {
		if(config.optimization.weight_kinematicNonHolonomic <= 0.0 && config.optimization.weight_kinematicDriveDirection <= 0.0) {
			return;
		}	
	} else {
		if(config.optimization.weight_kinematicNonHolonomic <= 0.0 && config.optimization.weight_kinematicDriveDirection <= 0.0 && config.optimization.weight_kinematicTurningRadius <= 0.0) {
			return;
		}
	}
	
	std::vector<Pose>& poses = seb->accessPoses();
	for(int i = 0; i < poses.size() - 1; i++) {
		problem.AddResidualBlock(cf_Kinematics, nullptr,
						         poses.at(i).position.data(), &poses.at(i).theta,
		                         poses.at(i + 1).position.data(), &poses.at(i + 1).theta);
	}
}

void Optimizer::addConstraintReducedVelocity(ceres::Problem& problem, const std::vector<std::vector<DetectedObject*>>& relevantObjectsPerPose) {
	if(config.optimization.weight_reducedVelocity <= 0.0) {
		return;
	}

	std::vector<Pose>& poses = seb->accessPoses();
	std::vector<double>& timeDiffs = seb->accessTimeDiffs();
	
	// Iterate over pose pairs i, i+1
	for(int i = 0; i < poses.size() - 1; i++) {
		const std::vector<DetectedObject*>& relevantObjects = relevantObjectsPerPose.at(i); // We simplify and only use the objects relevant for pose i
		
		for(DetectedObject* relevantObject : relevantObjects) {
			switch(relevantObject->getObjectType()) {
				case DetectedObject::ObjectType::Person:
				{
					DetectedPerson* person = static_cast<DetectedPerson*>(relevantObject);
					// Add cost function
					problem.AddResidualBlock(person->cf_reducedVelocity, nullptr,
							                 poses.at(i).position.data(), poses.at(i + 1).position.data(), &timeDiffs.at(i));
					
					break;
				}
				case DetectedObject::ObjectType::AGV:
				{
					// Currently no cost function
					break;
				}
				case DetectedObject::ObjectType::Corridor:
				{
					// Currently no cost function					
					break;
				}					
			}
		}
	}
}

std::vector<std::vector<DetectedObject*>> Optimizer::computeRelevantObjectsPerPose() const {
	std::vector<std::vector<DetectedObject*>> relevantObjectsPerPose;

	const std::vector<Pose>& poses = seb->getPoses();

	relevantObjectsPerPose.resize(poses.size());

	for(int i = 0; i < poses.size(); i++) {
		const Pose& pose = poses.at(i);

		for(const std::unique_ptr<DetectedObject>& detectedObject : detectedObjects) {
			switch(detectedObject->getObjectType()) {
				// Persons
				case DetectedObject::ObjectType::Person:
				{
					DetectedPerson* person = dynamic_cast<DetectedPerson*>(detectedObject.get());

					// Check in cell-space -> multiply by resolution
					if(Pose::getLinearDistance(pose, person->poseCellSpace) * costmap->getResolution() <= person->upperDistanceBound * 1.5) {
						relevantObjectsPerPose.at(i).push_back(person);
					}
					break;
				}
				case DetectedObject::ObjectType::AGV:
				{
					// Currently no cost function associated => not relevant
					break;
				}
				case DetectedObject::ObjectType::Corridor:
				{
					// Currently no cost function associated => not relevant
					break;
				}					
			}
		}
	}

	return relevantObjectsPerPose;
}

void Optimizer::printSummary() {
	if(!config.isVerbosityAtLeastNormal()) {
		return;
	}
	
	for(const OptimizationStep& step : optimizationSummary.steps) {
		if(optimizationSummary.successful) {
			ROS_INFO("Step %d: %-14s in %4d ms, %3d iterations, %3d poses", step.step, step.result.c_str(), step.timeMs, step.numIterations, step.numPoses);
		} else {
			ROS_WARN("Step %d: %-14s in %4d ms, %3d iterations, %3d poses", step.step, step.result.c_str(), step.timeMs, step.numIterations, step.numPoses);
		}		
	}
}

}
