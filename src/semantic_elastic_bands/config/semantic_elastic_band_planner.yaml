base_global_planner: "semantic_elastic_bands/AStarGlobalPlannerROS"
base_local_planner: "semantic_elastic_bands/SebLocalPlannerROS"
  
ThetaStarGlobalPlannerROS:
  # Cost added to every cell to make the planner actually consider high-cost cells
  costOffset: 70
  # Cells with up to this cost (inclusive) are considered traversable
  maxTraversalCost: 160

AStarGlobalPlannerROS:
  # Cost added to every cell to make the planner actually consider high-cost cells
  costOffset: 70
  # Cells with up to this cost (inclusive) are considered traversable
  maxTraversalCost: 160
  
  # Enable post-smoothing
  smoothing: true

recovery_behavior_enabled: true
clearing_rotation_allowed: true

controller_frequency: 20.0
planner_frequency: 10.0

global_costmap:
  resolution: 0.05
  update_frequency: 5.0
  publish_frequency: 5.0
  always_send_full_costmap: true

  plugins:
    - { name: static_layer, type: 'costmap_2d::StaticLayer' }
    - { name: obstacle_layer, type: 'costmap_2d::ObstacleLayer' }
    - { name: inflation_layer, type: 'costmap_2d::InflationLayer' }
    - { name: semantic_layer, type: 'semantic_elastic_bands/Costmap/SemanticLayer' }
      
  semantic_layer:
    localPlanTopic: "/move_base/SebLocalPlannerROS/seb_plan"
    detectedObjectsTopic: "/seb_mock_object_publisher/detected_objects"
    trackedObjectsTopic: "/seb_mock_object_publisher/tracked_objects"
    
  obstacle_layer:
    track_unknown_space: false
    combination_method: 1 # 0 = overwrite, 1 = maximum
    clearing: true
    marking: true
    obstacle_range: 3.0
    raytrace_range: 3.5
    observation_sources: "scan"
    scan: 
      expected_update_rate: 0
      data_type: LaserScan
      obstacle_range: 20.0
      raytrace_range: 20.5
      topic: /scan
      marking: true
      clearing: true
  
  inflation_layer:
    cost_scaling_factor: 6.0 # exponential rate at which the obstacle cost drops off (lower increases cost)
    inflation_radius: 0.35
    
local_costmap:
  global_frame: "map"
  robot_base_frame: "base_link"
  rolling_window: true
  width: 8.0
  height: 8.0
  resolution: 0.025
  update_frequency: 10.0
  publish_frequency: 10.0

  plugins:
    - { name: static_layer, type: 'costmap_2d::StaticLayer' }
    - { name: obstacle_layer, type: 'costmap_2d::ObstacleLayer' }
    - { name: inflation_layer, type: 'costmap_2d::InflationLayer' }
    - { name: semantic_layer, type: 'semantic_elastic_bands/Costmap/SemanticLayer' }
      
  semantic_layer:
    localPlanTopic: "/move_base/SebLocalPlannerROS/seb_plan"
    detectedObjectsTopic: "/seb_mock_object_publisher/detected_objects"
    trackedObjectsTopic: "/seb_mock_object_publisher/tracked_objects"
      
  obstacle_layer:
    track_unknown_space: false
    combination_method: 1 # 0 = overwrite, 1 = maximum
    clearing: true
    marking: true
    obstacle_range: 3.0
    raytrace_range: 3.5
    observation_sources: "scan"
    scan: 
      expected_update_rate: 0
      data_type: LaserScan
      obstacle_range: 20.0
      raytrace_range: 20.5
      topic: /scan
      marking: true
      clearing: true

  inflation_layer:
    cost_scaling_factor: 6.0 # exponential rate at which the obstacle cost drops off (lower increases cost)
    inflation_radius: 0.35


################################### SEB ###################################
SebLocalPlannerROS:
  # "none", "normal" or "full"
  verbosityLevel: "normal"
  
  # Topics to listen to for detected or tracked objects
  detectedObjectsTopic: "/seb_mock_object_publisher/detected_objects"
  trackedObjectsTopic: "/seb_mock_object_publisher/tracked_objects"

  # Cells with up to this cost (inclusive) are considered traversable
  maxTraversalCost: 200
  
  # When initializing paths, estimate the average velocity to be this percentage of the maximum velocity 
  initializationMaxSpeedPercentage: 0.9
  
  # These settings depend on the agv base hardware and need to be adjusted for each robot   
  maxVelX: 0.5                # in m/s
  maxVelTheta: 0.8            # in rad/s
  maxAccX: 0.75               # in m/s²
  maxAccTheta: 5.0            # in rad/s²
  maxVelXBackwards: 0.4       # in m/s
  
  # If true, acceleration is limited to the maxAcc_ set above. Might help if the simulation cant handle very high velocity commands,
  # actual hardware drivers should always convert any commands into sane ranges
  limitAcceleration: true

  # For carlike robot => minTurningRadius must be > 0
  minTurningRadius: 0.0
  steeringAngleInsteadVelocity: false
  wheelbase: 0.5

  # How much of the global plan should be used as the local plan.
  # Distance and Duration limits supported, whichever comes first. 0 to disable
  globalPlanMaxLookaheadDistance: 3.0
  globalPlanMaxLookaheadDuration: 8.0

  # Maximum distance between the previous end of the global plan and the new goal for the global plan to be reused
  maxGlobalPlanReuseDistance: 0.3
  
  # Maximum linear and angular distance for the elastic band of the previous optimization to be reused
  maxBandReuseGoalDistance: 0.3
  maxBandReuseGoalAngleDeg: 120

  # Goal tolerance defines how close the robot must come to have reached the goal.
  goalToleranceXY: 0.1
  goalToleranceThetaDeg: 10.0
  
  # If fixGoalOrientation=false the robot will still try to rotate on spot after reaching the goal
  fixGoalOrientation: true

  # Temporal resolution of the timed elastic band. Higher may be more accurate but also costs more performance
  referenceTimeDifference: 0.3
  
  # Tolerance when determining if an optimized solution is viable.
  # Increase to accept more solutions if planner repeatedly fails to find a valid solution
  isSolutionUsableToleranceFactor: 6.5
  
  # Number of outer Optimization iterations
  numOptimizerIterations: 1
  
  # Use as many threads as possible but maybe leave some for other ros nodes
  ceresNumThreads: 6
  
  # Max solve time depends on the controller_frequency set above. 
  ceresMaxSolverTime: 0.05
  ceresMaxNumIterations: 200
  
  # Enables a more precise but more computational expensive way of calculating trajectory length
  useExactArcLength: false

  # Weight for different constraints, set to 0 to disable. These defaults should work for most use-cases
  weight_optimalTime: 3.5
  weight_shortestPath: 0.35
  weight_occupancyGrid: 0.05
  weight_velocity: 5.0
  weight_acceleration: 1.0
  weight_kinematicNonHolonomic: 1000.0
  weight_kinematicDriveDirection: 1.0
  weight_kinematicTurningRadius: 2.0
  weight_reducedVelocity: 1.0
  weight_rotationDirection: 10.0