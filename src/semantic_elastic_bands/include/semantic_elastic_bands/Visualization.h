#ifndef SEMANTIC_ELASTIC_BANDS_VISUALIZATION_H
#define SEMANTIC_ELASTIC_BANDS_VISUALIZATION_H

#include <ros/publisher.h>
#include "SemanticElasticBand.h"
#include <std_msgs/ColorRGBA.h>
#include <geometry_msgs/PoseStamped.h>

namespace semantic_elastic_bands {

/* This class controls all of the visualization messages published. These messages are meant to be visualized by rviz, however,
 * manually checking their output via the terminal might be useful as well.
 * 
 * After initialize(), the owing class may call several publishXXX() methods to publish the state of the semantic elastic band in
 * different visualization formats.
 * */
class Visualization
{
public:
	explicit Visualization();
	
	void initialize(ros::NodeHandle& nodeHandle, std::string globalFrame);
	
	// Visualization methods
	
	// Publish as list of poses
	void publishSemanticElasticBandPlan(const SemanticElasticBand& seb) const;
	
	// Publish with linear and rotational velocities between points
	void publishSemanticElasticBand(const SemanticElasticBand& seb) const;
	
	// Publish part of global plan used for optimization 
	void publishInitialPath(const std::vector<geometry_msgs::PoseStamped>& initialPath) const;
	
	// Publish elastic band right before optimization as list of poses
	void publishSemanticElasticBandPreOptimization(const SemanticElasticBand& seb) const;
	
	// Publish exact velocity and acceleration profile of path
	void publishTrajectoryData(const SemanticElasticBand& seb) const;
	
	// Publish empty messages to remove old path from visualization
	void publishEmptySeb() const;

protected:
	bool isInitialized;
	std::string localCostmapFrame;
	
	ros::Publisher sebPlanPublisher;
	ros::Publisher sebPublisher;
	ros::Publisher initialPlanPublisher;
	ros::Publisher preOptimizationPlanPublisher;
	ros::Publisher trajectoryDataPublisher;

private:
	static std_msgs::ColorRGBA speedToColor(double speed, double max, double alpha = 1.0);
	static std_msgs::ColorRGBA speedToColorBackwards(double speed, double max, double alpha = 1.0);
	
	
};

}

#endif //SEMANTIC_ELASTIC_BANDS_VISUALIZATION_H
