#ifndef SEMANTIC_ELASTIC_BANDS_SEBBICUBICINTERPOLATOR_H
#define SEMANTIC_ELASTIC_BANDS_SEBBICUBICINTERPOLATOR_H

// This Class is a slightly modified version of the BiCubicInterpolator from Ceres, see
// https://ceres-solver.googlesource.com/ceres-solver/+/1.12.0/include/ceres/cubic_interpolation.h

// Ceres Solver - A fast non-linear least squares minimizer
// Copyright 2015 Google Inc. All rights reserved.
// http://ceres-solver.org/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: sameeragarwal@google.com (Sameer Agarwal)

/* We modified this code to improve its speed by ignoring cases which do not apply to our use-case 
 * */

#include <ceres/cubic_interpolation.h>

using ceres::CubicHermiteSpline;

namespace semantic_elastic_bands {

double CubicHermiteSpline(const double p0,
                          const double p1,
                          const double p2,
                          const double p3,
                          const double x) {
	const double a = 0.5 * (-p0 + 3.0 * p1 - 3.0 * p2 + p3);
	const double b = 0.5 * (2.0 * p0 - 5.0 * p1 + 4.0 * p2 - p3);
	const double c = 0.5 * (-p0 + p2);
	const double d = p1;

	// Use Horner's rule to evaluate the function value and its
	// derivative.

	// f = ax^3 + bx^2 + cx + d
	return d + x * (c + x * (b + x * a));
}

template<typename Grid>
class BiCubicInterpolator
{
public:
	explicit BiCubicInterpolator(const Grid& grid) :
			grid(grid) {
	}
	
	// Evaluate the interpolated function value and/or its
	// derivative. Uses the nearest point on the grid boundary if r or
	// c is out of bounds.
	void Evaluate(double r, double c, double* f, double* dfdr, double* dfdc) const {
		// BiCubic interpolation requires 16 values around the point being
		// evaluated.  We will use pij, to indicate the elements of the
		// 4x4 grid of values.
		//
		//          col
		//      p00 p01 p02 p03
		// row  p10 p11 p12 p13
		//      p20 p21 p22 p23
		//      p30 p31 p32 p33
		//
		// The point (r,c) being evaluated is assumed to lie in the square
		// defined by p11, p12, p22 and p21.

		const int row = std::floor(r);
		const int col = std::floor(c);

		Eigen::Matrix<double, 1, 1> p0, p1, p2, p3;

		// Interpolate along each of the four rows, evaluating the function
		// value and the horizontal derivative in each row.
		Eigen::Matrix<double, 1, 1> f0, f1, f2, f3;
		Eigen::Matrix<double, 1, 1> df0dc, df1dc, df2dc, df3dc;

		grid.GetValue(row - 1, col - 1, p0.data());
		grid.GetValue(row - 1, col    , p1.data());
		grid.GetValue(row - 1, col + 1, p2.data());
		grid.GetValue(row - 1, col + 2, p3.data());
		ceres::CubicHermiteSpline<1>(p0, p1, p2, p3, c - col, f0.data(), df0dc.data());

		grid.GetValue(row, col - 1, p0.data());
		grid.GetValue(row, col    , p1.data());
		grid.GetValue(row, col + 1, p2.data());
		grid.GetValue(row, col + 2, p3.data());
		ceres::CubicHermiteSpline<1>(p0, p1, p2, p3, c - col, f1.data(), df1dc.data());

		grid.GetValue(row + 1, col - 1, p0.data());
		grid.GetValue(row + 1, col    , p1.data());
		grid.GetValue(row + 1, col + 1, p2.data());
		grid.GetValue(row + 1, col + 2, p3.data());
		ceres::CubicHermiteSpline<1>(p0, p1, p2, p3, c - col, f2.data(), df2dc.data());

		grid.GetValue(row + 2, col - 1, p0.data());
		grid.GetValue(row + 2, col    , p1.data());
		grid.GetValue(row + 2, col + 1, p2.data());
		grid.GetValue(row + 2, col + 2, p3.data());
		ceres::CubicHermiteSpline<1>(p0, p1, p2, p3, c - col, f3.data(), df3dc.data());

		// Interpolate vertically the interpolated value from each row and
		// compute the derivative along the columns.
		ceres::CubicHermiteSpline<1>(f0, f1, f2, f3, r - row, f, dfdr);
		if(dfdc != nullptr) {
			// Interpolate vertically the derivative along the columns.
			ceres::CubicHermiteSpline<1>(df0dc, df1dc, df2dc, df3dc, r - row, dfdc, nullptr);
		}
	}

	// Custom Evaluate function which only interpolates the value and skips any derivatives.
	// This is only used if the Occupancy Grid cost function is computed numerically
	void Evaluate(double r, double c, double* f) const {
		const int row = std::floor(r);
		const int col = std::floor(c);

		double p0, p1, p2, p3;
		double f0, f1, f2, f3;

		grid.GetValue(row - 1, col - 1, &p0);
		grid.GetValue(row - 1, col    , &p1);
		grid.GetValue(row - 1, col + 1, &p2);
		grid.GetValue(row - 1, col + 2, &p3);
		f0 = semantic_elastic_bands::CubicHermiteSpline(p0, p1, p2, p3, c - col);

		grid.GetValue(row, col - 1, &p0);
		grid.GetValue(row, col    , &p1);
		grid.GetValue(row, col + 1, &p2);
		grid.GetValue(row, col + 2, &p3);
		f1 = semantic_elastic_bands::CubicHermiteSpline(p0, p1, p2, p3, c - col);

		grid.GetValue(row + 1, col - 1, &p0);
		grid.GetValue(row + 1, col    , &p1);
		grid.GetValue(row + 1, col + 1, &p2);
		grid.GetValue(row + 1, col + 2, &p3);
		f2 = semantic_elastic_bands::CubicHermiteSpline(p0, p1, p2, p3, c - col);

		grid.GetValue(row + 2, col - 1, &p0);
		grid.GetValue(row + 2, col    , &p1);
		grid.GetValue(row + 2, col + 1, &p2);
		grid.GetValue(row + 2, col + 2, &p3);
		f3 = semantic_elastic_bands::CubicHermiteSpline(p0, p1, p2, p3, c - col);

		// Interpolate vertically the interpolated value from each row and
		// compute the derivative along the columns.
		*f = semantic_elastic_bands::CubicHermiteSpline(f0, f1, f2, f3, r - row);
	}

	template<typename JetT>
	void Evaluate(const JetT& r, const JetT& c, JetT* f) const {
		double frc;
		double dfdr;
		double dfdc;
		Evaluate(r.a, c.a, &frc, &dfdr, &dfdc);
		f->a = frc;
		f->v = dfdr * r.v + dfdc * c.v;
	}

private:
	const Grid& grid;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_SEBBICUBICINTERPOLATOR_H
