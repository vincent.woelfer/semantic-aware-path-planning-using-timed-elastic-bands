#ifndef SEMANTIC_ELASTIC_BANDS_DETECTEDCORRIDOR_H
#define SEMANTIC_ELASTIC_BANDS_DETECTEDCORRIDOR_H

#include "DetectedObject.h"
#include <geometry_msgs/PoseStamped.h>
#include <ceres/cost_function.h>
#include "semantic_elastic_bands/Pose.h"

namespace semantic_elastic_bands {

class DetectedCorridor : public DetectedObject {
public:
	explicit DetectedCorridor(geometry_msgs::PoseStamped poseStart, geometry_msgs::PoseStamped poseEnd, double width);

	void convertToCellSpace(costmap_2d::Costmap2D* costmap) override;
	void updateCostmapBounds(double* min_x, double* min_y, double* max_x, double* max_y) const override;
	void getCostmapUpdateArea(double costmapResolution, int& minX, int& minY, int& maxX, int& maxY) const override;
	bool skipBasedOnRobotPose(const Pose& robotPose) override;
	unsigned char getCellCost(double costmapResolution, const Point& p) const override;

	// Data - Meter-Scale, populated by constructor
	const geometry_msgs::PoseStamped poseStart;
	const geometry_msgs::PoseStamped poseEnd;
	const double width;
		
	// Data cell-space - must be calculated through convertToCellSpace
	Point pointStartCellSpace;
	Point pointEndCellSpace;

private:
	const double maxCost = 130;
	const bool driveOnRightSide = true;
	
	bool robotColinearToCorridor;
	
	void computeBounds();	
	double minXMeterSpace;
	double minYMeterSpace;
	double maxXMeterSpace;
	double maxYMeterSpace;
	
	// Updated in convertToCellSpace
	int minXCellSpace;
	int minYCellSpace;
	int maxXCellSpace;
	int maxYCellSpace;
	
	// Data for faster calculation - in cell space
	double corridorVectorX;
	double corridorVectorY;
	double corridorVectorLengthSquared;
	double halfWidthCellSpace;
	double optimalTrackOffsetCellSpace;
	double tToDistanceFactor;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_DETECTEDPERSON_H
