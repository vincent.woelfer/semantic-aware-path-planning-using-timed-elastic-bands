#ifndef SEMANTIC_ELASTIC_BANDS_DETECTEDOBJECTPARSER_H
#define SEMANTIC_ELASTIC_BANDS_DETECTEDOBJECTPARSER_H

#include <ros/ros.h>
#include <mutex>
#include <visualization_msgs/MarkerArray.h>
#include <tf2_ros/buffer.h>

#include "DetectedObject.h"

#define CLASS_HUMAN 0
#define CLASS_AGV 1

#include <semantic_elastic_bands/DetectedObjectMsg.h>
#include <semantic_elastic_bands/DetectedObjectsMsg.h>
#include <semantic_elastic_bands/TrackedObjectMsg.h>
#include <semantic_elastic_bands/TrackedObjectsMsg.h>

namespace semantic_elastic_bands {

/* This class implements a parser which parses detected and tracked object messages. This includes keeping track of all currently known objects and 
 * matching their detection and tracking IDs. This allows us to combine their information into one object.
 */
class DetectedObjectParser {
public:
	explicit DetectedObjectParser(const std::string& detectedTopicName, const std::string& trackedTopicName);
	~DetectedObjectParser();

	std::vector<std::unique_ptr<DetectedObject>> getTransformedObjects(tf2_ros::Buffer* tf, const std::string& targetFrame, const ros::Duration& timeout = ros::Duration(0.1));

private:
	ros::Subscriber subscriberDetectedObjects;
	ros::Subscriber subscriberTrackedObjects;
	std::mutex mutex;

	semantic_elastic_bands::DetectedObjectsMsg latestDetectedObjectsMsg;
	semantic_elastic_bands::TrackedObjectsMsg latestTrackedObjectsMsg;	
	
	std::vector<DetectedObject*> detectedObjects;

	void detectedObjectCallback(const semantic_elastic_bands::DetectedObjectsMsg& msg);
	void trackedObjectCallback(const semantic_elastic_bands::TrackedObjectsMsg& msg);
	
	void matchDetectionsWithTrackings();
	
	
	void createMockCorridors();
};


}

#endif //SEMANTIC_ELASTIC_BANDS_DETECTEDOBJECTPARSER_H
