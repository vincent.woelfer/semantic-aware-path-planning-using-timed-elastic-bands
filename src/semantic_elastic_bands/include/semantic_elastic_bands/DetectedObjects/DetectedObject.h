#ifndef SEMANTIC_ELASTIC_BANDS_DETECTEDOBJECT_H
#define SEMANTIC_ELASTIC_BANDS_DETECTEDOBJECT_H

#include <costmap_2d/costmap_2d.h>
#include "semantic_elastic_bands/Pose.h"
#include "semantic_elastic_bands/Point.h"

namespace semantic_elastic_bands {

/* This class represents the abstract interface any detected object must expose. This includes the ObjectType (currently only Person, AGV and Corridor are supported),
 * The detection and tracking id as well as several functions to obtain data about this object.
 * */
class DetectedObject {
public:
	enum class ObjectType {Person, AGV, Corridor};
		
	virtual ~DetectedObject() = default;

	ObjectType getObjectType() const {
		return objectType;
	}

	unsigned long getDetectionId() const {
		return detectionId;
	}

	unsigned long getTrackingId() const {
		return trackingId;
	}
	
	// Convert to cell space internally, does not check any costmap bounds
	virtual void convertToCellSpace(costmap_2d::Costmap2D* costmap) = 0;

	// Enlarge area by own updated area -> OR areas
	virtual void updateCostmapBounds(double* min_x, double* min_y, double* max_x, double* max_y) const = 0;

	// Shrink area to get overlap of given and own updated area -> AND areas
	virtual void getCostmapUpdateArea(double costmapResolution, int& minX, int& minY, int& maxX, int& maxY) const = 0;
	
	// Update internal state based of robot pose. Decide if this object can be skipped entirely
	// True = skip this object because it is irrelevant given this robot pose
	virtual bool skipBasedOnRobotPose(const Pose& robotPose) = 0;

	// Get cell cost incurred through this object at x/y in cell space
	virtual unsigned char getCellCost(double costmapResolution, const Point& p) const = 0;
	
protected:
	explicit DetectedObject(ObjectType objectType, unsigned long detectionId, unsigned long trackingId) :
			objectType(objectType),
			detectionId(detectionId),
			trackingId(trackingId)
	{}
	
	const ObjectType objectType;
	const unsigned long detectionId;
	const unsigned long trackingId;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_DETECTEDOBJECT_H
