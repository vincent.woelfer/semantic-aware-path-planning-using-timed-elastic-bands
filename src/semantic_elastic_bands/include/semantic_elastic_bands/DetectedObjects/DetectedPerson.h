#ifndef SEMANTIC_ELASTIC_BANDS_DETECTEDPERSON_H
#define SEMANTIC_ELASTIC_BANDS_DETECTEDPERSON_H

#include "DetectedObject.h"
#include <geometry_msgs/PoseStamped.h>
#include <ceres/cost_function.h>
#include "semantic_elastic_bands/Pose.h"

namespace semantic_elastic_bands {

class DetectedPerson : public DetectedObject {
public:
	explicit DetectedPerson(unsigned long detectionId, unsigned long trackingId, geometry_msgs::PoseStamped pose, double extensionLength);	
	~DetectedPerson() override;
	
	void convertToCellSpace(costmap_2d::Costmap2D* costmap) override;
	void updateCostmapBounds(double* min_x, double* min_y, double* max_x, double* max_y) const override;
	void getCostmapUpdateArea(double costmapResolution, int& minX, int& minY, int& maxX, int& maxY) const override;
	bool skipBasedOnRobotPose(const Pose& robotPose) override;
	unsigned char getCellCost(double costmapResolution, const Point& p) const override;

	// Data - Meter-Scale, populated by constructor
	geometry_msgs::PoseStamped pose;
	double extensionLength;
	geometry_msgs::Point extensionPoint;

	// Data cell-space - must be calculated through convertToCellSpace
	Pose poseCellSpace; // For external use by e.g. ceres
	Point pointCellSpace; // internal only
	Point extensionPointCellSpace; // internal only

	// Cost functions - created by the optimizer,
	ceres::CostFunction* cf_reducedVelocity;
	
	// Only used for velocity reduction.
	const double lowerDistanceBound = 1.0;
	const double upperDistanceBound = 1.75;
	const double velocityReduction = 0.6;

	// Internal: Used for costmap cost
	const double maxCostCenter = 220;
	const double maxCostExtension = 100;
	double maxCostRadius = 2.0;
	double maxCostRadiusCellSpace;
	
	double lethalRadius = 0.15;
	double lethalRadiusCellspace;
	
};

}

#endif //SEMANTIC_ELASTIC_BANDS_DETECTEDPERSON_H
