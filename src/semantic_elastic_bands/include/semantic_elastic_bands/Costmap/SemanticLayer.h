#ifndef SEMANTIC_ELASTIC_BANDS_SEMANTICLAYER_H
#define SEMANTIC_ELASTIC_BANDS_SEMANTICLAYER_H

#include <ros/ros.h>
#include <costmap_2d/layer.h>
#include <costmap_2d/layered_costmap.h>
#include <costmap_2d/GenericPluginConfig.h>
#include <visualization_msgs/MarkerArray.h>
#include <nav_msgs/Path.h>
#include <mutex>
#include <geometry_msgs/PointStamped.h>
#include "semantic_elastic_bands/Pose.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedObject.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedObjectParser.h"

using geometry_msgs::Point;

namespace semantic_elastic_bands {

/* This class implements the costmap_2d::Layer interface and provides an additional costmap layer. This layer listens to any detected objects
 * and adds their induced cost to this costmap layer.
 * */
class SemanticLayer : public costmap_2d::Layer
{
public:
	SemanticLayer();
	~SemanticLayer() override;

	void onInitialize() override;

	void updateBounds(double robot_x, double robot_y, double robot_yaw, double* min_x, double* min_y, double* max_x, double* max_y) override;

	void updateCosts(costmap_2d::Costmap2D& master_grid, int min_i, int min_j, int max_i, int max_j) override;

	void localPlanCallback(const nav_msgs::Path& msg); 

private:
	std::mutex mutex;
	DetectedObjectParser* parser;
	std::vector<std::unique_ptr<DetectedObject>> detectedObjects;

	bool isPoseRobotValid;
	semantic_elastic_bands::Pose robotPoseCellSpace;
	
	ros::Subscriber localPlanSubscriber;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_SEMANTICLAYER_H
