#ifndef SEMANTIC_ELASTIC_BANDS_THETA_STAR_GLOBALPLANNER_ROS_H
#define SEMANTIC_ELASTIC_BANDS_THETA_STAR_GLOBALPLANNER_ROS_H

#include <ros/ros.h>

// base local planner base class and utilities
#include <nav_core/base_global_planner.h>

// message types
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

// transforms
#include <tf2/utils.h>
#include <tf2_ros/buffer.h>

// costmap
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/costmap_2d_ros.h>

#include "semantic_elastic_bands/GlobalPathPlanners/ThetaStarPathPlanner.h"

namespace semantic_elastic_bands {

/* Interface for the Theta-Star planner as a nav_core::BaseGlobalPlanner plugin
 */
class ThetaStarGlobalPlannerROS : public nav_core::BaseGlobalPlanner {
public:
	ThetaStarGlobalPlannerROS();
	ThetaStarGlobalPlannerROS(std::string name, costmap_2d::Costmap2DROS* costmap_ros);
	
	~ThetaStarGlobalPlannerROS() override;

	void initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros) override;
	
	bool makePlan(const geometry_msgs::PoseStamped& start, const geometry_msgs::PoseStamped& goal, std::vector<geometry_msgs::PoseStamped>& plan) override;

private:
	bool isInitialized;
	
	ThetaStarPathPlanner* planner{};
	ros::Publisher publisher;

	std::string frameId;
	void publishPlan(const std::vector<geometry_msgs::PoseStamped>& path) const;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_THETA_STAR_GLOBALPLANNER_ROS_H
