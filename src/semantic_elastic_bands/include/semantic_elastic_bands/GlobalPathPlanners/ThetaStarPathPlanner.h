#ifndef SEMANTIC_ELASTIC_BANDS_THETASTARPPATHLANNER_H
#define SEMANTIC_ELASTIC_BANDS_THETASTARPPATHLANNER_H

#include "BasePathPlanner.h"
#include "semantic_elastic_bands/LineOfSightCalculations/CohenSutherlandClipping.h"

#include <vector>
#include <queue>
#include <unordered_map>
#include <thread>

namespace semantic_elastic_bands {

class ThetaStarPathPlanner : public BasePathPlanner
{
public:
	explicit ThetaStarPathPlanner(costmap_2d::Costmap2D* map, std::string globalFrameId, int costOffset, int maxTraversalCost);
	~ThetaStarPathPlanner() override = default;

	void reconfigure(int costOffset, int maxTraversalCost) override;

protected:
	std::vector<Point> computePath(Point start, Point goal) override;

private:
	struct Node
	{
		const Point pos;
		Node* prev;
		float gScore;     // Best known path from start to this Node
		float fScore;     // f = g + heuristic

		static constexpr float maxScore = std::numeric_limits<float>::max();

		explicit Node(Point pos) :
				pos(pos),
				prev(nullptr),
				gScore(maxScore),
				fScore(maxScore) {}
	};

	struct CompareNodeFScore
	{
		inline bool operator()(const Node* a, const Node* b) const {
			return (a->fScore > b->fScore) || (a->fScore == b->fScore && a->gScore < b->gScore);
		}
	};
	
	typedef std::unordered_map<Point, Node, PointHashFunction> NodeMap;
	typedef std::priority_queue<Node*, std::vector<Node*>, CompareNodeFScore> NodePriorityQueue;

	static inline float heuristic(const Point& p, const Point& goalPoint);
	static float lineOfSight(const Point& a, const Point& b, const costmap_2d::Costmap2D* map);

	static int estimateExploredSetSize(const Point& a, const Point& b) ;
	static int estimateOpenSetSize(const Point& a, const Point& b) ;
	static int estimatePathSize(const Point& a, const Point& b) ;
		
	// Thread Pool
	struct ThreadInformation {
		const costmap_2d::Costmap2D* map;
		Point goalPoint;
		
		NodeMap* exploredSet;
		std::atomic_flag exploredSetFlag;
		
		NodePriorityQueue* openSet;		 
		std::atomic_flag openSetFlag;
		
		// Current Node
		Node* currentNode;
		std::atomic<unsigned long> currentNodeId;
		
		// Wait for threads to finish 
		std::atomic<int> finishedThreads;

		std::atomic<bool> isRunning;
		
		explicit ThreadInformation(const costmap_2d::Costmap2D* map) :
			map(map)
		{}
	};
	
	static constexpr std::memory_order memoryOrder = std::memory_order::memory_order_relaxed;
	static constexpr int numThreads = 4; // Must be 4!
	ThreadInformation threadInformation;
	std::thread threadPool[numThreads];
	
	void initThreadPool(NodeMap* exploredSet, NodePriorityQueue* openSet, Point goalPoint);
	
	void initThreadPoolTermination();
	void finishThreadPoolTermination();
	
	static void threadPoolFunction(int index, ThreadInformation* threadInformation);

	static float costOffsetInternal; 
	static float maxTraversalCostInternal;
		
};

}

#endif //SEMANTIC_ELASTIC_BANDS_THETASTARPATHPLANNER_H
