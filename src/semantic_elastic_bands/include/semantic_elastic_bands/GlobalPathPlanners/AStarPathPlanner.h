#ifndef SEMANTIC_ELASTIC_BANDS_ASTARPPATHLANNER_H
#define SEMANTIC_ELASTIC_BANDS_ASTARPPATHLANNER_H

#include "BasePathPlanner.h"

#include <vector>
#include <queue>
#include <unordered_map>

namespace semantic_elastic_bands {

/* Implementation of the well-known A-Star pathfinding algorithm. This implementation is single-threaded and explores the four adjacent cells during the expansion step.
 * To improve speed, integer-only math is used exclusively, cardinal and diagonal moves incur different costs. 
 * */
class AStarPathPlanner : public BasePathPlanner
{
public:
	explicit AStarPathPlanner(costmap_2d::Costmap2D* map, std::string globalFrameId, int costOffset, int maxTraversalCost, bool smoothing);
	~AStarPathPlanner() override = default;

	void reconfigure(int costOffset, int maxTraversalCost) override;
	
protected:
	std::vector<Point> computePath(Point start, Point goal) override;

private:
	// Inspired by https://github.com/riscy/a_star_on_grids#move-costs 
	static constexpr int C = 7;                 // Cardinal Move Cost
	static constexpr int D = 10;                // Diagonal Move Cost
	static constexpr int E = 2 * C - D;         // Two Cardinal - Diagonal Move Cost
	static constexpr int W = 1;                 // Weight for heuristic
	
	bool smoothing;
	
	struct Node
	{
		const Point pos;
		Node* prev;
		int gScore;     // Best known path from start to this Node
		int fScore;     // f = g + heuristic
		bool expanded;

		static constexpr int maxScore = std::numeric_limits<int>::max();

		explicit Node(Point pos) :
				pos(pos),
				prev(nullptr),
				gScore(maxScore),
				fScore(maxScore),
				expanded(false) {}
	};

	struct CompareNodeFScore
	{
		inline bool operator()(const Node* a, const Node* b) const {
			return (a->fScore > b->fScore) || (a->fScore == b->fScore && a->gScore < b->gScore);
		}
	};

	struct Neighbour
	{
		Point point;
		int cost;

		Neighbour(int x, int y, int cost) :
				point(x, y),
				cost(cost) {}

		Neighbour() = default;
	};

	typedef std::unordered_map<Point, Node, PointHashFunction> NodeMap;
	typedef std::priority_queue<Node*, std::vector<Node*>, CompareNodeFScore> NodePriorityQueue;

	Point goalPoint;
	[[nodiscard]] inline int heuristic(Point p) const;

	Neighbour neighbourList[8];
	Neighbour* lastNeighbour;
	inline void generateNeighbours(const Point& pos);

	std::vector<Point> smoothPath(const std::vector<Point>& originalPath);

	static int estimateExploredSetSize(const Point& a, const Point& b) ;
	static int estimateOpenSetSize(const Point& a, const Point& b) ;
	static int estimatePathSize(const Point& a, const Point& b) ;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_ASTARPATHPLANNER_H
