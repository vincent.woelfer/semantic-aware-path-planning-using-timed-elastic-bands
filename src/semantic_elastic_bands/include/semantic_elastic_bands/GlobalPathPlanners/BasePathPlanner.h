#ifndef SEMANTIC_ELASTIC_BANDS_BASE_PATHPLANNER_H
#define SEMANTIC_ELASTIC_BANDS_BASE_PATHPLANNER_H

#include <costmap_2d/costmap_2d.h>
#include <geometry_msgs/PoseStamped.h>
#include <Eigen/Eigen>
#include <mutex>
#include <ros/publisher.h>

namespace semantic_elastic_bands {

class BasePathPlanner		
{
public:
	/* Public Interface */
	explicit BasePathPlanner(costmap_2d::Costmap2D* map, std::string globalFrameId, int costOffset, int maxTraversalCost);
	virtual ~BasePathPlanner() = default;

	std::vector<geometry_msgs::PoseStamped> findPath(const geometry_msgs::PoseStamped& start, const geometry_msgs::PoseStamped& goal);

	virtual void reconfigure(int costOffset, int maxTraversalCost) = 0;
	
protected:	
	/* Definitions */
	struct Point
	{
		int x;
		int y;

		Point(int x, int y) :
				x(x),
				y(y) {}

		Point() = default;

		inline bool operator==(const Point& other) const {
			return this->x == other.x && this->y == other.y;
		}

		inline bool operator!=(const Point& other) const {
			return !(*this == other);
		}
	};

	struct PointHashFunction
	{
		std::size_t operator()(const Point& point) const {
			return static_cast<unsigned int>(point.x) ^ (static_cast<unsigned int>(point.y) << 16u);
		}
	};

	/* Functions */
	// Empty -> no path found
	// If a path is found the start and goal pose is guaranteed to be contained (even if start = goal)
	virtual std::vector<Point> computePath(Point start, Point goal) = 0;	
	
	/* Class members */
	const costmap_2d::Costmap2D* map;
	const std::string globalFrameId;
	std::mutex mutex;

	int costOffset;       // Increase everything by this cost to avoid the planner staying in low cost regions completely 
	int maxTraversalCost; // Cells higher than this are impassable	

private:
	bool convertWorldToCostmap(double worldX, double worldY, Point& mapPoint) const;

	ros::Publisher planningTimeGlobalPublisher;
	
};

}

#endif //SEMANTIC_ELASTIC_BANDS_BASE_PATHPLANNER_H
