#ifndef SEMANTIC_ELASTIC_BANDS_PLANNER_H
#define SEMANTIC_ELASTIC_BANDS_PLANNER_H

#include <ros/ros.h>
#include "Visualization.h"
#include "SemanticElasticBand.h"
#include "Pose.h"
#include <nav_msgs/OccupancyGrid.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <costmap_2d/costmap_2d.h>
#include "DetectedObjects/DetectedObject.h"
#include "Optimizer.h"
#include <geometry_msgs/Twist.h>

namespace semantic_elastic_bands {

/* This class contains all the planning logic. Given an initial global path, control commands can be computed via computeCmdVel().
 * Note that this class is NOT the highest-level instance for the actual local planner seen by ROS, you can find it in SebLocalPlannerROS
 * Furthermore, this class handles the visualization of the semantic elastic band via its own instance of the visualization class.
 * 
 * The main public function of this class is the plan() method which roughly does the following:
 * 1) Transform the initial global path into the cell space of the local costmap
 * 2) Check if the optimization result of the previous iteration exists and can be reused
 *      2.1) If not -> reinitialize the semantic elastic band
 *      2.2) If yes -> update start and goal of semantic elastic band
 * 3) Specify the velocity constraints for the start/end of the elastic band
 * 4) Construct a new instance of the optimizer and call optimize()
 * 5) Return and log result of optimization and visualize optimized semantic elastic band
 * */
class Planner
{
public:
	enum class PlanningResult {SUCCESS, FAILURE, USABLE_ONCE};
	
	Planner(ros::NodeHandle& nodeHandle, const SebConfig& SebConfig, costmap_2d::Costmap2D* costmap, const std::string& localCostmapFrame);
	~Planner();
	
	PlanningResult plan(const std::vector<geometry_msgs::PoseStamped>& initialPath, geometry_msgs::Twist startVelocity, bool isFinalGoal, bool forceReinitialization);
	
	void computeCmdVel(double& vX, double& vTheta);
	
	void updateDetectedObjects(std::vector<std::unique_ptr<DetectedObject>> newDetectedObjects);
	
	double prevPlanningTime;
			
private:
	// Transforms the initial plan to cell space
	bool transformInitialPathToCellspace(const std::vector<geometry_msgs::PoseStamped>& initialPath);
	
	bool canBandBeReused();
	
	void visualizePostOptimization(const std::vector<geometry_msgs::PoseStamped>& initialPath);
	void visualizePreOptimization();
		
	// Owned
	std::unique_ptr<SemanticElasticBand> seb;
	Visualization visualisation;
	std::vector<std::unique_ptr<DetectedObject>> detectedObjects;

	std::vector<Pose> initialPathTransformed;
	
	// Used	
	const SebConfig& config;
	costmap_2d::Costmap2D* costmap;
	
	// Ros interfacing
	const std::string localCostmapFrame;
	

};
}

#endif //SEMANTIC_ELASTIC_BANDS_PLANNER_H
