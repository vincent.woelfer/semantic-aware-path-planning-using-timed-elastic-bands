#ifndef SEMANTIC_ELASTIC_BANDS_OPTIMIZER_H
#define SEMANTIC_ELASTIC_BANDS_OPTIMIZER_H

#include "DetectedObjects/DetectedObject.h"
#include "SebConfig.h"
#include "SemanticElasticBand.h"

#include <ceres/ceres.h>
#include <utility>
#include <vector>
#include <costmap_2d/costmap_2d.h>

namespace semantic_elastic_bands {

/* This class optimizes the semantic elastic band according to the specified cost functions. This optimizer is constructed anew for every new optimization process and is meant to be only used once.
 * The only public function optimize() (apart from logging) roughly works as follows:
 * 1) Construct cost functions according to the weights specified in the config and nearby detected objects (specified by the parameter detectedObjects)
 * 2) Optimize n outer iterations:
 *      2.1) Verify that band is in a valid state
 *      2.2) Create Ceres Problem, assign cost functions via the addConstraint functions
 *      2.3) Let Ceres optimize
 *      2.4) Verify band correctness and optimization result afterwards
 * 3) Delete cost functions
 * 
 * Note that this class owns the cost function it creates. However, semantic cost functions are stored inside the detected objects and are therefore owned (and destroyed) by these objects.
 * */
class Optimizer
{
public:
	explicit Optimizer(SemanticElasticBand* seb, const SebConfig& config, costmap_2d::Costmap2D* costmap, const std::vector<std::unique_ptr<DetectedObject>>& detectedObjects);
	
	bool optimize();

	void printSummary();

private:
	// Not owned
	SemanticElasticBand* seb;
	const SebConfig& config;
	costmap_2d::Costmap2D* costmap;
	const std::vector<std::unique_ptr<DetectedObject>>& detectedObjects;
	
	// Options
	void setOptions();
	ceres::Problem::Options problemOptions;
	ceres::Solver::Options solverOptions;
	
	// Verification of band before/after optimization 
	bool isBandUsableForOptimization() const;
	bool isSolutionUsable(const ceres::Solver::Summary& solverSummary) const;

	// Cost functions
	void createCostFunctions();
	void deleteCostFunctions();
	ceres::CostFunction* cf_TimeOptimal = nullptr;
	ceres::CostFunction* cf_ShortestPath = nullptr;
	ceres::CostFunction* cf_OccupancyGrid = nullptr;
	ceres::CostFunction* cf_Velocity = nullptr;
	ceres::CostFunction* cf_Acceleration = nullptr;
	ceres::CostFunction* cf_AccelerationStart = nullptr;
	ceres::CostFunction* cf_AccelerationGoal = nullptr;
	ceres::CostFunction* cf_Kinematics = nullptr;

	std::vector<std::vector<DetectedObject*>> computeRelevantObjectsPerPose() const;

	// Add all relevant constraints
	void constructOptimizationProblem(ceres::Problem& problem, const std::vector<std::vector<DetectedObject*>>& relevantObjectsPerPose);
	
	// "Default" constraints
	void addConstraintTimeOptimal(ceres::Problem& problem);
	void addConstraintShortestPath(ceres::Problem& problem);
	void addConstraintOccupancyGrid(ceres::Problem& problem);
	void addConstraintVelocity(ceres::Problem& problem);
	void addConstraintAcceleration(ceres::Problem& problem);
	void addConstraintKinematics(ceres::Problem& problem);
	
	// Semantic constraints
	void addConstraintReducedVelocity(ceres::Problem& problem, const std::vector<std::vector<DetectedObject*>>& relevantObjectsPerPose);
	
	// Optimization Summary
	struct OptimizationStep {
		int step;
		int timeMs;
		int numIterations;
		int numPoses;
		bool successful;
		std::string result;
		
		OptimizationStep(int step, int timeMs, int numIterations, int numPoses, bool successful, std::string result) :
			step(step),
			timeMs(timeMs),
			numIterations(numIterations),
			numPoses(numPoses),
			successful(successful),
			result(std::move(result))
			{ }
	};
	
	struct OptimizationSummary {
		std::vector<OptimizationStep> steps;
		bool successful = true;
		
		void addStep(const OptimizationStep& step) {
			steps.push_back(step);
			successful = successful && step.successful;
		}
	};
	
	OptimizationSummary optimizationSummary;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_OPTIMIZER_H
