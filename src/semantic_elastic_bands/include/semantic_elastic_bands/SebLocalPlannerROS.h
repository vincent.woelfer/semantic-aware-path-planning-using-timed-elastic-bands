#ifndef SEMANTIC_ELASTIC_BANDS_LOCAL_PLANNER_ROS_H
#define SEMANTIC_ELASTIC_BANDS_LOCAL_PLANNER_ROS_H

#include <ros/ros.h>

// base local planner base class and utilities
#include <nav_core/base_local_planner.h>
#include <base_local_planner/goal_functions.h>
#include <base_local_planner/odometry_helper_ros.h>
#include <base_local_planner/costmap_model.h>

// message types
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

// transforms
#include <tf2/utils.h>
#include <tf2_ros/buffer.h>

// costmap
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/costmap_2d_ros.h>

// Dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <semantic_elastic_bands/SebLocalPlannerReconfigureConfig.h>
#include "semantic_elastic_bands/DetectedObjects/DetectedObjectParser.h"

// Semantic Elastic Bands Imports
#include "Visualization.h"
#include "Planner.h"

namespace semantic_elastic_bands {

using geometry_msgs::PoseStamped;
using geometry_msgs::Twist;

/* This class implements the nav_core::BaseLocalPlanner interface and is the highest-level planner class which is directly called by 
 * the ros - move_base package. A normal lifecycle looks like this:
 * 1) This planner is initialized at startup
 * 2) A new global plan is assigned via setPlan(). The planner now tries to follow that global plan
 * 3) Until the goal is reached, move_base continuously calls computeVelocityCommands() and asks for new control commands
 * 4) Until the goal is reached, isGoalReached() returns false. Before any goal has been assigned or after the current goal was reached,
 *    this function returns true.
 *    
 * This class handles the communication with other ros packages, this involves retrieving the most recent robot position and velocity, 
 * the most recent costmap as well as any parameter changes during runtime. Furthermore, parts of the global plan already passed by the robot
 * are pruned. However, most of the actual planning takes place in Planner.h
 * 
 * This planner always is in one of three states: GoalReached, OnlyRotationRequired, DrivingRequired
 * from which only the first one results in isGoalReached() returning true.
 * */
class SebLocalPlannerROS : public nav_core::BaseLocalPlanner
{
public:
	SebLocalPlannerROS();
	
	~SebLocalPlannerROS() override;

	/**
	 * @brief  Constructs the local planner
	 * @param name The name to give this instance of the local planner
	 * @param tf A pointer to a transform listener
	 * @param costmap_ros The cost map to use for assigning costs to local plans
	 */
	void initialize(std::string name, tf2_ros::Buffer* tf_, costmap_2d::Costmap2DROS* costmapRos_) override;
	
	/**
	 * @brief  Given the current position, orientation, and velocity of the robot, compute velocity commands to send to the base
	 * @param cmd_vel Will be filled with the velocity command to be passed to the robot base
	 * @return True if a valid velocity command was found, false otherwise
	 */
	bool computeVelocityCommands(Twist& cmd_vel) override;

	/**
	 * @brief  Check if the goal pose has been achieved by the local planner
	 * @return True if achieved, false otherwise
	 */
	bool isGoalReached() override;

	/**
	 * @brief  Set the plan that the local planner is following
	 * @param plan The plan to pass to the local planner
	 * @return True if the plan was updated successfully, false otherwise
	 */
	bool setPlan(const std::vector<PoseStamped>& plan) override;	

private:
	enum class State {GoalReached, OnlyRotationRequired, DrivingRequired};
	
	/* Methods */
	std::pair<std::vector<PoseStamped>, bool> transformGlobalPlanToCostmapFrame();
	bool updateRobotPositionData();
	bool updateRobotVelocityData();
	void pruneGlobalPlan();
	
	void updatePlanningStatus(const PoseStamped& finalGoal);
	void updateMaximumVelocity(const std::vector<std::unique_ptr<DetectedObject>>& detectedObjects);
	
	void limitVelocities(double& velX, double& velTheta) const;
	
	double convertAngularVelocityToSteeringAngle(double velX, double velTheta) const;
	
	// Point must be in costmap frame and meter-scale. LocalCostmap margin is applied
	bool isPointInCostmap(const geometry_msgs::Point& point) const;
	
	void reconfigureCallback(SebLocalPlannerReconfigureConfig& cfg, uint32_t level);
	
	void setCurrentState(State state);
	
	bool isSimilarToGlobalPlan(const std::vector<PoseStamped>& plan) const;
	
	/* Member Variables */
	bool isInitialized;
	
	// Current State
	PoseStamped robotPose;
	Twist robotVelocity;	
	double currentMaxLinearVelocity;
	
	ros::Publisher planningTimePublisher;
	ros::Publisher planningTimeGlobalPublisher;
	
	State currentState;	

	// Current plan
	std::vector<PoseStamped> globalPlan;
	bool forceBandReinitializationNextIteration;
	
	// Owned Components
	Planner* planner;	
	SebConfig config;
	base_local_planner::OdometryHelperRos odometryHelper;
	boost::shared_ptr<dynamic_reconfigure::Server<SebLocalPlannerReconfigureConfig>> dynamicReconfigure;
	DetectedObjectParser* detectedObjectParser;
	
	// External Components
	costmap_2d::Costmap2DROS* costmapRos;
	costmap_2d::Costmap2D* costmap;
	tf2_ros::Buffer* tf;
	
	std::string localCostmapFrame;
	
	
};

}

#endif //SEMANTIC_ELASTIC_BANDS_LOCAL_PLANNER_ROS_H
