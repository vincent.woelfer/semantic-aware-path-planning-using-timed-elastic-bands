#ifndef SEMANTIC_ELASTIC_BANDS_GRID2DWRAPPER_H
#define SEMANTIC_ELASTIC_BANDS_GRID2DWRAPPER_H

#include <Eigen/src/Core/util/Macros.h>
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/cost_values.h>
#include <ros/console.h>

namespace semantic_elastic_bands {

/* Wrapper which conforms to Ceres Grid2D interface. Row-Major order is assumed.
 * */
struct Grid2DWrapper
{
public:
	explicit Grid2DWrapper(costmap_2d::Costmap2D* costmap) :
			costmap(costmap),
			maxX(static_cast<int>(costmap->getSizeInCellsX() - 1)),
			maxY(static_cast<int>(costmap->getSizeInCellsY() - 1)),
			error(false)
	{	
	}

	EIGEN_STRONG_INLINE void GetValue(const int r, const int c, double* f) const {
		int x = r;
		int y = c;
		
		if(ROS_UNLIKELY(x < 0 || x > maxX || y < 0 || y > maxY)) {
			//ROS_ERROR("Tried to access costmap at %d/%d which is outside of its bounds!", x, y);
			x = std::clamp(x, 0, maxX);
			y = std::clamp(y, 0, maxY);
			error = true;
		}
				
		unsigned char cost = costmap->getCost(x, y);
		
		if(ROS_UNLIKELY(cost == costmap_2d::NO_INFORMATION)) {
			cost = 0;
		}
		*f = static_cast<double>(cost);
	}

	EIGEN_STRONG_INLINE void GetValue(const double r, const double c, double* f) const {
		int x = std::floor(r);
		int y = std::floor(c);

		if(ROS_UNLIKELY(x < 0 || x > maxX || y < 0 || y > maxY)) {
			ROS_ERROR_THROTTLE(1.0, "Tried to access costmap at %d/%d which is outside of its bounds!", x, y);
			x = std::clamp(x, 0, maxX);
			y = std::clamp(y, 0, maxY);
			error = true;
		}

		unsigned char cost = costmap->getCost(x, y);

		if(ROS_UNLIKELY(cost == costmap_2d::NO_INFORMATION)) {
			cost = 0;
		}
		*f = static_cast<double>(cost);
	}
	
	bool hasErrorOccurred() const {
		const bool tmp = error;
		error = false;
		return tmp;
	}

private:
	const costmap_2d::Costmap2D* costmap;
	const int maxX;
	const int maxY;
	mutable bool error;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_GRID2DWRAPPER_H
