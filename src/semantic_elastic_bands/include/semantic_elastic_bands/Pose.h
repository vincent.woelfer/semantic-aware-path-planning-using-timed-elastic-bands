#ifndef SEMANTIC_ELASTIC_BANDS_POSE_H
#define SEMANTIC_ELASTIC_BANDS_POSE_H

#include <Eigen/Geometry>
#include <geometry_msgs/Pose.h>
#include "Point.h"

namespace semantic_elastic_bands {

/* Stores a pose in continuous 2-dimensional space with a rotation theta
 * */
class Pose
{
public:
	explicit Pose();
	Pose(Eigen::Vector2d position, double theta);
	Pose(double x, double y, double theta);

	~Pose() = default;

	static Pose constructAverage(const Pose& a, const Pose& b);
	static double getLinearDistance(const Pose& a, const Pose& b);

	bool operator==(const Pose& other) const;
	bool operator!=(const Pose& other) const;
	
	[[nodiscard]] geometry_msgs::Pose toPoseMsg() const;
	[[nodiscard]] Point toPoint() const;

	// Members	
	Eigen::Vector2d position;
	double theta;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

#endif //SEMANTIC_ELASTIC_BANDS_POSE_H
