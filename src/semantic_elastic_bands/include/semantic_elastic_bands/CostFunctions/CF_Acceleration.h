#ifndef SEMANTIC_ELASTIC_BANDS_CF_ACCELERATION_H
#define SEMANTIC_ELASTIC_BANDS_CF_ACCELERATION_H

#include <ceres/ceres.h>

#include "PenaltyUtilityFunctions.h"
#include "semantic_elastic_bands/SebConfig.h"

namespace semantic_elastic_bands {

/* This Cost Function governs the linear and angular acceleration between 3 consecutive poses. First, the velocity between q1 <-> q2 and q2 <-> q3 is computed.
 * Based on these, the acceleration can be computed.
 * There are two special variants of this cost function below: One for the fist and one for the last node pair of the band. Instead of the non-existent velocity 
 * before/after the start/end of the band, a predefined velocity is assumed. This allows us to use the robots current velocity as starting velocity and the desired
 * terminal velocity as end velocity (most of the time this is 0).
 * */
class CF_Acceleration
{
public:
	// Constructor
	explicit CF_Acceleration(double weight, double mapResolution, const SebConfig& config) :
			weight(weight),
			mapResolution(mapResolution),
			config(config) {}

	template<typename T>
	bool operator()(const T* pos1, const T* theta1, const T* pos2, const T* theta2, const T* pos3, const T* theta3, const T* dt1, const T* dt2, T* residual) const {
		// Linear Acceleration
		T dx1 = pos2[0] - pos1[0];
		T dy1 = pos2[1] - pos1[1];
		T dx2 = pos3[0] - pos2[0];
		T dy2 = pos3[1] - pos2[1];

		T dist1 = ceres::sqrt(dx1 * dx1 + dy1 * dy1) * T(mapResolution);
		T dist2 = ceres::sqrt(dx2 * dx2 + dy2 * dy2) * T(mapResolution);

		T angleDiff1 = normalizeTheta(theta2[0] - theta1[0]);
		T angleDiff2 = normalizeTheta(theta3[0] - theta2[0]);

		// Compute exact arc length
		if(config.optimization.useExactArcLength) {
			if(angleDiff1 != 0.0) {
				T radius = dist1 / (T(2.0) * ceres::sin(angleDiff1/T(2.0)));
				dist1 = ceres::abs(angleDiff1 * radius);	
			}
			if(angleDiff2 != 0.0) {
				T radius = dist2 / (T(2.0) * ceres::sin(angleDiff2/T(2.0)));
				dist2 = ceres::abs(angleDiff2 * radius);
			}
		}
			
		T vel1 = dist1 / dt1[0];
		T vel2 = dist2 / dt2[0];

		// Check for backwards motion and adjust sign of vel accordingly
		T direction1 = dot(dx1, dy1, ceres::cos(theta1[0]), ceres::sin(theta1[0]));
		T direction2 = dot(dx2, dy2, ceres::cos(theta2[0]), ceres::sin(theta2[0]));

		vel1 *= fastSigmoid(direction1 * T(100.0));
		vel2 *= fastSigmoid(direction2 * T(100.0));

		T accLin = ((vel2 - vel1) * T(2.0)) / (dt1[0] + dt2[0]);

		// Angular Acceleration
		T omega1 = angleDiff1 / dt1[0];
		T omega2 = angleDiff2 / dt2[0];
		T accRot = (omega2 - omega1) * T(2.0) / (dt1[0] + dt2[0]);

		residual[0] = (T) weight * boundToIntervalMirrored(accLin, T(config.robot.maxAccX));
		residual[1] = (T) weight * boundToIntervalMirrored(accRot, T(config.robot.maxAccTheta));

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weight, double mapResolution, const SebConfig& config) {
		return new ceres::AutoDiffCostFunction<CF_Acceleration, 2, // 2 residual (velX and theta-Error), this gets minimized
				2, // 2 Parameters (x,y) in parameter block (1. point)
				1, // 1 Parameter (theta) in parameter block (1. point theta)
				2, // 2 Parameters (x,y) in parameter block (2. point)
				1, // 1 Parameter (theta) in parameter block (2. point theta)
				2, // 2 Parameters (x,y) in parameter block (3. point)
				1, // 1 Parameter (theta) in parameter block (3. point theta)
				1, // 1 Parameters (1. dt) in parameter block
				1> // 1 Parameters (2. dt) in parameter block
				(new CF_Acceleration(weight, mapResolution, config));
	}

protected:
	const double weight;
	const double mapResolution;
	const SebConfig& config;
};

class CF_AccelerationStart
{
public:
	// Constructor
	explicit CF_AccelerationStart(double weight, double mapResolution, const SebConfig& config, double velXStart, double velThetaStart) :
			weight(weight),
			mapResolution(mapResolution),
			config(config),
			velXStart(velXStart),
			velThetaStart(velThetaStart) {}

	template<typename T>
	bool operator()(const T* pos1, const T* theta1, const T* pos2, const T* theta2, const T* dt1, T* residual) const {
		// Linear Acceleration
		T dx1 = pos2[0] - pos1[0];
		T dy1 = pos2[1] - pos1[1];

		T dist1 = ceres::sqrt(dx1 * dx1 + dy1 * dy1) * T(mapResolution);
		T angleDiff1 = normalizeTheta(theta2[0] - theta1[0]);

		if(config.optimization.useExactArcLength && angleDiff1 != 0.0) {
			T radius = dist1 / (T(2.0) * ceres::sin(angleDiff1/T(2.0)));
			dist1 = ceres::abs(angleDiff1 * radius);
		}

		T velStart = T(velXStart);
		T vel1 = dist1 / dt1[0];

		// Consider directional changes		
		T direction1 = dot(dx1, dy1, ceres::cos(theta1[0]), ceres::sin(theta1[0]));

		vel1 *= fastSigmoid(direction1 * T(100.0));

		T accLin = (vel1 - velStart) / dt1[0];

		// Angular Acceleration
		T omegaStart = T(velThetaStart);
		T omega1 = angleDiff1 / dt1[0];
		T accRot = (omega1 - omegaStart) / dt1[0];

		residual[0] = (T) weight * boundToIntervalMirrored(accLin, T(config.robot.maxAccX));
		residual[1] = (T) weight * boundToIntervalMirrored(accRot, T(config.robot.maxAccTheta));

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weight, double mapResolution, const SebConfig& config, double velXStart, double velThetaStart) {
		return new ceres::AutoDiffCostFunction<CF_AccelerationStart, 2, // 2 residual (velX and theta-Error), this gets minimized
				2, // 2 Parameters (x,y) in parameter block (1. point)
				1, // 1 Parameter (theta) in parameter block (1. point theta)
				2, // 2 Parameters (x,y) in parameter block (2. point)
				1, // 1 Parameter (theta) in parameter block (2. point theta)
				1> // 1 Parameters (1. dt) in parameter block
				(new CF_AccelerationStart(weight, mapResolution, config, velXStart, velThetaStart));
	}

protected:
	const double weight;
	const double mapResolution;
	const SebConfig& config;
	const double velXStart;
	const double velThetaStart;
};

class CF_AccelerationGoal
{
public:
	// Constructor
	explicit CF_AccelerationGoal(double weight, double mapResolution, const SebConfig& config, double velXGoal, double velThetaGoal) :
			weight(weight),
			mapResolution(mapResolution),
			config(config),
			velXGoal(velXGoal),
			velThetaGoal(velThetaGoal) {}

	template<typename T>
	bool operator()(const T* pos1, const T* theta1, const T* pos2, const T* theta2, const T* dt1, T* residual) const {
		// Linear Acceleration
		T dx1 = pos2[0] - pos1[0];
		T dy1 = pos2[1] - pos1[1];

		T dist1 = ceres::sqrt(dx1 * dx1 + dy1 * dy1) * T(mapResolution);
		T angleDiff1 = normalizeTheta(theta2[0] - theta1[0]);

		if(config.optimization.useExactArcLength && angleDiff1 != 0.0) {
			T radius = dist1 / (T(2.0) * ceres::sin(angleDiff1/T(2.0)));
			dist1 = ceres::abs(angleDiff1 * radius);
		}

		T vel1 = dist1 / dt1[0];
		T velGoal = T(velXGoal);

		// Consider directional changes
		T direction1 = dot(dx1, dy1, ceres::cos(theta1[0]), ceres::sin(theta1[0]));

		vel1 *= fastSigmoid(direction1 * T(100.0));

		T accLin = (velGoal - vel1) / dt1[0];

		// Angular Acceleration
		T omegaGoal = T(velThetaGoal);
		T omega1 = angleDiff1 / dt1[0];
		T accRot = (omegaGoal - omega1) / dt1[0];

		residual[0] = (T) weight * boundToIntervalMirrored(accLin, T(config.robot.maxAccX));
		residual[1] = (T) weight * boundToIntervalMirrored(accRot, T(config.robot.maxAccTheta));

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weight, double mapResolution, const SebConfig& config, double velXGoal, double velThetaGoal) {
		return new ceres::AutoDiffCostFunction<CF_AccelerationGoal, 2, // 2 residual (velX and theta-Error), this gets minimized
				2, // 2 Parameters (x,y) in parameter block (1. point)
				1, // 1 Parameter (theta) in parameter block (1. point theta)
				2, // 2 Parameters (x,y) in parameter block (2. point)
				1, // 1 Parameter (theta) in parameter block (2. point theta)
				1> // 1 Parameters (1. dt) in parameter block
				(new CF_AccelerationGoal(weight, mapResolution, config, velXGoal, velThetaGoal));
	}

protected:
	const double weight;
	const double mapResolution;
	const SebConfig& config;
	const double velXGoal;
	const double velThetaGoal;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_ACCELERATION_H
