#ifndef SEMANTIC_ELASTIC_BANDS_CF_KINEMATICSCARLIKE_H
#define SEMANTIC_ELASTIC_BANDS_CF_KINEMATICSCARLIKE_H

#include <ceres/ceres.h>
#include "PenaltyUtilityFunctions.h"
#include "semantic_elastic_bands/Utility.h"
#include "semantic_elastic_bands/SebConfig.h"

namespace semantic_elastic_bands {

/* This Cost Function governs the kinematic constraints for a carlike robot, eg. one with a minimum turning radius. There are two resiudals in this cost function,
 * 1) the holonomic error and the turning radius error.
 * */
class CF_KinematicsCarlike 
{
public:
	// Constructor
	explicit CF_KinematicsCarlike(double weightNonHolonomic, double weightTurningRadius, double mapResolution, const SebConfig& config) :
			weightNonHolonomic(weightNonHolonomic),
			weightTurningRadius(weightTurningRadius),
			mapResolution(mapResolution),
			config(config) {}

	template<typename T>
	bool operator()(const T* pos1, const T* theta1, const T* pos2, const T* theta2, T* residual) const {
		T cos1 = ceres::cos(theta1[0]);
		T cos2 = ceres::cos(theta2[0]);
		T sin1 = ceres::sin(theta1[0]);
		T sin2 = ceres::sin(theta2[0]);
		T aux1 = cos1 + cos2;
		T aux2 = sin1 + sin2;

		T dx = pos2[0] - pos1[0];
		T dy = pos2[1] - pos1[1];
		
		// Non-holonomic curvature error
		T nonHolonomicError = ceres::abs(cross(aux1, aux2, dx, dy));
		residual[0] = T (weightNonHolonomic) * nonHolonomicError;

		// Turning radius for carlike
		T angleDiff = ceres::abs(normalizeTheta(theta2[0] - theta1[0]));
		T turningRadiusError;
				
		if(angleDiff == 0.0) {
			turningRadiusError = angleDiff;
		} else if (config.optimization.useExactArcLength) {
			// Use exact arc length
			T distance = ceres::sqrt(dx*dx + dy*dy) * T(mapResolution);
			turningRadiusError = ceres::abs(  distance/(T(2.0) * ceres::sin(angleDiff/T(2.0)))  );
		} else {
			T distance = ceres::sqrt(dx*dx + dy*dy) * T(mapResolution);
			turningRadiusError = distance / angleDiff;
		}

		residual[1] = T (weightTurningRadius) * boundFromBelow(turningRadiusError, T(config.robot.minTurningRadius), T(0.0));
		
		return true;
	}

	static ceres::CostFunction* createCostFunction(double weightNonHolonomic, double weightTurningRadius, double mapResolution, const SebConfig& config) {
		return new ceres::AutoDiffCostFunction<CF_KinematicsCarlike, 2, 2, 1, 2, 1>(new CF_KinematicsCarlike(weightNonHolonomic, weightTurningRadius, mapResolution, config));
	}

protected:
	const double weightNonHolonomic;
	const double weightTurningRadius;
	const double mapResolution;
	const SebConfig& config;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_KINEMATICSCARLIKE_H

