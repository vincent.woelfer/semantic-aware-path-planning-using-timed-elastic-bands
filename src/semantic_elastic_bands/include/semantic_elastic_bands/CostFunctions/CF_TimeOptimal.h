#ifndef SEMANTIC_ELASTIC_BANDS_CF_TIMEOPTIMAL_H
#define SEMANTIC_ELASTIC_BANDS_CF_TIMEOPTIMAL_H

#include <ceres/ceres.h>

namespace semantic_elastic_bands {

/* This Cost Function reduces the transition time between two consecutive poses. The Cost Functions also ensures that only positive time differences results in a valid optimization step.
 */
class CF_TimeOptimal : public ceres::SizedCostFunction<1, 1>
{
public:
	~CF_TimeOptimal() override = default;

	// Constructor
	explicit CF_TimeOptimal(double weight) :
			weight(weight) {}

	bool Evaluate(double const* const* dt, double* residual, double** jacobian) const override {
		if(ROS_UNLIKELY(dt[0][0] <= 0.0)) {
			return false;
		}

		residual[0] = weight * dt[0][0];

		// Compute the Jacobian if asked for.
		if(jacobian != nullptr && jacobian[0] != nullptr) {
			jacobian[0][0] = weight;
		}

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weight) {
		return new CF_TimeOptimal(weight);
	}

protected:
	const double weight;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_TIMEOPTIMAL_H
