#ifndef SEMANTIC_ELASTIC_BANDS_PENALTYUTILITYFUNCTIONS_H
#define SEMANTIC_ELASTIC_BANDS_PENALTYUTILITYFUNCTIONS_H

namespace semantic_elastic_bands {

// Reduce intervals by this small amount to create a little leeway for errors during optimization
constexpr double defaultEpsilon = 0.01;

template<typename T>
inline T boundToInterval(const T& value, const T& min, const T& max, const T& epsilon) {
	if(value < (min + epsilon)) {
		// Below bound + eps
		return (min + epsilon) - value;
	} else if(value <= (max - epsilon)) {
		// Inside 
		return T(0.0);
	} else {
		// Above bound - eps
		return value - (max - epsilon);
	}
}

template<typename T>
inline T boundToInterval(const T& value, const T& min, const T& max) {
	return boundToInterval(value, min, max, (T)defaultEpsilon);
}

template<typename T>
inline T boundToIntervalMirrored(const T& value, const T& max, const T& epsilon) {
	if(value < (-max + epsilon)) {
		// Below bound + eps
		return (-max + epsilon) - value;
	} else if(value <= (max - epsilon)) {
		// Inside 
		return T(0.0);
	} else {
		// Above bound - eps
		return value - (max - epsilon);
	}
}

template<typename T>
inline T boundToIntervalMirrored(const T& value, const T& max) {
	return boundToIntervalMirrored(value, max, (T)defaultEpsilon);
}

template<typename T>
inline T boundFromBelow(const T& value, const T& min, const T& epsilon) {
	if(value >= min + epsilon) {
		return (T)0.0;
	} else {
		return (min + epsilon) - value;
	}
}

template<typename T>
inline T boundFromBelow(const T& value, const T& min) {
	return boundFromBelow(value, min, (T)defaultEpsilon);
}

inline double boundToIntervalDerivative(const double& value, const double& min, const double& max) {
	if(value < min + defaultEpsilon) {
		return -1.0;
	} else if(value <= max - defaultEpsilon) {
		return 0.0;
	} else {
		return 1.0;
	}
}

}

#endif //SEMANTIC_ELASTIC_BANDS_PENALTYUTILITYFUNCTIONS_H
