#ifndef SEMANTIC_ELASTIC_BANDS_CF_VELOCITY_H
#define SEMANTIC_ELASTIC_BANDS_CF_VELOCITY_H

#include <ceres/ceres.h>

#include "PenaltyUtilityFunctions.h"
#include "semantic_elastic_bands/Utility.h"
#include "semantic_elastic_bands/SebConfig.h"

namespace semantic_elastic_bands {

/* This Cost Function ensures a maximum linear and angular velocity.
 */
class CF_Velocity
{
public:
	// Constructor
	explicit CF_Velocity(double weight, double weightRotationDirection, double mapResolution, const SebConfig& config) :
			weight(weight),
			weightRotationDirection(weightRotationDirection),
			mapResolution(mapResolution),
			config(config) {}

	template<typename T>
	bool operator()(const T* pos1, const T* theta1, const T* pos2, const T* theta2, const T* dt, T* residual) const {
		T dx = pos2[0] - pos1[0];
		T dy = pos2[1] - pos1[1];
		T angleDifference = normalizeTheta(theta2[0] - theta1[0]);

		T dist = ceres::sqrt(dx * dx + dy * dy) * (T) mapResolution;
		
		if(config.optimization.useExactArcLength && angleDifference != 0.0) {
			T radius = dist / (T(2.0) * ceres::sin(angleDifference/T(2.0)));
			dist = ceres::abs(angleDifference * radius);
		}
		
		T vel = dist / dt[0];

		// Check for backwards motion and adjust sign of vel accordingly
		T direction = dot(dx, dy, ceres::cos(theta1[0]), ceres::sin(theta1[0]));
		vel *= fastSigmoid(direction * T(100.0));

		T omega = angleDifference / dt[0];

		residual[0] = (T) weight * boundToInterval(vel, (T) (-config.robot.maxVelXBackwards), (T) config.robot.maxVelX);
		residual[1] = (T) weight * boundToInterval(omega, (T) (-config.robot.maxVelTheta), (T) config.robot.maxVelTheta); // Currently [-max, max]
		
		residual[2] = (T) weightRotationDirection * boundFromBelow(-angleDifference, (T) 0.0, (T) 0.0);				

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weight, double weightRotationDirection, double mapResolution, const SebConfig& config) {
		return new ceres::AutoDiffCostFunction<CF_Velocity, 3, // 2 residuals (velX and theta-Error), this gets minimized
				2, // 2 Parameters (x,y) in parameter block (1. point)
				1, // 1 Parameter (theta) in parameter block (1. point theta)
				2, // 2 Parameters (x,y) in parameter block (2. point)
				1, // 1 Parameter (theta) in parameter block (2. point theta)
				1> // 1 Parameters (dt) in parameter block
				(new CF_Velocity(weight, weightRotationDirection, mapResolution, config));
	}

protected:
	const double weight;
	const double weightRotationDirection;
	const double mapResolution;
	const SebConfig& config;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_VELOCITY_H
