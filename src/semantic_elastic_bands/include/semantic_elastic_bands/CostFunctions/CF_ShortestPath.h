#ifndef SEMANTIC_ELASTIC_BANDS_CF_SHORTESTPATH_H
#define SEMANTIC_ELASTIC_BANDS_CF_SHORTESTPATH_H

#include <Eigen/Core>
#include <ceres/ceres.h>

namespace semantic_elastic_bands {

/* This Cost Function minimizes the overall path length by minimizing dx and dy between two consecutive poses.
 */
class CF_ShortestPath : public ceres::SizedCostFunction<2, 2, 2>
{
public:
	~CF_ShortestPath() override = default;

	// Constructor
	explicit CF_ShortestPath(double weight) :
			weight(weight) {}

	bool Evaluate(double const* const* x, double* residual, double** jacobian) const override {
		const double dx = x[1][0] - x[0][0];
		const double dy = x[1][1] - x[0][1];

		residual[0] = weight * dx;
		residual[1] = weight * dy;

		if(jacobian != nullptr) {
			if(ROS_UNLIKELY(dx == 0 && dy == 0)) {
				ROS_ERROR("Encountered two identical consecutive poses during ShortestPath-Costfunction calculation! Treating this optimizer step as failed");
				return false;
			}

			// Point 1
			if(jacobian[0] != nullptr) {
				jacobian[0][0] = -weight; // dev x0
				jacobian[0][1] = 0;       // dev x1
				jacobian[0][2] = 0;       // dev y0
				jacobian[0][3] = -weight; // dev y1
			}

			// Point 2
			if(jacobian[1] != nullptr) {
				jacobian[1][0] = weight; // dev x0
				jacobian[1][1] = 0;      // dev x1			
				jacobian[1][2] = 0;      // dev y0			
				jacobian[1][3] = weight; // dev y1
			}
		}

		return true;
	}

	static ceres::CostFunction* createCostFunction(const double weight) {
		return new CF_ShortestPath(weight);
	}

protected:
	const double weight;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_SHORTESTPATH_H
