#ifndef SEMANTIC_ELASTIC_BANDS_CF_REDUCEDVELOCITY_H
#define SEMANTIC_ELASTIC_BANDS_CF_REDUCEDVELOCITY_H

#include <ceres/ceres.h>

#include <utility>
#include "PenaltyUtilityFunctions.h"
#include "semantic_elastic_bands/SebConfig.h"

namespace semantic_elastic_bands {

/* This Cost Function ensures a reduced velocity in the proximity of humans. We define an upper and a lower distance bound. This results in three possible scenarios:
 * 1) dist > upper distance bound           => No reduction of maximal velocity at all.
 * 2) dist < upper bound but >= lower bound => Partial velocity reduction which scales linearly from "velocityReduction" to 1.0 (no reduction) with the distance
 * 3) dist < lower distance bound           => Full reduction, the maximum allowed velocity is reduced by the factor "velocityReduction"
 */
class CF_ReducedVelocity
{
public:
	// Constructor
	explicit CF_ReducedVelocity(double weight, double mapResolution, const SebConfig& config, Eigen::Matrix<double, 2, 1> obstacle, double lowerDistanceBound, double upperDistanceBound, double velocityReduction) :
			weight(weight),
			mapResolution(mapResolution),
			config(config),
			obstacle(std::move(obstacle)),
			lowerDistanceBound(lowerDistanceBound),
			upperDistanceBound(upperDistanceBound),
			velocityReduction(velocityReduction) {}

	template<typename T>
	bool operator()(const T* x1, const T* x2, const T* dt, T* residual) const {
		// Distance to Obstacle
		T dxObst = obstacle[0] - x1[0];
		T dyObst = obstacle[1] - x1[1];
		T distToObstacle = ceres::sqrt(dxObst * dxObst + dyObst * dyObst) * (T) mapResolution;

		// Distance between poses 
		T dx = x2[0] - x1[0];
		T dy = x2[1] - x1[1];
		T dist = ceres::sqrt(dx * dx + dy * dy) * (T) mapResolution;

		T vel = dist / dt[0];

		// Compute ratio between reduced speed(0) and normal speed(1)
		T ratio;

		if(distToObstacle < lowerDistanceBound) {
			ratio = (T) velocityReduction;
		} else if(distToObstacle > upperDistanceBound) {
			ratio = (T) 1.0;
		} else {
			// 0 -> completely inner bound, 1 -> completely outer bound
			ratio = (distToObstacle - (T) lowerDistanceBound) / ((T) upperDistanceBound - (T) lowerDistanceBound); 
			ratio = (T) velocityReduction + ratio * (T) (1.0 - velocityReduction);
		}

		// Compute maximally reduced speed		
		T reducedMaxVelX = (T) config.robot.maxVelX * ratio;

		residual[0] = (T) weight * boundToInterval(vel, -reducedMaxVelX, reducedMaxVelX, (T) 0.0);

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weight, double mapResolution, const SebConfig& config, const Eigen::Matrix<double, 2, 1>& obstacle, double lowerDistanceBound, double upperDistanceBound, double velocityReduction) {
		return new ceres::AutoDiffCostFunction<CF_ReducedVelocity, 1, // 1 residual (velX-Error), this gets minimized
				2, // 2 Parameters (x,y) in parameter block (1. point)
				2, // 2 Parameters (x,y) in parameter block (2. point)
				1> // 1 Parameters (dt) in parameter block
				(new CF_ReducedVelocity(weight, mapResolution, config, obstacle, lowerDistanceBound, upperDistanceBound, velocityReduction));
	}

protected:
	const double weight;
	const double mapResolution;
	const SebConfig& config;
	const Eigen::Matrix<double, 2, 1> obstacle;

	// DistanceBounds are already in correct resolution
	const double lowerDistanceBound;
	const double upperDistanceBound;

	const double velocityReduction;

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_REDUCEDVELOCITY_H
