#ifndef SEMANTIC_ELASTIC_BANDS_CF_OCCUPANCYGRID_H
#define SEMANTIC_ELASTIC_BANDS_CF_OCCUPANCYGRID_H

#include <Eigen/Core>
#include <ceres/ceres.h>
#include <ros/ros.h>
#include "semantic_elastic_bands/Grid2DWrapper.h"
#include "semantic_elastic_bands/SebBiCubicInterpolator.h"
#include "PenaltyUtilityFunctions.h"
#include <costmap_2d/costmap_2d.h>

namespace semantic_elastic_bands {

/* This Cost Function governs the cost induced by the costmap. This included costs from detected obstacles in the occupancy map as well as costs induced by 
 * semantic rules which express spatial preferences. This cost function may return false which signals that the optimizer step is invalid.
 * This happens when the position in question lies in an impassable cell or outside of the costmap.
 * */
class CF_OccupancyGrid
{
public:
	// Constructor
	// OccupancyMaps and grid2D both store data in row-major by default 
	explicit CF_OccupancyGrid(costmap_2d::Costmap2D* costmap, double weight, double maxTraversalCost) :
			weight(weight),
			maxTraversalCost(maxTraversalCost),
			wrapper(costmap),
			interpolator(wrapper) {
	}

	template<typename T>
	bool operator()(const T* x, T* residual) const {
		T value;
		interpolator.Evaluate(x[0], x[1], &value);	
		
		if(ROS_UNLIKELY(value > maxTraversalCost)) {
			ROS_ERROR("Path crossed a non-traversable part of the map during optimization, treating this optimizer step as failed!");
			return false;
		}
		if(ROS_UNLIKELY(wrapper.hasErrorOccurred())) {
			ROS_ERROR("Tried to access costmap outside of it's bounds, treating this optimizer step as failed!");
			return false;
		}
		
		residual[0] = (T)weight * value;
		return true;
	}

	static ceres::CostFunction* createCostFunction(costmap_2d::Costmap2D* costmap, double weight, double maxTraversalCost) {
		return new ceres::AutoDiffCostFunction<CF_OccupancyGrid, 1, 2>(new CF_OccupancyGrid(costmap, weight, maxTraversalCost));
	}

protected:
	const double weight;
	const double maxTraversalCost;
	const semantic_elastic_bands::Grid2DWrapper wrapper;
	const semantic_elastic_bands::BiCubicInterpolator<Grid2DWrapper> interpolator;

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

#endif //SEMANTIC_ELASTIC_BANDS_CF_OCCUPANCYGRID_H
