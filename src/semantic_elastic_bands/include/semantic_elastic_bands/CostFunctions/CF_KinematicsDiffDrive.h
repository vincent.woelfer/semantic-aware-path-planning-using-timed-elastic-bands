#ifndef SEMANTIC_ELASTIC_BANDS_CF_KINEMATICSDIFFDRIVE_H
#define SEMANTIC_ELASTIC_BANDS_CF_KINEMATICSDIFFDRIVE_H

#include <ceres/ceres.h>
#include "PenaltyUtilityFunctions.h"
#include "semantic_elastic_bands/Utility.h"

namespace semantic_elastic_bands {

/* This Cost Function governs the kinematic constraints for a differential drive robot, eg. one with no minimum turning radius. There are two residuals in this cost function,
 * 1) the holonomic error and the drive direction error.
 * */
class CF_KinematicsDiffDrive : public ceres::SizedCostFunction<2, 2, 1, 2, 1>
{
public:
	~CF_KinematicsDiffDrive() override = default;

	// Constructor
	explicit CF_KinematicsDiffDrive(double weightNonHolonomic, double weightDriveDirection) :
			weightNonHolonomic(weightNonHolonomic),
			weightDriveDirection(weightDriveDirection) {}

	bool Evaluate(double const* const* x, double* residual, double** jacobian) const override {
		// Parameters:
		// 0: pos1
		// 1: theta1
		// 2: pos2
		// 3: theta2
		double cos1 = ceres::cos(x[1][0]);
		double cos2 = ceres::cos(x[3][0]);
		double sin1 = ceres::sin(x[1][0]);
		double sin2 = ceres::sin(x[3][0]);
		double sin12 = sin1 + sin2;
		double cos12 = cos1 + cos2;
		
		double dx = x[2][0] - x[0][0];
		double dy = x[2][1] - x[0][1];

		double nonHolonomicError = cos12 * dy - sin12 * dx;
		double driveDirError = dot(dx, dy, cos1, sin1); // dot product. 1 => collinear, < 0 => deg over 90° -> error

		residual[0] = weightNonHolonomic * ceres::abs(nonHolonomicError);
		residual[1] = weightDriveDirection * boundFromBelow(driveDirError, 0.0, 0.0);

		// Compute jacobian
		// jacobian[i] => i is Parameter block (so 0:pos1, 1:theta1, 2:pos2, 3:theta2)
		// jacobian[i][j] => j is numResiduals * parameterCount(i) (so 4 for pos and 2 for theta)
		// [0][0] => residual=0, c=0 | [0][1] => residual=0, c=1
		// [0][2] => residual=1, c=0 | [0][3] => residual=1, c=1		

		if(jacobian != nullptr) {
			// Penalty bound from below derivative
			double nonHolonomicDevSign = sign(nonHolonomicError);
			double driveDirDev = (driveDirError >= 0.0) ? 0.0 : -1.0;

			// Include weights here
			nonHolonomicDevSign *= weightNonHolonomic;
			driveDirDev *= weightDriveDirection;

			// pos1			
			if(jacobian[0] != nullptr) {
				jacobian[0][0] = sin12 * nonHolonomicDevSign;   // non-holonomic x1
				jacobian[0][1] = -cos12 * nonHolonomicDevSign;  // non-holonomic y1
				jacobian[0][2] = -cos1 * driveDirDev;           // drive-dir     x1
				jacobian[0][3] = -sin1 * driveDirDev;           // drive-dir     y1
			}

			// theta1
			if(jacobian[1] != nullptr) {
				jacobian[1][0] = -driveDirError * nonHolonomicDevSign; // non-holonomic theta 1
				jacobian[1][1] = (-dx*sin1 + dy*cos1) * driveDirDev;   // drive-dir theta 1
			}

			// pos2
			if(jacobian[2] != nullptr) {
				jacobian[2][0] = -sin12 * nonHolonomicDevSign;  // non-holonomic x2
				jacobian[2][1] = cos12 * nonHolonomicDevSign;   // non-holonomic y2
				jacobian[2][2] = cos1 * driveDirDev;            // drive-dir     x2
				jacobian[2][3] = sin1 * driveDirDev;            // drive-dir     y2
			}

			// theta2
			if(jacobian[3] != nullptr) {
				jacobian[3][0] = (-sin2*dy - cos2*dx) * nonHolonomicDevSign;
				jacobian[3][1] = 0;                                             // drive-dir theta 2
			}
		}

		return true;
	}

	static ceres::CostFunction* createCostFunction(double weightNonHolonomic, double weightDriveDirection) {
		return new CF_KinematicsDiffDrive(weightNonHolonomic, weightDriveDirection);
	}

protected:
	const double weightNonHolonomic;
	const double weightDriveDirection;
};



}

#endif //SEMANTIC_ELASTIC_BANDS_CF_KINEMATICSDIFFDRIVE_H

