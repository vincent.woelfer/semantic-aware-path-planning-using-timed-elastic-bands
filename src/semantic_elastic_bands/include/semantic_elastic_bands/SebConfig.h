#ifndef SEMANTIC_ELASTIC_BANDS_SEBCONFIG_H
#define SEMANTIC_ELASTIC_BANDS_SEBCONFIG_H

#include <ros/console.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <Eigen/StdVector>

#include <mutex>
#include <costmap_2d/costmap_2d.h>

#include <semantic_elastic_bands/SebLocalPlannerReconfigureConfig.h>

namespace semantic_elastic_bands {

/* This Class stores all configurable parameters regarding our Semantic Elastic Band Planner. 
 * */
class SebConfig
{
public:
	// Get this parameter from move_base, this is ---NOT--- reconfigurable during runtime.
	// The value below is only used as a fallback, please configure this via the ros parameter framework
	double controllerFrequency = 10.0;
	
	/* ================================================================================== */
	// Everything below is overwritten by the values in the SebLocalPlannerReconfigure.cfg!
	// and any launch-file parameters.
	// Do NOT set values here! These are only default/fallback values
	/* ================================================================================== */
	
	std::string odomTopic = "/odom";
	std::string mapFrame = "/map";
	std::string detectedObjectsTopic = "/detected_objects";
	std::string trackedObjectsTopic = "/tracked_objects";
	
	enum class VerbosityLevel {none, normal, full};
	VerbosityLevel verbosityLevel = VerbosityLevel::none;
	
	struct Robot 
	{
		// Taken from official Waffle Turtlebot site
		double maxVelX = 0.26;         // max Vel X in m/s
		double maxVelXBackwards = 0.1; // max Vel X backwards in m/s
		double maxVelTheta = 1.82;     // max Vel Theta in rad/s
		double maxAccX = 0.75;         // max Acc X in m/s²
		double maxAccTheta = 2.50;     // max Acc Theta in rad/s²
		
		// Only for carlike:
		double minTurningRadius = 0.0; // Switch from differential-drive robot to carlike robot if > 0
		bool steeringAngleInsteadVelocity = false; // Compute control steering angle instead of angular velocity
		double wheelbase = 0.5; // Distance between the drive shaft and steering axle for carlike robots. Negative value for back-wheeled robots
		
		bool limitAcceleration = true;		
		
	} robot;
	
	struct Planning 
	{
		double globalPlanPruneDistance = 0.15;
		double maxGlobalPlanReuseDistance = 0.15;
		
		double maxBandReuseGoalDistance = 0.3;
		double maxBandReuseGoalAngleDeg = 120;
		
		double globalPlanMaxLookaheadDistance = 2.0; // in meter
		double globalPlanMaxLookaheadDuration = 5.0; // in seconds
		
		double localCostmapSafetyMarginInMeter = 0.5;
		
		double goalToleranceXY = 0.1;
		double goalToleranceThetaDeg = 10.0;
		
		double velocityCommandLookaheadTime = 0.25;
		
		double maxTraversalCost = 200;
		
		double initializationMaxSpeedPercentage = 1.0;
		
	} planning;
	
	struct Optimization 
	{
		// Optimization Weights
		double weight_optimalTime = 2.0;
		double weight_shortestPath = 1.0;
		double weight_occupancyGrid = 0.035;
		double weight_velocity = 5.0;
		double weight_acceleration = 5.0;
		double weight_kinematicNonHolonomic = 1000.0;
		double weight_kinematicDriveDirection = 100.0;
		double weight_kinematicTurningRadius = 100.0;
		double weight_reducedVelocity = 20.0;
		double weight_rotationDirection = 10;
		
		// Resizing and local path construction
		double referenceTimeDifference = 0.275;
		double hysteresisTimeDiffFactor = 0.25;
		double hysteresisIncreaseFactor = 0.02;
		int minElasticBandSamples = 6;
		int maxElasticBandSamples = 150;
		int maxResizeIterations = 30;
		double isSolutionUsableToleranceFactor = 5.0;
		
		// Path Options
		bool fixStartOrientation = true;
		bool fixGoalOrientation = true;
		bool allowFreeGoalVelocity = false;
		bool allowBackwardsInitialization = true;

		// Ceres Options
		int numOptimizerIterations = 1;
		double ceresMaxSolverTime = 0.175;
		int ceresMaxNumIterations = 1000;		
		int ceresNumThreads = 6;
		
		// Accuracy vs. Approximation Settings
		bool useExactArcLength = true;
		
	} optimization;

	/* Methods */
	void reconfigure(SebLocalPlannerReconfigureConfig& config);
	
	void loadFromRosNodeHandle(const ros::NodeHandle& handle);
	
	void checkParameterValidity() const;
	
	void checkParameterValidityRegardingCostmap(const costmap_2d::Costmap2D* costmap) const;
	
	std::mutex& getConfigMutex();
	
	[[nodiscard]] bool isVerbosityAtLeastNormal() const;
	[[nodiscard]] bool isVerbosityAtLeastFull() const;

private:
	std::mutex mutex;
	
	bool parseVerbosityLevel(const std::string& level);
	[[nodiscard]] std::string verbosityLevelToString() const;
};


}

#endif //SEMANTIC_ELASTIC_BANDS_SEBCONFIG_H
