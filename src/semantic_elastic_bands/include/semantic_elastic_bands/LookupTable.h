#ifndef SEMANTIC_ELASTIC_BANDS_LOOKUPTABLE_H
#define SEMANTIC_ELASTIC_BANDS_LOOKUPTABLE_H

#include <cmath>
#include <array>

/* Generic LookUp Table Class. Currently, only the cosine cost function is supported.
 * */
class LookupTable {
private:
	static constexpr int Size = 128;
	
public:	
	static constexpr int LastIndex = Size - 1;

private:
	//Function to build the lookup table
	static std::array<float, Size> initCosineCostFunction() {
		std::array<float, Size> a = {};

		for(int i = 0; i < Size; i++) {
			double x = ((double)i) / LastIndex;
			a[i] = static_cast<float>((1.0 + std::cos(x * M_PI)) / 2.0);
		}

		return a;
	}

public:
	static const std::array<float, Size> lutCosineCostFunction;
};

#endif //SEMANTIC_ELASTIC_BANDS_LOOKUPTABLE_H
