#ifndef SEMANTIC_ELASTIC_BANDS_UTILITY_H
#define SEMANTIC_ELASTIC_BANDS_UTILITY_H

#include <cmath>
#include <ceres/jet.h>
#include <costmap_2d/costmap_2d.h>
#include <ros/console.h>
#include <tf/transform_datatypes.h>

#include "Pose.h"
#include "LookupTable.h"

namespace semantic_elastic_bands {

/* 
 * This File offers several utility functions, mostly to convert between stuff.
 */

inline double deg2rad(double deg) {
	constexpr double toRad = M_PI / 180.0;
	return deg * toRad;
}

inline double rad2deg(double rad) {
	constexpr double toDeg = 180.0 / M_PI;
	return rad * toDeg;
}

template<typename T>
inline T sign(T x) {
	if(x > (T) 0) {
		return (T) 1;
	} else if(x < (T) 0) {
		return (T) (-1);
	} else {
		return (T) 0;
	}
}

template<typename T>
inline T fastSigmoid(T x) {
	return x / (T(1.0) + ceres::abs(x));
}

template<typename T>
inline T dot(T x1, T y1, T x2, T y2) {
	return x1 * x2 + y1 * y2;
}

template<typename T>
inline T cross(T x1, T y1, T x2, T y2) {
	return x1 * y2 - y1 * x2;
}

template<typename T>
inline T normalizeTheta(const T& angle) {
	// See https://github.com/ceres-solver/ceres-solver/blob/master/examples/slam/pose_graph_2d/normalize_angle.h

	// Normalizes the angle in radians between [-pi and pi).
	// Use ceres::floor because it is specialized for double and Jet types
	const T two_pi(2.0 * M_PI);
	const T pi(M_PI);
	return angle - two_pi * ceres::floor((angle + pi) / two_pi);
}

inline double averageTheta(double thetaA, double thetaB) {
	const double x = std::cos(thetaA) + std::cos(thetaB);
	const double y = std::sin(thetaA) + std::sin(thetaB);

	if(ROS_UNLIKELY(x == 0 && y == 0)) {
		return 0;
	} else {
		return std::atan2(y, x);
	}
}

inline double getAngleDifference(double thetaA, double thetaB) {
	// Computes the difference between to angles (in rad) while taking care of the wraparound from -pi to pi
	double angle = normalizeTheta(thetaB) - normalizeTheta(thetaA);
	constexpr double two_pi = M_PI * 2.0;

	if(angle > M_PI) {
		angle = angle - two_pi;
	} else if(angle < -M_PI) {
		angle = angle + two_pi;
	}

	return angle;
}

inline double toAngle(geometry_msgs::Quaternion quaternion) {
	return normalizeTheta(tf::getYaw(quaternion));
}

inline double getAngleBetweenPoints(double aX, double aY, double bX, double bY) {
	const double dx = bX - aX;
	const double dy = bY - aY;
	
	if(ROS_UNLIKELY(dx == 0 && dy == 0)) {
		return 0;
	} else {
		return normalizeTheta(std::atan2(dy, dx));
	}
}

inline double getAngleBetweenPoints(const semantic_elastic_bands::Pose& a, const semantic_elastic_bands::Pose& b) {
	return getAngleBetweenPoints(a.position.x(), a.position.y(), b.position.x(), b.position.y());
}

/* Distance Functions */
template<typename T>
inline double getDistance(T x1, T y1, T x2, T y2) {
	double dx = x1 - x2;
	double dy = y1 - y2;
	return std::sqrt(dx*dx + dy*dy);
}

template<typename T>
inline double getDistanceSquared(T x1, T y1, T x2, T y2) {
	double dx = x1 - x2;
	double dy = y1 - y2;
	return dx*dx + dy*dy;
}

inline double getDistance(const geometry_msgs::Point& a, const geometry_msgs::Point& b) {
	return getDistance(a.x, a.y, b.x, b.y);
}

inline double getDistanceSquared(const geometry_msgs::Point& a, const geometry_msgs::Point& b) {
	return getDistanceSquared(a.x, a.y, b.x, b.y);
}

inline double getDistance(const semantic_elastic_bands::Point& a, const semantic_elastic_bands::Point& b) {
	return getDistance(a.x, a.y, b.x, b.y);
}

inline double getDistanceSquared(const semantic_elastic_bands::Point& a, const semantic_elastic_bands::Point& b) {
	return getDistanceSquared(a.x, a.y, b.x, b.y);
}

inline double getDistance(const semantic_elastic_bands::Pose& a, const semantic_elastic_bands::Pose& b) {
	return (a.position - b.position).norm();
}

inline double getDistanceSquared(const semantic_elastic_bands::Pose& a, const semantic_elastic_bands::Pose& b) {
	return (a.position - b.position).squaredNorm();
}

inline double getDistanceToLineSegmentSquared(double pX, double pY, double startX, double startY, double endX, double endY) {
	double segmentX = endX - startX;
	double segmentY = endY - startY;
	
	double segmentLengthSquared = segmentX*segmentX + segmentY*segmentY;

	// If line segment is too short or a point
	if(segmentLengthSquared <= 0.01) {
		return getDistanceSquared(pX, pY, startX, startY);
	}

	// Project point onto line segment, clamp to t [0, 1] to cut of points extending the line segment ends
	double t = std::clamp(dot(pX - startX, pY - startY, segmentX, segmentY) / segmentLengthSquared, 0.0, 1.0);
	double projectionX = startX + t * segmentX;
	double projectionY = startY + t * segmentY;

	return getDistanceSquared(projectionX, projectionY, pX, pY);
}

inline double getDistanceToLineSegmentSquared(const Point& p, const Point& start, const Point& end) {
	return getDistanceToLineSegmentSquared(p.x, p.y, start.x, start.y, end.x, end.y);
}

inline double getDistanceToLineSegment(double pX, double pY, double startX, double startY, double endX, double endY) {
	return std::sqrt(getDistanceToLineSegmentSquared(pX, pY, startX, startY, endX, endY));
}

inline double getDistanceToLineSegment(const Point& p, const Point& start, const Point& end) {
	return std::sqrt(getDistanceToLineSegmentSquared(p, start, end));
}

inline double getArcDistance(double x1, double y1, double theta1, double x2, double y2, double theta2) {
	double linearDist = getDistance(x1, y1, x2, y2);
	double angleDifference = getAngleDifference(theta1, theta2);
	
	if(angleDifference != 0.0) {
		double radius = linearDist / (2.0 * std::sin(angleDifference/2.0));
		return std::abs(angleDifference * radius);	
	}
	return linearDist;	
}

// x=0 -> 1, x=1 -> 0 cost
inline double cosineCostFunction(double x) {
	int z = std::clamp((int)std::round(x * LookupTable::LastIndex), 0, LookupTable::LastIndex);
	return LookupTable::lutCosineCostFunction[z];
}

inline bool convertPoseToCellSpace(costmap_2d::Costmap2D* costmap, double worldX, double worldY, double theta, Pose& mapPose, double marginInCells) {
	// No floor() here because we dont want ints for cell centers, we want the exact position in cell space
	mapPose.position.x() = (worldX - costmap->getOriginX()) / costmap->getResolution();
	mapPose.position.y() = (worldY - costmap->getOriginY()) / costmap->getResolution();
	mapPose.theta = theta;

	if(ROS_LIKELY(mapPose.position.x() >= marginInCells && mapPose.position.x() <= costmap->getSizeInCellsX() - marginInCells &&
	              mapPose.position.y() >= marginInCells && mapPose.position.y() <= costmap->getSizeInCellsY() - marginInCells)) {
		return true;
	}

	return false;
}

inline void convertPoseToCellSpaceNoBounds(costmap_2d::Costmap2D* costmap, double worldX, double worldY, double theta, Pose& mapPose) {
	// No floor() here because we dont want ints for cell centers, we want the exact position in cell space
	mapPose.position.x() = (worldX - costmap->getOriginX()) / costmap->getResolution();
	mapPose.position.y() = (worldY - costmap->getOriginY()) / costmap->getResolution();
	mapPose.theta = theta;
}

inline void convertPointToCellSpaceNoBounds(costmap_2d::Costmap2D* costmap, double worldX, double worldY, Point& mapPoint) {
	mapPoint.x = std::floor((worldX - costmap->getOriginX()) / costmap->getResolution());
	mapPoint.y = std::floor((worldY - costmap->getOriginY()) / costmap->getResolution());
}

}

#endif //SEMANTIC_ELASTIC_BANDS_UTILITY_H
