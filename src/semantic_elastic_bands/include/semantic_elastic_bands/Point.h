#ifndef SEMANTIC_ELASTIC_BANDS_POINT_H
#define SEMANTIC_ELASTIC_BANDS_POINT_H

namespace semantic_elastic_bands {

/* Stores a 2-dimensional point in discrete space
 * */
class Point
{
public:
	explicit Point();
	Point(int x, int y);

	~Point() = default;

	bool operator==(const Point& other) const;
	bool operator!=(const Point& other) const;

	// Members	
	int x;
	int y;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_POINT_H
