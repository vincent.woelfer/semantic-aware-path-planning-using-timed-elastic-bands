#ifndef SEMANTIC_ELASTIC_BANDS_LINEOFSIGHTCALCULATOREXACT_H
#define SEMANTIC_ELASTIC_BANDS_LINEOFSIGHTCALCULATOREXACT_H

#include "CohenSutherlandClipping.h"

namespace semantic_elastic_bands {

/* This Class calculates the Line-Of-Sight cost between two cells on a grid with non-uniform traversal costs.
 * The calculation uses floating point precision, this means slower execution but more accurate results.
 */
template<class T>
class LineOfSightCalculatorExact
{
public:
	// Line of Sight between exact positions
	LineOfSightCalculatorExact(const T* mapData, unsigned int mapWidth, float startX, float startY, float endX, float endY);
	
	float getLineOfSightCost(float costOffset, float maxTraversalCost);

private:
	// Map
	const T* mapData;
	unsigned int mapWidth;
	inline float getCellCost(int x, int y) const;
	
	// Start/End coordinates of ray
	float startX;
	float startY;
	float endX;
	float endY;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_LINEOFSIGHTCALCULATOREXACT_H
