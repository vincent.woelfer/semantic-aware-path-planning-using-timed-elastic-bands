#ifndef SEMANTIC_ELASTIC_BANDS_LINEOFSIGHTCALCULATORCELLCENTER_H
#define SEMANTIC_ELASTIC_BANDS_LINEOFSIGHTCALCULATORCELLCENTER_H

#include "CohenSutherlandClipping.h"

namespace semantic_elastic_bands {

/* This Class calculates the Line-Of-Sight cost between two cells on a grid with non-uniform traversal costs.
 * The calculation uses integer math only, this means faster execution time. However, the start and end point must exactly lie on cell centers.
 */
template<class T>
class LineOfSightCalculatorCellCenter
{
public:
	// Line of Sight between Cell Centers 
	LineOfSightCalculatorCellCenter(const T* mapData, unsigned int mapWidth, int startX, int startY, int endX, int endY);
	
	float getLineOfSightCost(float costOffset, float maxTraversalCost);

private:
	// Map
	const T* mapData;
	unsigned int mapWidth;
	inline float getCellCost(int x, int y) const;
	
	int startX;
	int startY;
	int endX;
	int endY;
};

}

#endif //SEMANTIC_ELASTIC_BANDS_LINEOFSIGHTCALCULATORCELLCENTER_H
