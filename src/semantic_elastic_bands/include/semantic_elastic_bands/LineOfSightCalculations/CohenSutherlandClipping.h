#ifndef SEMANTIC_ELASTIC_BANDS_COHENSUTHERLANDCLIPPING_H
#define SEMANTIC_ELASTIC_BANDS_COHENSUTHERLANDCLIPPING_H

#include <cstdint>

namespace semantic_elastic_bands {

/* This is an implementation of the Cohen-Sutherland Clipping Algorithm.
 * This implementation is stateful, one line-of-sight check is initialized using the constructor.
 * This defines the start and end point of the line-of-sight. 
 * The length through any cell can then be calculated by passing the x/y coordinates of that cell into getLengthThroughCell().
 * // See https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
 */
class CohenSutherlandClipping
{
public:
	CohenSutherlandClipping(float startX, float startY, float endX, float endY);

	float getLengthThroughCell(int x, int y);
	
private:
	enum OutCode : unsigned int
	{
		INSIDE = 0,
		LEFT = 1,
		RIGHT = 2,
		BOTTOM = 4,
		TOP = 8
	};
	
	// Set in constructor for whole line of sight
	const float startXConst;
	const float startYConst;
	const float endXConst;
	const float endYConst;

	// Updated for each cell
	float minX{};
	float maxX{};
	float minY{};
	float maxY{};
	float startX{};
	float startY{};
	float endX{};
	float endY{};

	[[nodiscard]] inline unsigned int computeOutCode(float x, float y) const;

	inline void resetCurrentCell(float minX_, float maxX_, float minY_, float maxY_);
};

}

#endif //SEMANTIC_ELASTIC_BANDS_COHENSUTHERLANDCLIPPING_H

