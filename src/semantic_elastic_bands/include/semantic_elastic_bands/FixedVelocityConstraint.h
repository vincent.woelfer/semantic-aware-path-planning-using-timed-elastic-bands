#ifndef SEMANTIC_ELASTIC_BANDS_FIXEDVELOCITYCONSTRAINT_H
#define SEMANTIC_ELASTIC_BANDS_FIXEDVELOCITYCONSTRAINT_H

namespace semantic_elastic_bands {

/* Struct to hold information regarding the velocity at the start/end of the elastic band. This velocity may be fixed or free. If it is fixed, the 
 * linear and angular velocity must be specified.
 * */
struct FixedVelocityConstraint {
public:
	bool isFixed;
	double velX;
	double velTheta;
	
	FixedVelocityConstraint(bool isFixed, double velX, double velTheta) :
		isFixed(isFixed),
		velX(velX),
		velTheta(velTheta)
	{}
};

}

#endif //SEMANTIC_ELASTIC_BANDS_FIXEDVELOCITYCONSTRAINT_H
