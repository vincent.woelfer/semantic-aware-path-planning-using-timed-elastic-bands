#ifndef SEMANTIC_ELASTIC_BANDS_SEMANTICELASTICBAND_H
#define SEMANTIC_ELASTIC_BANDS_SEMANTICELASTICBAND_H

#include <vector>
#include "Pose.h"
#include "semantic_elastic_bands/DetectedObjects/DetectedObject.h"
#include "FixedVelocityConstraint.h"
#include "SebConfig.h"

namespace semantic_elastic_bands {

/* This class represents the state of the semantic elastic band. This state is defined by
 *  - The list of band poses
 *  - The list of time differences between those poses
 *  - The eventually fixed velocity at the start/end nodes
 *  
 *  Since these poses lie in the cell-space of the local costmap, a variant transformed into meter-space is also kept for faster access by other classes.
 *  The state of this elastic band is only modifiable by the class itself and the optimizer class (which passes these mutable references to the Ceres optimizer) 
 * */
class SemanticElasticBand
{
	// Declare Optimizer as friend, it is the only class allowed to modify the bands internal state (during optimization)
	// through the accessPoses/TimeDiffs method which returns a modifiable reference.
	friend class Optimizer;
	
public:
	explicit SemanticElasticBand(const std::vector<Pose>& path, bool isFinalGoal, const SebConfig& config, double mapResolution);	
	~SemanticElasticBand();	

	// Getters
	double getBandLength() const;
	double getBandDuration() const;
	double getMapResolution() const;
	const SebConfig& getConfig() const;
	const std::vector<Pose>& getPoses() const;
	const std::vector<double>& getTimeDiffs() const;
	
	std::vector<double> getLinearVelocities() const;
	std::vector<double> getRotationalVelocities() const;
	
	FixedVelocityConstraint getVelocityConstraintStart() const;
	FixedVelocityConstraint getVelocityConstraintGoal() const;
	
	const std::vector<Pose>& getTransformedPoses() const;
	
	// Setters
	void setVelocityConstraintStart(FixedVelocityConstraint constraint);
	void setVelocityConstraintGoal(FixedVelocityConstraint constraint);
	
	// Methods	
	void updateStartGoal(const Pose& start, const Pose& goal);
	void autoResize(bool fastMode = false);
	void updateTransformedPoses(double mapOriginX, double mapOriginY, double resolution);
	int getNumberOfDirectionChanges() const;

	// For debugging
	void verifyBandCorrectness(const std::string& testLocation = "") const;
	
protected:
	// Only accessible for the friend class optimizer
	std::vector<Pose>& accessPoses();
	std::vector<double>& accessTimeDiffs();	

private:
	void constructFromInitialPath(const std::vector<Pose>& path, bool isFinalGoal);
	
	// Add / Remove poses and timeDiffs
	void addPose(const Pose& pose);
	void addPose(Eigen::Vector2d position, double theta);
	void insertPose(int index, const Pose& pose);
	void deletePose(int index);
	void deletePoses(int startIndex, int endIndex);

	void addTimeDiff(double timeDiff);
	void insertTimeDiff(int index, double timeDiff);
	void deleteTimeDiff(int index);
	void deleteTimeDiffs(int startIndex, int endIndex);
	
	void addPoseAndTimeDiff(const Pose& pose, double timeDiff);

	// Member Variables
	const SebConfig& config;
	const double mapResolution;
	
	std::vector<Pose> poses;
	std::vector<double> timeDiffs;
	std::vector<Pose> transformedPoses;

	FixedVelocityConstraint velocityConstraintStart;
	FixedVelocityConstraint velocityConstraintGoal;
	
	// Helper Methods
	double estimateTimeDifference(const Pose& a, const Pose& b) const;

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}


#endif //SEMANTIC_ELASTIC_BANDS_SEMANTICELASTICBAND_H
